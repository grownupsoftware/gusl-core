![Alt text](https://www.googleapis.com/download/storage/v1/b/gusl-images/o/logo.png?generation=1588764693002370&alt=media)
# Core

Core is a project which is common **G**rown**Up** **S**oftware **L**imited (gusl) applications.


## Requirements
1. Java 11
2. Gradle 6

## Getting Started

```shell script
gradle wapper
```

```shell script
./gradlew clean compileJava
```

```shell script
./gradlew publishToMavenLocal
```

