package gusl.core.timer;

import java.util.Date;

/**
 * @author dhudson
 * @since 28/02/2023
 */
public class Stopwatch {

    private Long startTime;
    private Long endTime;

    public Stopwatch() {
        start();
    }

    public Stopwatch(long startTime) {
        this.startTime = startTime;
    }

    public Stopwatch(Date startTime) {
        this(startTime.getTime());
    }

    public void start() {
        startTime = System.currentTimeMillis();
    }

    public void stop() {
        endTime = System.currentTimeMillis();
    }

    public long duration() {
        if (startTime == null || endTime == null) {
            return -1;
        }

        return endTime - startTime;
    }

    public String stopAndFormat() {
        stop();
        return format();
    }

    public String format() {
        long duration = duration();
        long minutes = duration / (60 * 1000);
        long seconds = (duration / 1000) % 60;
        return String.format("%d:%02d", minutes, seconds);
    }

    public String toString() {
        return Long.toString(duration());
    }
}
