package gusl.core.config;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import gusl.core.exceptions.GUSLException;
import gusl.core.security.ObfuscatedStorage;
import gusl.core.utils.StringUtils;
import gusl.core.utils.SystemPropertyUtils;

import java.io.IOException;

/**
 * @author dhudson
 * @since 11/03/2022
 */
public class SecretDeserializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.readValueAsTree();
        String value = node.asText();
        if (value != null) {
            value = SystemProperyDeserializer.substitute(value);
            if (ObfuscatedStorage.isObfuscated(value)) {
                return ObfuscatedStorage.decryptKey(value);
            }
            if (!StringUtils.isBlank(SystemPropertyUtils.getVaultUrl())) {
                try {
                    return VaultDeserializer.process(value);
                } catch (GUSLException ex) {
                    throw new JsonParseException(jp, ex.getLocalizedMessage());
                }
            }
        }
        return value;
    }
}
