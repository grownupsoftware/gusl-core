/*
 * Grownup Software Limited.
 */
package gusl.core.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import java.io.IOException;

/**
 *
 * @author dhudson - Jul 26, 2017 - 11:20:58 AM
 */
public class ConfigDeserializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        String result = StringDeserializer.instance.deserialize(jp, dc);
        return result;
    }

}
