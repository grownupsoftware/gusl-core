/*
 * Grownup Software Limited.
 */
package gusl.core.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import gusl.core.exceptions.GUSLException;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.IOUtils;
import gusl.core.utils.Platform;
import gusl.core.utils.SystemPropertyUtils;
import lombok.CustomLog;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

/**
 * @author dhudson - Jul 26, 2017 - 9:55:49 AM
 */
@CustomLog
public class ConfigUtils {

    public static final String DEFAULT_CONFIG_FILE_NAME = "application.json";

    public static final int KILOBYTE = 1024;
    public static final int MEGABYTE = KILOBYTE * 1024;
    public static final int GIGABYTE = MEGABYTE * 1024;
    public static final int TERABYTE = GIGABYTE * 1024;

    public static final ObjectMapper theMapper = createConfigMapper();

    private ConfigUtils() {
    }

    public static <T> T loadConfig(Class<T> config, String configName) throws IOException {
        return loadConfig(config, configName, theMapper);
    }

    public static <T> T loadConfig(Class<T> config) throws IOException {
        if (SystemPropertyUtils.hasConfigLocation()) {
            logger.info("Loading config {}", SystemPropertyUtils.getGivenConfig());
            return gusl.core.config.ConfigUtils.loadConfig(config, SystemPropertyUtils.getGivenConfig(), theMapper);
        }

        // Lets load the default
        try (InputStream is = IOUtils.getResourceAsStream(DEFAULT_CONFIG_FILE_NAME, config.getClassLoader())) {
            logger.info("Loading default config {}", DEFAULT_CONFIG_FILE_NAME);
            return theMapper.readValue(is, config);
        } catch (IOException ex) {
            logger.error("Error reading config file: {}", DEFAULT_CONFIG_FILE_NAME, ex);
            throw ex;
        }
    }

    /**
     * Creates a standard Jackson object mapper, with a naming strategy of
     * KebabCase
     *
     * @return a new object mapper
     */
    public static ObjectMapper createConfigMapper() {
        return ObjectMapperFactory.createObjectMapper(PropertyNamingStrategy.KEBAB_CASE);
    }

    public static ObjectMapper getObjectMapper() {
        return theMapper;
    }

    /**
     * Load the config.
     *
     * @param <T>
     * @param config
     * @param fileName
     * @param mapper
     * @return
     * @throws IOException
     */
    public static <T> T loadConfig(Class<T> config, String fileName, ObjectMapper mapper) throws IOException {
        try (InputStream is = IOUtils.getResourceAsStream(fileName, ConfigUtils.class.getClassLoader())) {
            return mapper.readValue(is, config);
        } catch (IOException ex) {
            throw ex;
        }
    }

    /**
     * Converts a size into bytes.
     * <p>
     * Append the letter k or K to indicate kilobytes, or m or M to indicate
     * megabytes, g or G to represent Gigabytes.
     * <p>
     * Fractions are also allowed, for example 8.2M
     *
     * @param number
     * @return the number representation in bytes
     * @throws GUSLException
     */
    public static int byteNumberResolver(String number) throws GUSLException {
        if (number == null || number.isEmpty()) {
            throw new GUSLException("Empty byte number");
        }

        number = number.trim();
        int result = 0;

        char lastChar = number.charAt(number.length() - 1);
        if (Character.isLetter(lastChar)) {
            double num;
            try {
                num = Double.parseDouble(number.substring(0, number.length() - 1));
            } catch (NumberFormatException ex) {
                throw new GUSLException("Invalid number " + number);
            }
            switch (lastChar) {
                case 'M':
                case 'm':
                    result = (int) (num * MEGABYTE);
                    break;
                case 'K':
                case 'k':
                    result = (int) (num * KILOBYTE);
                    break;
                case 'G':
                case 'g':
                    result = (int) (num * GIGABYTE);
                    if (result <= 0) {
                        throw new GUSLException("Invalid number too large " + number);
                    }
                    break;
                default:
                    throw new GUSLException("Invalid number " + number);
            }
        } else {
            // Its bytes
            try {
                result = Integer.parseInt(number);
            } catch (NumberFormatException ex) {
                throw new GUSLException("Invalid number " + number);
            }
        }
        return result;
    }

    /**
     * Checks the last character of the string to denote the unit of measure.
     * <p>
     * D or d for days, h or H to indicate hours, or m or M to indicate minutes,
     * s or S to indicate seconds
     *
     * @param time
     * @return the number of milliseconds
     * @throws GUSLException
     */
    public static long millisNumberResolver(String time) throws GUSLException {
        long result = 0;
        time = time.trim();

        char lastChar = time.charAt(time.length() - 1);
        if (Character.isLetter(lastChar)) {
            long num;
            try {
                num = Long.parseLong(time.substring(0, time.length() - 1));
            } catch (NumberFormatException ex) {
                throw new GUSLException("Invalid time " + time);
            }

            switch (lastChar) {
                case 'd':
                case 'D':
                    result = TimeUnit.MILLISECONDS.convert(num, TimeUnit.DAYS);
                    break;
                case 'h':
                case 'H':
                    result = TimeUnit.MILLISECONDS.convert(num, TimeUnit.HOURS);
                    break;
                case 'm':
                case 'M':
                    result = TimeUnit.MILLISECONDS.convert(num, TimeUnit.MINUTES);
                    break;
                case 's':
                case 'S':
                    result = TimeUnit.MILLISECONDS.convert(num, TimeUnit.SECONDS);
                    break;
                default:
                    throw new GUSLException("Invalid time " + time);
            }
        } else {
            // Its millis
            try {
                result = Long.parseLong(time);
            } catch (NumberFormatException ex) {
                throw new GUSLException("Invalid time " + time);
            }
        }
        return result;
    }

    /**
     * Return the number of threads represented by this String.
     * <p>
     * Simple Arithmetic expression to determine the number of threads to use,
     * based of the number of available cores on the deployed machine.
     * <p>
     * The number can be absolute I.E. 5, or can be arithmetic, I.E. =/2, which
     * mean half the available cores. Remember, with threads, often less means
     * more.
     * <p>
     * With that given, =/1, =+0, =-0, =*1 all equate to the number of cores.
     *
     * @param threads
     * @return the result of the expression
     * @throws GUSLException
     */
    public static int threadNumberResolver(String threads) throws GUSLException {

        if (threads == null || threads.isEmpty()) {
            throw new GUSLException("Empty thread number");
        }

        threads = threads.trim();

        if (Character.isDigit(threads.charAt(0))) {
            // Its absolute
            try {
                return Integer.parseInt(threads);
            } catch (NumberFormatException ex) {
                throw new GUSLException("Unable to resolve thread number " + threads, ex);
            }
        }

        int num;
        String value = threads.substring(1);

        try {
            num = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            throw new GUSLException("Can't parse as thread count " + threads);
        }

        int result = 0;
        switch (threads.charAt(0)) {
            case '+':
                result = Platform.getNumberOfCores() + num;
                break;
            case '-':
                result = Platform.getNumberOfCores() - num;
                break;
            case '/':
                if (num == 0) {
                    throw new GUSLException("Can't divide by zero, thread count");
                }
                result = Platform.getNumberOfCores() / num;
                break;
            case '*':
                result = Platform.getNumberOfCores() * num;
                break;
            default:
                throw new GUSLException("Can't parse thread count " + threads);
        }

        if (result < 0) {
            throw new GUSLException("Thread expression < 0 " + threads);
        }

        return result;
    }

    public static int asKb(String value) throws GUSLException {
        return byteNumberResolver(value) / KILOBYTE;
    }

    public static int asGb(String value) throws GUSLException {
        return byteNumberResolver(value) / GIGABYTE;
    }
}
