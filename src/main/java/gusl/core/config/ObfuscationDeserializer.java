/*
 * Grownup  Software Limited.
 */
package gusl.core.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import gusl.core.security.ObfuscatedStorage;

import java.io.IOException;

/**
 * @author dhudson - Jul 26, 2017 - 12:35:21 PM
 */
public class ObfuscationDeserializer extends JsonDeserializer<String> {


    @Override
    public String deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
        JsonNode node = jp.readValueAsTree();
        String value = node.textValue();
        if (value != null) {
            if (ObfuscatedStorage.isObfuscated(value)) {
                value = ObfuscatedStorage.decryptKey(value);
            }
        }
        return value;
    }

}
