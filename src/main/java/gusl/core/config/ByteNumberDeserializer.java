/*
 * Grownup  Software Limited.
 */
package gusl.core.config;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import gusl.core.exceptions.GUSLException;

import java.io.IOException;

/**
 * Converts a size into bytes.
 * <p>
 * Append the letter k or K to indicate kilobytes, or m or M to indicate
 * megabytes, g or G to represent Gigabytes.
 * <p>
 * Fractions are also allowed, for example 8.2M
 *
 * @author dhudson - Jul 26, 2017 - 12:05:35 PM
 */
public class ByteNumberDeserializer extends JsonDeserializer<Integer> {

    @Override
    public Integer deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
        JsonNode node = jp.readValueAsTree();
        try {
            return ConfigUtils.byteNumberResolver(node.asText());
        } catch (GUSLException ex) {
            throw new JsonParseException(jp, ex.getLocalizedMessage());
        }
    }

}
