package gusl.core.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import gusl.core.utils.StringUtils;
import lombok.CustomLog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This looks for a pattern of ${foo:bar} or ${foo}.
 * <p>
 * Foo is a name of a system env, or system property. A system env will over
 * ride a system property. bar is the default, if no default is found, and not
 * env or system property found, null will be returned.
 *
 * @author dhudson
 */
@CustomLog
public class SystemProperyDeserializer extends JsonDeserializer<String> {

    private static final Pattern PATTERN = Pattern.compile("\\$\\{(.+?)\\}");
    private static final String EMPTY_STRING = "";
    private static final String DEFAULT_VALUE_SEPARATOR = ":";

    @Override
    public String deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        JsonNode node = jp.readValueAsTree();
        return substitute(node.asText());
    }

    public static String substitute(String text) {
        if (StringUtils.isBlank(text)) {
            return text;
        }
        String parsed = text;
        Matcher matcher = PATTERN.matcher(text);

        List<MatchResult> results = new ArrayList<>(2);
        while (matcher.find()) {
            results.add(matcher.toMatchResult());
        }

        if (!results.isEmpty()) {
            for (int i = results.size() - 1; i > -1; i--) {
                MatchResult matchResult = results.get(i);
                String key = matchResult.group(1);
                String def = EMPTY_STRING;
                int index = key.indexOf(":");
                if (index > 0) {
                    def = key.substring(index + 1);
                    key = key.substring(0, index);
                }

                String result = System.getenv(key);
                if (result == null) {
                    result = System.getProperty(key);
                }

                if (result == null) {
                    result = def;
                }

                String prefix = parsed.substring(0, matchResult.start());
                String suffix = parsed.substring(matchResult.end());
                parsed = prefix + result + suffix;
            }
        }

        return parsed;
    }

}
