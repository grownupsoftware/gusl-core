/*
 * Grownup  Software Limited.
 */
package gusl.core.config;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import gusl.core.exceptions.GUSLException;

import java.io.IOException;

/**
 * @author dhudson - Jul 26, 2017 - 1:19:13 PM
 */
public class MillisNumberDeserializer extends JsonDeserializer<Long> {

    @Override
    public Long deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
        JsonNode node = jp.readValueAsTree();
        try {
            return ConfigUtils.millisNumberResolver(node.asText());
        } catch (GUSLException ex) {
            throw new JsonParseException(jp, ex.getLocalizedMessage());
        }
    }

}
