/*
 * Grownup Software Limited.
 */
package gusl.core.config;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import gusl.core.exceptions.GUSLException;

import java.io.IOException;

/**
 * Simple Arithmetic expression to determine the number of threads to use, based
 * of the number of available cores on the deployed machine.
 * <p>
 * The number can be absolute I.E. 5, or can be arithmetic, I.E. /2, which mean
 * half the available cores. Remember, with threads, often less means more.
 * <p>
 * With that given, /1, +0, -0, *1 all equate to the number of cores.
 * <p>
 * Annotate the property in question with @JsonDeserialize(using =
 * CoreCountDeserializer.class)
 *
 * @author dhudson - Jul 26, 2017 - 11:33:46 AM
 */
public class CoreCountDeserializer extends JsonDeserializer<Integer> {

    @Override
    public Integer deserialize(JsonParser jp, DeserializationContext dc) throws IOException {
        JsonNode node = jp.readValueAsTree();
        try {
            return ConfigUtils.threadNumberResolver(node.asText());
        } catch (GUSLException ex) {
            throw new JsonParseException(jp, ex.getLocalizedMessage());
        }
    }

}
