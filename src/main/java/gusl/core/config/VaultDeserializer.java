package gusl.core.config;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import gusl.core.exceptions.GUSLException;
import gusl.core.vault.VaultHandler;

import java.io.IOException;

/**
 * @author dhudson
 * @since 11/03/2022
 */
public class VaultDeserializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.readValueAsTree();
        try {
            return process(node.asText());
        } catch (GUSLException ex) {
            throw new JsonParseException(jp, ex.getLocalizedMessage());
        }
    }

    static String process(String value) throws GUSLException {
        if (value != null) {
            if (value.startsWith("=")) {
                return value.substring(1);
            }
            return VaultHandler.getSecret(value);
        }
        return null;
    }
}
