package gusl.core.eventbus;

import java.util.List;
import java.util.concurrent.TimeUnit;

import gusl.core.executors.ExecutorState;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;

/**
 * EventBus Executor.
 *
 * Based off a Thread Pool Executor, but with some differences.
 *
 * First off, there is a fixed thread pool size, which will never change in
 * size.
 *
 * @author dhudson
 */
public class EventBusExecutor {

    private final GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    private final EventBusQueue theQueue;
    private final ExecutorState theState;

    /**
     * Permission required for callers of shutdown and shutdownNow. We
     * additionally require (see checkShutdownAccess) that callers have
     * permission to actually interrupt threads in the worker set (as governed
     * by Thread.interrupt, which relies on ThreadGroup.checkAccess, which in
     * turn relies on SecurityManager.checkAccess). Shutdowns are attempted only
     * if these checks pass.
     *
     * All actual invocations of Thread.interrupt (see interruptIdleWorkers and
     * interruptWorkers) ignore SecurityExceptions, meaning that the attempted
     * interrupts silently fail. In the case of shutdown, they should not fail
     * unless the SecurityManager has inconsistent policies, sometimes allowing
     * access to a thread and sometimes not. In such cases, failure to actually
     * interrupt threads may disable or delay full termination. Other uses of
     * interruptIdleWorkers are advisory, and failure to actually interrupt will
     * merely delay response to configuration changes so is not handled
     * exceptionally.
     */
    private static final RuntimePermission shutdownPerm = new RuntimePermission("modifyThread");

    /**
     * Set containing all worker threads in pool.
     */
    private EventBusQueueWorker[] theWorkers;

    private int theThreadPoolSize;
    private int theThreadPriority;
    private final String theExecutorName = "LmEventBus";
    private final ListenerRegistra theRegistra;

    /**
     * Create an executor with the given name, a thread priority of Normal and
     * the pool size of the number of cores on the system.
     *
     * @param registra
     */
    public EventBusExecutor(ListenerRegistra registra) {
        theRegistra = registra;
        theState = new ExecutorState();
        theQueue = new EventBusQueue();
    }

    public void configure(int numberOfThreads, int priority) {

        theThreadPriority = priority;
        theThreadPoolSize = numberOfThreads;
        theWorkers = new EventBusQueueWorker[numberOfThreads];

        prestartThreads();
    }

    /**
     * Starts all threads, causing them to idly wait for work.
     */
    private void prestartThreads() {

        for (int i = 0; i < theThreadPoolSize; i++) {

            String workerName = theExecutorName + " worker " + i;
            EventBusQueueWorker worker = new EventBusQueueWorker(theRegistra, theState, theQueue,
                    workerName, theThreadPriority);

            final Thread t = worker.getThread();
            if (t != null) {
                theWorkers[i] = worker;
                t.start();
            } else {
                logger.error("Unable to create thread");
            }
        }
    }

    /**
     * Submit all entries.
     *
     * @param entries
     */
    public void submit(List<? extends EventBusRepositoryEntry> entries) {
        entries.forEach((entry) -> {
            submit(entry);
        });
    }

    /**
     * Submit an entry for execution.
     *
     * @param entry for execution
     */
    public void submit(EventBusRepositoryEntry entry) {
        checkSubmission(entry);
        theQueue.offer(entry);
    }

    private void checkSubmission(EventBusRepositoryEntry entry) {
        if (entry == null) {
            throw new NullPointerException();
        }

        if (!theState.isRunning()) {
            throw new IllegalStateException("Executor not running");
        }
    }

    /**
     * Initiates an orderly shutdown in which previously submitted tasks are
     * executed, but no new tasks will be accepted.
     *
     * <p>
     * Invocation has no additional effect if already shut down.
     * </p>
     *
     * <p>
     * This method does not wait for previously submitted tasks to complete
     * execution. Use {@link #awaitTermination awaitTermination} to do that.
     * </p>
     *
     * @throws SecurityException {@inheritDoc}
     */
    public synchronized void shutdown() {
        if (theState.isRunning()) {
            checkShutdownAccess();
            theState.set(ExecutorState.SHUTDOWN);
            interruptIdleWorkers();
            tryTerminate();
        }
    }

    /**
     * Attempts to stop all actively executing tasks, halts the processing of
     * waiting tasks, and returns a list of the tasks that were awaiting
     * execution.
     *
     * <p>
     * These tasks are drained (removed) from the task queue upon return from
     * this method.</p>
     *
     * <p>
     * This method does not wait for actively executing tasks to terminate. Use
     * {@link #awaitTermination awaitTermination} to do that.
     * </p>
     *
     * <p>
     * There are no guarantees beyond best-effort attempts to stop processing
     * actively executing tasks. This implementation cancels tasks via
     * {@link Thread#interrupt}, so any task that fails to respond to interrupts
     * may never terminate.
     * </p>
     *
     * @throws SecurityException {@inheritDoc}
     */
    public synchronized void shutdownNow() {
        if (theState.isShutdown()) {
            return;
        }

        checkShutdownAccess();

        theState.set(ExecutorState.STOP);
        theQueue.clear();
        interruptWorkers();

        tryTerminate();
    }

    /**
     * Block, waiting for the shutdown process to happen.
     *
     * @param timeout time to wait
     * @param unit unit of time
     * @return true if shutdown successful
     * @throws InterruptedException
     */
    @SuppressWarnings("CallToThreadYield")
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        long nanos = unit.toNanos(timeout);
        long endTime = System.nanoTime() + nanos;

        while (!theState.isTerminated()) {

            if (Thread.interrupted()) {
                throw new InterruptedException();
            }

            if (theQueue.isEmpty() && !hasRunningWorkers()) {
                theState.set(ExecutorState.TERMINATED);
                break;
            } else {
                // If the queue is not empty, then there is still work to do
                if (theQueue.isEmpty()) {
                    if (theState.getState() == ExecutorState.STOP) {
                        interruptWorkers();
                    } else {
                        interruptIdleWorkers();
                    }
                }
                // Don't chew the CPU
                Thread.yield();
            }

            if (System.nanoTime() > endTime) {
                // Timeout
                return false;
            }
        }

        return true;
    }

    public boolean isShutdown() {
        return !theState.isRunning();
    }

    /**
     * If there is a security manager, makes sure caller has permission to shut
     * down threads in general (see shutdownPerm). If this passes, additionally
     * makes sure the caller is allowed to interrupt each worker thread. This
     * might not be true even if first check passed, if the SecurityManager
     * treats some threads specially.
     */
    private void checkShutdownAccess() {
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            security.checkPermission(shutdownPerm);
            for (EventBusQueueWorker w : theWorkers) {
                security.checkAccess(w.getThread());
            }

        }
    }

    /**
     * Interrupts all threads, even if active.
     *
     * This sets the cancel flag, to stop the threads running.
     *
     * Ignores SecurityExceptions (in which case some threads may remain
     * uninterrupted).
     */
    private void interruptWorkers() {
        for (EventBusQueueWorker w : theWorkers) {
            w.interruptIfStarted();
        }
    }

    /**
     * Transitions to TERMINATED state if either (SHUTDOWN and pool and queue
     * empty) or (STOP and pool empty).
     */
    private void tryTerminate() {

        // Stuff still running..
        if (hasRunningWorkers()) {
            return;
        }

        if (theState.isShutdown() && theQueue.isEmpty()) {
            theState.set(ExecutorState.TERMINATED);
        }

    }

    private boolean hasRunningWorkers() {
        for (EventBusQueueWorker worker : theWorkers) {
            if (worker.isRunning()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Invokes {@code shutdown} when this executor is no longer referenced and
     * it has no threads.
     */
    @Override
    protected void finalize() {
        try {
            shutdown();
        } finally {
            try {
                super.finalize();
            } catch (Throwable ex) {
                // All a bit late now
            }
        }
    }

    /**
     * Interrupts threads that might be waiting for tasks (as indicated by not
     * being locked) so they can check for termination or configuration changes.
     * Ignores SecurityExceptions (in which case some threads may remain
     * uninterrupted).
     */
    private void interruptIdleWorkers() {
        for (EventBusQueueWorker worker : theWorkers) {
            if (worker.isRunning()) {
                Thread t = worker.getThread();
                if (!t.isInterrupted() && worker.tryLock()) {
                    try {
                        t.interrupt();
                    } catch (SecurityException ignore) {
                    } finally {
                        worker.unlock();
                    }

                }
            }
        }
    }

    /**
     * Return the number of work threads
     *
     * @return number of worker threads
     */
    public int getThreadPoolSize() {
        return theThreadPoolSize;
    }

    /**
     * Return the thread priority
     *
     * @return
     */
    public int getThreadPriority() {
        return theThreadPriority;
    }

    public int getQueueSize() {
        return theQueue.size();
    }

}
