/*
 * Grownup Software Limited.
 */
package gusl.core.eventbus;

import lombok.CustomLog;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Contains all of the listener methods.
 * <p>
 * CopyOnWrite used, for iteration performance.
 *
 * @author dhudson - Apr 10, 2017 - 9:00:38 AM
 */
@CustomLog
public class ListenerRegistra {

    private final ConcurrentMap<Class<?>, CopyOnWriteArrayList<ListenerMethod>> listeners;

    public ListenerRegistra() {
        listeners = new ConcurrentHashMap<>();
    }

    public void register(Object listener) {
        internalRegister(listener);
    }

    public void unregister(Object listener) {
        internalUnregister(listener);
    }

    /**
     * Return a list of listening methods.
     *
     * @param clazz
     * @return a list of listening methods, or null if there are no listening methods.
     */
    public List<ListenerMethod> getListenersFor(Class<?> clazz) {
        if (clazz == null) {
            return null;
        }
        return listeners.get(clazz);
    }

    private void internalUnregister(Object listener) {
        for (Method method : getOnEventMethods(listener, false)) {
            Class<?>[] params = method.getParameterTypes();
            CopyOnWriteArrayList<ListenerMethod> methodlisteners = listeners.get(params[0]);
            if (methodlisteners == null || methodlisteners.isEmpty()) {
                // Nothing to do
                return;
            }

            ListenerMethod listMethod = new ListenerMethod(listener, method, method.getAnnotation(OnEvent.class));
            methodlisteners.remove(listMethod);
        }
    }

    private void internalRegister(Object listener) {
        List<Method> javaMethods = getOnEventMethods(listener, true);
        if (javaMethods.isEmpty()) {
            logger.debug("No @OnEvent annotated methods found {}", listener.getClass().getName());
            return;
        }

        for (Method method : javaMethods) {
            Class<?>[] params = method.getParameterTypes();

            // If we have got this far its all good.
            ListenerMethod listenerMethod = new ListenerMethod(listener, method, method.getAnnotation(OnEvent.class));

            CopyOnWriteArrayList<ListenerMethod> methods = listeners.get(params[0]);
            if (methods == null) {
                methods = new CopyOnWriteArrayList<>();
                methods.add(listenerMethod);
                listeners.put(params[0], methods);
            } else {
                // Only add it once
                if (!methods.contains(listenerMethod)) {
                    boolean added = false;
                    int size = methods.size();

                    for (int i = 0; i < size; i++) {
                        // Lets add it in the correct place
                        if (listenerMethod.compareTo(methods.get(i)) <= 0) {
                            methods.add(i, listenerMethod);
                            added = true;
                            // Skip to end
                            i = size;
                        }
                    }

                    if (!added) {
                        methods.add(listenerMethod);
                    }
                }
            }
        }
    }

    private List<Method> getOnEventMethods(Object listener, boolean grumble) {
        List<Method> methods = new ArrayList<>(3);
        Class<?> parentClass = listener.getClass();

        while (parentClass != null) {
            for (Method method : parentClass.getMethods()) {
                if (method.isSynthetic()) {
                    continue;
                }

                if (method.isAnnotationPresent(OnEvent.class)) {
                    Class<?>[] params = method.getParameterTypes();
                    if (params.length == 0 || params.length > 2) {
                        if (grumble) {
                            logger.warn("Invalid number of params for @OnEvent annotated method {} on class {}",
                                    method.getName(), listener.getClass().getName());
                        }
                        continue;
                    }

                    methods.add(method);
                }
            }

            parentClass = parentClass.getSuperclass();
        }

        return methods;
    }
}
