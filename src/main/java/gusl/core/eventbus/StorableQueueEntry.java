package gusl.core.eventbus;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Sub classes of this can be persisted by the repository.
 *
 * @author dhudson
 */
public abstract class StorableQueueEntry extends EventBusAbstractQueueEntry {

    private String id;
    // As these get stored, lets not store this.
    @JsonIgnore
    private EventBusRepository theRepository;

    public StorableQueueEntry() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRepository(EventBusRepository repository) {
        theRepository = repository;
    }

    @Override
    public void complete() {
        // The repository will not be set, if the events has not been stored
        if (theRepository != null) {
            theRepository.delete(this);
        }
    }
}
