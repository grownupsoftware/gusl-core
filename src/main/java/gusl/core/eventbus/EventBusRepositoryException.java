package gusl.core.eventbus;

import gusl.core.exceptions.GUSLException;

/**
 *
 * @author dhudson
 */
@SuppressWarnings("serial")
public class EventBusRepositoryException extends GUSLException {

    public EventBusRepositoryException(String message, Throwable ex) {
        super(message, ex);
    }

    public EventBusRepositoryException(String message) {
        super(message);
    }

}
