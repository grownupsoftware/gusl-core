/*
 * Grownup Software Limited.
 */
package gusl.core.eventbus;

import gusl.core.config.ConfigUtils;
import gusl.core.exceptions.GUSLException;
import lombok.CustomLog;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author dhudson - Apr 10, 2017 - 9:43:21 AM
 */
@CustomLog
public class ListenerMethod implements Comparable<ListenerMethod> {

    private final Method method;
    private final OnEvent annotation;
    private final Object listener;
    private final long executeTime;
    private final Class<? extends Throwable> expected;

    public ListenerMethod(Object listener, Method method, OnEvent annotation) {
        this.listener = listener;
        this.method = method;
        this.annotation = annotation;
        executeTime = getExecuteTime(annotation);
        expected = annotation.expected();
    }

    private long getExecuteTime(OnEvent annotation) {
        try {
            return ConfigUtils.millisNumberResolver(annotation.executeTime());
        } catch (GUSLException ex) {
            logger.warn("Can't understand execute time of {}, setting to 1 second", annotation.executeTime());
            // Default to 1 second
            return 1000L;
        }
    }

    public int getOrder() {
        return annotation.order();
    }

    public Method getMethod() {
        return method;
    }

    public Object getListener() {
        return listener;
    }

    public boolean isExpectedNone() {
        return (expected == NoneThrowable.class);
    }

    public Class<? extends Throwable> getExpected() {
        return expected;
    }

    public void invoke(Object event) throws Exception {
        try {
            getMethod().invoke(listener, new Object[]{event});

        } catch (IllegalAccessException | IllegalArgumentException ex) {
            logger.error("********* Error listener: {} method: {}, event: {}", listener == null ? "null" : listener.getClass().getCanonicalName(), getMethod() == null ? "null" : getMethod().getName(), event == null ? "null" : event.toString(), ex);
            throw ex;
        }
    }

    @Override
    public int compareTo(ListenerMethod o) {
        return Integer.compare(getOrder(), o.getOrder());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof ListenerMethod) {
            ListenerMethod comp = ((ListenerMethod) obj);

            if (comp.getListener() != getListener()) {
                return false;
            }

            return ((ListenerMethod) obj).hashCode() == hashCode();
        }

        return false;
    }

    @Override
    public int hashCode() {
        return method.hashCode();
    }

    boolean isFailFast() {
        return annotation.failFast();
    }

    boolean isSlowLongRunning() {
        return annotation.slowLongRunning();
    }

    long getExecuteTime() {
        return executeTime;
    }

    String getMethodName() {
        return method.getName();
    }

    boolean isReportingException(Throwable t) {
        if (!isFailFast()) {
            return true;
        }

        if (expected == NoneThrowable.class) {
            // We are not expecting anything
            return true;
        }

        if (t instanceof InvocationTargetException) {
            InvocationTargetException ex = (InvocationTargetException) t;
            return expected != ex.getCause().getClass();
        }

        return t.getClass() != expected;
    }
}
