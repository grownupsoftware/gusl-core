package gusl.core.eventbus;

import java.util.List;

/**
 * Repository needs to be able load / save and delete events.
 *
 * @author dhudson
 */
public interface EventBusRepository {

    public void save(StorableQueueEntry entry) throws EventBusRepositoryException;

    public List<StorableQueueEntry> load() throws EventBusRepositoryException;

    public void delete(StorableQueueEntry entry);
}
