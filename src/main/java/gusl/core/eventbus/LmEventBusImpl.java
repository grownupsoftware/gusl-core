/*
 * Grownup Software Limited.
 */
package gusl.core.eventbus;

import lombok.CustomLog;

import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Simple internal Event Bus.
 *
 * @author dhudson - Apr 10, 2017 - 3:06:02 PM
 */
@CustomLog
public class LmEventBusImpl implements LmEventBus {

    private final ListenerRegistra theRegistra;
    private final EventBusExecutor theExecutor;
    private EventBusRepository theRepository;

    public LmEventBusImpl() {
        theRegistra = new ListenerRegistra();
        theExecutor = new EventBusExecutor(theRegistra);
    }

    @Override
    public void register(Object object) {
        theRegistra.register(object);
    }

    @Override
    public void unregister(Object object) {
        theRegistra.unregister(object);
    }

    @Override
    public void registerAll(Collection<Object> listeners) {
        listeners.forEach((listener) -> {
            register(listener);
        });
    }

    @Override
    public void shutdown() {
        theExecutor.shutdown();
    }

    @Override
    public void configure(int numberOfThreads) {
        theExecutor.configure(numberOfThreads, Thread.NORM_PRIORITY);
    }

    @Override
    public void post(Object event) {
        theExecutor.submit(createPostEntry(event));
    }

    @Override
    public void postWhenComplete(Object event, Future<?> future) {
        LmPostFutureEntry entry = new LmPostFutureEntry();
        entry.setEvent(event);
        entry.setFuture(future);
        theExecutor.submit(entry);
    }

    @Override
    public void scheduledPost(Object event, long time, TimeUnit unit) {
        theExecutor.submit(createScheduleEntry(event, time, unit));
    }

    @Override
    public void persistAndPost(Object event) throws EventBusRepositoryException {
        saveAndSubmit(createPostEntry(event));
    }

    @Override
    public void persistAndScheduledPost(Object event, long time, TimeUnit unit) throws EventBusRepositoryException {
        saveAndSubmit(createScheduleEntry(event, time, unit));
    }

    private void saveAndSubmit(StorableQueueEntry entry) throws EventBusRepositoryException {
        entry.setId(UUID.randomUUID().toString());
        theRepository.save(entry);
        entry.setRepository(theRepository);
        theExecutor.submit(entry);
    }

    private LmPostEntry createPostEntry(Object event) {
        LmPostEntry entry = new LmPostEntry();
        entry.setEvent(event);
        return entry;
    }

    private LmScheduledEntry createScheduleEntry(Object event, long time, TimeUnit unit) {
        LmScheduledEntry entry = new LmScheduledEntry();
        entry.setEvent(event);
        entry.setTimeToRun((System.currentTimeMillis() + TimeUnit.MILLISECONDS.convert(time, unit)));
        return entry;
    }

    @Override
    public void installRepositoryHandler(EventBusRepository repository) throws EventBusRepositoryException {
        theRepository = repository;
        theExecutor.submit(repository.load());
    }

    @Override
    public CompletableFuture<Void> postWithFuture(Object event) {
        CompletableFutureEntry entry = new CompletableFutureEntry();
        entry.setEvent(event);
        theExecutor.submit(entry);
        return entry.getCompletableFuture();
    }

    @Override
    public int getQueueSize() {
        return theExecutor.getQueueSize();
    }

    @Override
    public int getThreadPoolSize() {
        return theExecutor.getThreadPoolSize();
    }
}
