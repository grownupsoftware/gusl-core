package gusl.core.eventbus;

/**
 * A schedule event is to be processed some time in the future.
 *
 * This can be used for retrying an event who's service is intermittent.
 *
 * @author dhudson
 */
public class LmScheduledEntry extends StorableQueueEntry {

    private long timeToRun;

    public LmScheduledEntry() {
    }

    public long getTimeToRun() {
        return timeToRun;
    }

    public void setTimeToRun(long theTimeToRun) {
        this.timeToRun = theTimeToRun;
    }

    @Override
    public boolean isReady() {
        return (System.currentTimeMillis() >= timeToRun);
    }

}
