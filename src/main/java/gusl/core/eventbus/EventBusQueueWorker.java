package gusl.core.eventbus;

import gusl.core.executors.ExecutorState;
import lombok.CustomLog;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * Worker thread for the Keyed Executor.
 * <p>
 * This class opportunistically extends AbstractQueuedSynchronizer to simplify
 * acquiring and releasing a lock surrounding each task execution. This protects
 * against interrupts that are intended to wake up a worker thread waiting for a
 * task from instead interrupting a task being run.</p>
 *
 * @author dhudson
 */
@CustomLog
public class EventBusQueueWorker extends AbstractQueuedSynchronizer implements Runnable {

    private static final long serialVersionUID = 6138294804551838833L;

    /**
     * Thread this worker is running in.
     */
    private final Thread theThread;

    // Flag to state that the executor wants to shut down
    private volatile boolean isClosing;

    // Flag to indicate if the thread is running
    private boolean isRunning;

    private final ExecutorState theState;
    private final EventBusQueue theQueue;
    private final ListenerRegistra theRegistra;

    /**
     * Creates with given first task and thread from ThreadFactory.
     */
    EventBusQueueWorker(ListenerRegistra registra, ExecutorState state, EventBusQueue queue, String name, int priority) {
        // inhibit interrupts until run
        setState(-1);
        theRegistra = registra;
        theState = state;
        theQueue = queue;

        // Create a thread
        theThread = new Thread(this);
        theThread.setName(name);
        theThread.setPriority(priority);
    }

    /**
     * Repeatedly gets tasks from queue and executes them.
     * <p>
     * If getTask returns null, then exit from the loop.
     */
    @Override
    @SuppressWarnings("unchecked")
    public void run() {
        logger.debug("Starting ... {} ", getName());
        isRunning = true;
        EventBusRepositoryEntry entry;

        // allow interrupts
        unlock();

        while ((entry = getTask()) != null) {
            lock();

            try {
                // If pool is stopping, ensure thread is interrupted;
                // if not, ensure thread is not interrupted.  This
                // requires a recheck in second case to deal with
                // shutdownNow race while clearing interrupt
                if ((theState.runStateAtLeast(ExecutorState.STOP)
                        || (Thread.interrupted()
                        && theState.runStateAtLeast(ExecutorState.STOP)))
                        && !theThread.isInterrupted()) {
                    theThread.interrupt();
                }

                final Object event = entry.getEvent();

                // Lets get the Listener methods, at this time, as this may be a future post.
                List<ListenerMethod> listeners = theRegistra.getListenersFor(event.getClass());

                // The list can be null if the event has no listeners
                if (listeners != null && !listeners.isEmpty()) {

                    // I have the listeners in order
                    for (ListenerMethod method : listeners) {
                        if (method.isSlowLongRunning()) {
                            // Create a thread and let it go
                            new SlowQueueWorker(method, event).start();
                        } else {
                            try {
                                final long startTime = System.currentTimeMillis();
                                method.invoke(event);
                                if ((System.currentTimeMillis() - startTime) > method.getExecuteTime()) {
                                    String className = event.getClass().getCanonicalName();
                                    if (className == null) {
                                        className = event.getClass().getName();
                                    }
                                    logger.warn("{} has a long running invocation time of {}ms - class: for event {} method {}",
                                            theThread.getName(), (System.currentTimeMillis() - startTime), className, method.getMethodName());
                                }
                            } catch (Throwable t) {
                                if (method.isReportingException(t)) {
                                    // Get the cause
                                    if (t instanceof InvocationTargetException) {
                                        t = ((InvocationTargetException) t).getCause();
                                    }

                                    logger.warn("EventBus caught exception, whilst trying to process {} for {}",
                                            method.getMethod().getName(), method.getListener().getClass().getName(), t);
                                }
                            }
                        }
                    }

                }
                // Delete here ...
                entry.complete();
            } catch (Throwable t) {
                try {
                    // Never let these die.
                    logger.warn("Error in EventBusWorker", t);
                } catch (Throwable t1) {
                    // Yes the logger can throw exceptions too
                    System.err.println("Error logging error [EventBusQueueWorker]");
                    t1.printStackTrace();
                }
            } finally {
                unlock();
            }
        }

        isRunning = false;

        logger.debug("Stopping ... {} ", getName());
    }

    /**
     * Performs blocking timed wait for a task.
     *
     * @return task, or null if the worker must exit.
     */
    private EventBusRepositoryEntry getTask() {

        while (true) {
            try {
                // Check if queue empty only if necessary.
                if (theState.isShutdown() && theQueue.isEmpty()) {
                    isClosing = true;
                    return null;
                }

                // Wait for the queue to assign work
                return theQueue.take();
            } catch (InterruptedException ex) {
                // Been woken up by a change of state
            }
        }
    }

    // Lock methods
    //
    // The value 0 represents the unlocked state.
    // The value 1 represents the locked state.
    @Override
    protected boolean isHeldExclusively() {
        return getState() != 0;
    }

    @Override
    protected boolean tryAcquire(int unused) {
        if (compareAndSetState(0, 1)) {
            setExclusiveOwnerThread(Thread.currentThread());
            return true;
        }
        return false;
    }

    @Override
    protected boolean tryRelease(int unused) {
        setExclusiveOwnerThread(null);
        setState(0);
        return true;
    }

    void lock() {
        acquire(1);
    }

    boolean tryLock() {
        return tryAcquire(1);
    }

    void unlock() {
        release(1);
    }

    boolean isLocked() {
        return isHeldExclusively();
    }

    void interruptIfStarted() {
        isClosing = true;
        Thread t;
        if (getState() >= 0 && (t = theThread) != null && !t.isInterrupted()) {
            try {
                t.interrupt();
            } catch (SecurityException ignore) {
            }
        }
    }

    Thread getThread() {
        return theThread;
    }

    boolean isRunning() {
        return isRunning;
    }

    String getName() {
        return theThread.getName();
    }

    boolean isClosing() {
        return isClosing;
    }

}
