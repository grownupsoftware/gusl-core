package gusl.core.eventbus;

import java.util.concurrent.CompletableFuture;

/**
 * Entry that will complete the future when complete.
 *
 * @author dhudson
 */
public class CompletableFutureEntry extends EventBusAbstractQueueEntry {

    private final CompletableFuture<Void> theFuture;

    public CompletableFutureEntry() {
        theFuture = new CompletableFuture<>();
    }

    @Override
    public boolean isReady() {
        return true;
    }

    public CompletableFuture<Void> getCompletableFuture() {
        return theFuture;
    }

    @Override
    public void complete() {
        theFuture.complete(null);
    }

}
