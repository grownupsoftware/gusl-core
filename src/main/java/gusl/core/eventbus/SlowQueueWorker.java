package gusl.core.eventbus;

import lombok.CustomLog;

import java.lang.reflect.InvocationTargetException;

/**
 * Create a new Slow thread, and execute the OnEvent method.
 * <p>
 * Don't worry about execution time, we know it will be long.
 *
 * @author dhudson
 */
@CustomLog
public class SlowQueueWorker extends Thread {

    private final ListenerMethod theMethod;
    private final Object theEvent;

    public SlowQueueWorker(ListenerMethod method, Object event) {
        super("SlowQueueWorker");
        setPriority(MIN_PRIORITY);
        theMethod = method;
        theEvent = event;
    }

    @Override
    public void run() {
        try {
            theMethod.invoke(theEvent);
        } catch (Throwable t) {
            if (theMethod.isReportingException(t)) {
                // Get the cause
                if (t instanceof InvocationTargetException) {
                    t = ((InvocationTargetException) t).getCause();
                }

                logger.warn("EventBus caught exception, whilst trying to process {} for {}",
                        theMethod.getMethod().getName(), theMethod.getListener().getClass().getName(), t);
            }
        }
    }

}
