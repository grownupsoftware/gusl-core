package gusl.core.eventbus;

import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Queue which handles the processing of Event Bus events.
 *
 * @author dhudson
 */
class EventBusQueue extends LinkedBlockingQueue<EventBusRepositoryEntry> {

    private static final long serialVersionUID = 1L;
    private final ReentrantLock theLock;
    private final Object sync = new Object();
    private volatile boolean isWaiting;

    EventBusQueue() {
        this(Integer.MAX_VALUE, true);
    }

    EventBusQueue(int capacity) {
        this(capacity, true);
    }

    /**
     * Create a new keyed queue with the given fixed capacity and whether
     * locking is fair or not.
     *
     * @param capacity Fixed queue capacity.
     * @param useFairLock If true all threads get a fair chance to take from the
     * queue, if false take will be faster but not fair meaning some threads
     * will wait longer than others.
     */
    EventBusQueue(int capacity, boolean useFairLock) {
        super(capacity);
        theLock = new ReentrantLock(useFairLock);
    }

    @Override
    public boolean offer(EventBusRepositoryEntry e) {
        boolean result = super.offer(e);
        if (isWaiting) {
            synchronized (sync) {
                sync.notify();
            }
        }
        return result;
    }

    /**
     * Take in order and await forever ..
     *
     * @return
     */
    @Override
    public EventBusRepositoryEntry take() throws InterruptedException {
        try {
            // At this point in time, only one thread will access this
            theLock.lock();
            while (true) {

                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }

                // Are we empty
                if (isEmpty()) {
                    isWaiting = true;
                    synchronized (sync) {
                        sync.wait(5l);
                    }
                    isWaiting = false;
                    continue;
                }

                // List in order
                Iterator<EventBusRepositoryEntry> iterator = iterator();

                EventBusRepositoryEntry entry;

                while (iterator.hasNext()) {
                    entry = iterator.next();

                    // Check to see if there has been an interruption
                    if (Thread.interrupted()) {
                        throw new InterruptedException();
                    }

                    if (entry.isReady()) {
                        iterator.remove();
                        return entry;
                    }
                }
            }

        } finally {
            theLock.unlock();
        }
    }

}
