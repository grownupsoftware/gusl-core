package gusl.core.eventbus;

/**
 * Used in the OnEvent Annotation.
 *
 * @author dhudson
 */
class NoneThrowable extends Throwable {

    private static final long serialVersionUID = 1L;

    NoneThrowable() {
    }
}
