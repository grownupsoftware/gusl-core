package gusl.core.eventbus;

import java.util.concurrent.Future;

/**
 * Wrapper Class for the Queue.
 *
 * @author dhudson
 */
public class LmPostFutureEntry extends EventBusAbstractQueueEntry {

    private Future<?> future;

    public LmPostFutureEntry() {
    }

    public void setFuture(Future<?> future) {
        this.future = future;
    }

    public Future<?> getFuture() {
        return future;
    }

    @Override
    public boolean isReady() {
        return future.isDone();
    }
}
