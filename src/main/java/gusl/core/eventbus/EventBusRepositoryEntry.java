package gusl.core.eventbus;

/**
 * Event Entry.
 *
 * @author dhudson
 */
public interface EventBusRepositoryEntry {

    /**
     * Set the event to process.
     *
     * @param event
     */
    public void setEvent(Object event);

    /**
     * Get the event.
     *
     * @return
     */
    public Object getEvent();

    /**
     * Return true, if event ready to process.
     *
     * @return
     */
    public abstract boolean isReady();

    /**
     * Called then the event has been processed by all of the listeners.
     */
    public void complete();
}
