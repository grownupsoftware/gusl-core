package gusl.core.eventbus;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.json.ObjectMapperFactory;
import lombok.CustomLog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Store entries as single files, using JSON.
 *
 * @author dhudson
 */
@CustomLog
public class JsonFileEventBusRepository implements EventBusRepository {

    private final File theLocation;
    private final ObjectMapper theMapper;

    private final Map<String, Class<?>> theClassMap;

    public JsonFileEventBusRepository(File location) throws EventBusRepositoryException {
        theLocation = location;
        theMapper = ObjectMapperFactory.getDefaultObjectMapper();
        theClassMap = new HashMap<>();

        // Make sure the directories exist
        location.mkdirs();
        if (!location.exists()) {
            throw new EventBusRepositoryException("Location doesn't exist " + location.getAbsolutePath());
        }
    }

    @Override
    public void save(StorableQueueEntry entry) throws EventBusRepositoryException {
        if (entry.getId() == null) {
            throw new EventBusRepositoryException("Unable to store event, id not set");
        }

        File store = new File(theLocation, entry.getId());

        try {
            theMapper.writeValue(store, new JsonClassWrapper(entry));
            logger.info("Stored {}", store.getAbsoluteFile());
        } catch (IOException ex) {
            logger.warn("Unable to store entry {}", entry, ex);
        }
    }

    @Override
    public List<StorableQueueEntry> load() throws EventBusRepositoryException {
        List<StorableQueueEntry> entries = new ArrayList<>();
        for (File file : theLocation.listFiles()) {
            try {
                JsonNode node = theMapper.readTree(file);
                String className = node.get("type").asText();
                StorableQueueEntry entry = (StorableQueueEntry) theMapper.treeToValue(node.get("entry"), getType(className));
                entry.setRepository(this);
                entries.add(entry);
            } catch (IOException | ClassNotFoundException ex) {
                logger.warn("Unable to load event {}", file.getName(), ex);
            }
        }
        return entries;
    }

    @Override
    public void delete(StorableQueueEntry entry) {
        File store = new File(theLocation, entry.getId());
        if (store.delete()) {
            logger.info("Deleted event {}", entry.getId());
        } else {
            logger.info("Unable to delete event {}", entry.getId());
        }
    }

    private class JsonClassWrapper {

        private String type;
        private StorableQueueEntry entry;

        JsonClassWrapper(StorableQueueEntry entry) {
            type = entry.getClass().getName();
            this.entry = entry;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public StorableQueueEntry getEntry() {
            return entry;
        }

        public void setEntry(StorableQueueEntry entry) {
            this.entry = entry;
        }

    }

    /**
     * Get the type of the response.
     *
     * @param typeName
     * @return
     * @throws ClassNotFoundException
     */
    private Class<?> getType(String typeName) throws ClassNotFoundException {
        Class<?> type = theClassMap.get(typeName);
        if (type == null) {
            type = Class.forName(typeName);
            theClassMap.put(typeName, type);
        }
        return type;
    }
}
