package gusl.core.eventbus;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Base class for Queue entries.
 *
 * @author dhudson
 */
public abstract class EventBusAbstractQueueEntry implements EventBusRepositoryEntry {

    @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
    private Object event;

    public EventBusAbstractQueueEntry() {
    }

    @Override
    public void setEvent(Object event) {
        this.event = event;
    }

    @Override
    public Object getEvent() {
        return event;
    }

    @Override
    public void complete() {
    }
}
