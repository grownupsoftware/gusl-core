/*
 * Grownup Software Limited.
 */
package gusl.core.eventbus;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Event Bus Method tagging annotation.
 *
 * By default the order is set to 50, failFast is set to false.
 *
 * The description can be used by documentation systems, or just to give the
 * programmer a clue of what is going on.
 *
 * It is possible to add many listeners to the same event, if that is the case
 * then the listeners will be executed in order given.
 *
 * You can change the execute time, to stop the executor grumbling about long
 * running methods.
 *
 * Slow long running means that a new low priority thread will be created for
 * each invocation of the event.
 *
 * @author dhudson - Apr 10, 2017 - 8:31:17 AM
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface OnEvent {

    int order() default 50;

    boolean failFast() default false;

    String description() default "";

    String executeTime() default "1s";

    public Class<? extends Throwable> expected() default NoneThrowable.class;

    boolean slowLongRunning() default false;

}
