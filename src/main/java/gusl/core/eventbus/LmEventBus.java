/*
 * Grownup Software Limited.
 */
package gusl.core.eventbus;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Interface for the internal Event Bus.
 *
 * @author dhudson - Mar 23, 2017 - 8:49:32 AM
 */
public interface LmEventBus {

    /**
     * Register a collection of listeners.
     *
     * @param listeners
     */
    public void registerAll(Collection<Object> listeners);

    /**
     * Register an object looking for OnEvent annotated methods
     *
     * @param object
     */
    public void register(Object object);

    /**
     * Remove the listener.
     *
     * @param object
     */
    public void unregister(Object object);

    /**
     * Post an event to the event bus.
     *
     * This is a async call as its executed by the service.
     *
     * @param event
     */
    public void post(Object event);

    /**
     * Post an event, and complete the future when the listeners have completed.
     *
     * @param event
     * @return
     */
    public CompletableFuture<Void> postWithFuture(Object event);

    /**
     * Post an event to the event bus, when the future is complete.
     *
     * @param event
     * @param future
     */
    public void postWhenComplete(Object event, Future<?> future);

    /**
     * Store the event before posting it.
     *
     * @param event
     * @throws EventBusRepositoryException
     */
    public void persistAndPost(Object event) throws EventBusRepositoryException;

    /**
     * Schedule the event in the future.
     *
     * Give the time to wait, once that has been reached, post the event on the
     * event bus.
     *
     * @param event
     * @param time
     * @param unit
     */
    public void scheduledPost(Object event, long time, TimeUnit unit);

    /**
     * Store and process in the future.
     *
     * @param event
     * @param time
     * @param unit
     * @throws EventBusRepositoryException
     */
    public void persistAndScheduledPost(Object event, long time, TimeUnit unit) throws EventBusRepositoryException;

    /**
     * Configure the Executor
     *
     * @param numberOfThreads
     */
    public void configure(int numberOfThreads);

    /**
     * Shutdown the Eventbus
     */
    public void shutdown();

    /**
     * Install the repository.
     *
     * This will load any existing / persisted events and add them to the queue.
     *
     * @param repository
     * @throws EventBusRepositoryException
     */
    public void installRepositoryHandler(EventBusRepository repository) throws EventBusRepositoryException;

    /**
     * Return the current size of the queue.
     *
     * @return
     */
    int getQueueSize();

    /**
     * Return the number of worker threads.
     *
     * @return
     */
    int getThreadPoolSize();

}
