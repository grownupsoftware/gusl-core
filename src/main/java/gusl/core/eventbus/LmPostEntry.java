package gusl.core.eventbus;

/**
 * Post Now event.
 *
 * @author dhudson
 */
public class LmPostEntry extends StorableQueueEntry {

    public LmPostEntry() {
    }

    @Override
    public boolean isReady() {
        return true;
    }

}
