package gusl.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD,
    ElementType.CONSTRUCTOR, ElementType.ANNOTATION_TYPE,
    ElementType.PACKAGE, ElementType.FIELD, ElementType.LOCAL_VARIABLE,})
@Inherited
/**
 *
 * @author dhudson
 */
public @interface DocSequence {

    int step() default 0;

    String group() default "";

    String description() default "";
}
