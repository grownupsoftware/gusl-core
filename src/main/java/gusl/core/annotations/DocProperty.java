/*
 * Grownup Software Limited.
 */
package gusl.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Property Annotation.
 *
 * Use this annotation for properties, which is then picked up by the
 * documentation service.
 *
 * @author dhudson
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface DocProperty {

    /**
     * Description of the field value
     *
     * @return description
     */
    String description();

    /**
     * All blast properties should have a sensible default value
     *
     * @return what the default value is.
     */
    String defaultValue() default "";
}
