package gusl.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD,
    ElementType.CONSTRUCTOR, ElementType.ANNOTATION_TYPE,
    ElementType.PACKAGE, ElementType.FIELD, ElementType.LOCAL_VARIABLE})
@Inherited

/**
 * Used to decorate code that should be removed.
 *
 * @author dhudson
 */
public @interface RemoveMe {

    String comment() default "";
}
