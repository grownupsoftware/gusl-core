package gusl.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * list denotes if the returning response will be a list of the returns type.
 *
 * @author grant
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface DocApi {

    String description();

    boolean list() default false;

    Class returnType() default DocApi.class;

    String[] errors() default "";
}
