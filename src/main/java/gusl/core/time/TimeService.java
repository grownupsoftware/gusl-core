package gusl.core.time;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

/**
 * @author dhudson
 * @since 31/10/2023
 */
public class TimeService {

    private TimeService() {
    }

    private static final DateTimeFormatter SAVINGS_FORMATTER = DateTimeFormatter.ofPattern("z");
    public static final TimeZone BST = TimeZone.getTimeZone("Europe/London");
    public static final TimeZone DST = TimeZone.getTimeZone("America/New_York");
    public static final TimeZone GMT = TimeZone.getTimeZone("GMT");
    public static final TimeZone CET = TimeZone.getTimeZone("Europe/Rome");
    public static final TimeZone UTC = TimeZone.getTimeZone("UTC");

    public static ZonedDateTime getTimeAt(LocalDateTime dateTime, TimeZone zone) {
        if (dateTime == null) {
            return null;
        }

        if (zone == null) {
            return dateTime.atZone(getSystemZone());
        }

        return dateTime.atZone(zone.toZoneId());
    }

    public static ZoneId getSystemZone() {
        return UTC.toZoneId();
    }

    public static ZonedDateTime getSystemTime() {
        return ZonedDateTime.now(getSystemZone());
    }


    public static TimeZone getESTDSTZone(LocalDateTime dateTime) {
        return TimeZone.getTimeZone(dateTime.atZone(DST.toZoneId()).format(SAVINGS_FORMATTER));
    }

    public static ZonedDateTime getZonedDateTimeAt(ZonedDateTime dateTime, TimeZone zone) {
        return dateTime.withZoneSameInstant(zone.toZoneId());
    }

    public static ZonedDateTime convertESTDST(ZonedDateTime dateTime) {
        if (dateTime.getZone().getId().equals(DST.getID())) {
            return getZonedDateTimeAt(dateTime, getESTDSTZone(dateTime.toLocalDateTime()));
        }
        return dateTime;
    }
}
