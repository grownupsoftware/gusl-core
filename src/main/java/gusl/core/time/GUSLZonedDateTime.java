package gusl.core.time;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.TimeZone;

/**
 * @author dhudson
 * @since 31/10/2023
 */
@Getter
@Setter
@NoArgsConstructor
public class GUSLZonedDateTime {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime dateTime;
    private String timeZone;

    public GUSLZonedDateTime(ZonedDateTime dateTime) {
        this.dateTime = dateTime.toLocalDateTime();
        this.timeZone = TimeZone.getTimeZone(dateTime.getZone()).getID();
    }

    @JsonIgnore
    public ZonedDateTime getZonedDateTime() {
        return ZonedDateTime.of(dateTime, TimeZone.getTimeZone(timeZone).toZoneId());
    }
}
