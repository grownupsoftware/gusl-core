package gusl.core.tostring;

/**
 *
 * @author dhudson
 */
public class ToString {

    public static String toString(Object object) {
        return internalToString(object, Object.class);
    }

    public static String toString(Object object, Class<?> upTo) {
        return internalToString(object, upTo);
    }

    private static String internalToString(Object object, Class<?> upTo) {
        if (object == null) {
            return "Null";
        }

        ToStringBuilder toStringBuilder = new ToStringBuilder();

        Class<?> clazz = object.getClass();

        toStringBuilder.build(new ToStringClassDetails(clazz), object);
        while (clazz.getSuperclass() != null && clazz != upTo) {
            clazz = clazz.getSuperclass();
            toStringBuilder.build(new ToStringClassDetails(clazz), object);
        }

        return toStringBuilder.getResult();
    }
}
