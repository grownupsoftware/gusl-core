package gusl.core.tostring;

import gusl.core.utils.StringUtils;

import java.lang.reflect.Field;
import java.util.Collection;

/**
 * Field Level detail.
 *
 * @author dhudson
 */
public class ToStringFieldDetail {

    private static final String MASK_STRING = "*!*!*";

    private final Field theField;
    private boolean isCount;
    private int summaryLength;
    private boolean isMask;
    private boolean ignoreIfNull;
    private boolean isTable;
    private String[] tableFields;

    ToStringFieldDetail(Field field) {
        theField = field;
    }

    Field getField() {
        return theField;
    }

    void setCount() {
        isCount = true;
    }

    void setMask() {
        isMask = true;
    }

    void setSummaryLength(int len) {
        summaryLength = len;
    }

    void setIgnoreIfNull() {
        ignoreIfNull = true;
    }

    boolean getIgnoreIfNull() {
        return ignoreIfNull;
    }

    Object processValue(Object source) throws IllegalAccessException {
        Object value = getValue(source);

        if (isCount) {
            if (value instanceof Collection) {
                return ((Collection) value).size();
            } else {
                return value;
            }
        }

        if (isTable) {
            if (value instanceof Collection) {
                TableGenerator generator = new TableGenerator();
                return generator.generateTable((Collection<Object>) value, tableFields);
            } else {
                return value;
            }
        }

        if (summaryLength > 0) {
            if (value != null) {
                String stringValue = value.toString();
                if (stringValue != null) {
                    if (stringValue.length() > summaryLength) {
                        return StringUtils.elipseString(summaryLength, stringValue);
                    }
                }
            }
        }

        if (isMask) {
            if (value != null) {
                String stringValue = value.toString();
                if (stringValue != null) {
                    return MASK_STRING;
                }
            }
        }
        return value;
    }


    private Object getValue(Object source) throws IllegalAccessException {
        return theField.get(source);
    }

    boolean isValueNull(Object source) throws IllegalAccessException {
        return (getValue(source) == null);
    }

    String getFieldName() {
        return theField.getName();
    }

    public void setTable(String[] fields) {
        isTable = true;
        tableFields = fields;
    }
}
