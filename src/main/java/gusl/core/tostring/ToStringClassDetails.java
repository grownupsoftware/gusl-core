package gusl.core.tostring;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * One of these created for each class for performance reasons.
 *
 * @author dhudson
 */
public class ToStringClassDetails {

    private static final char INNER_CLASS_SEPARATOR_CHAR = '$';

    private final List<ToStringFieldDetail> theFields;

    public ToStringClassDetails(Class<?> clazz) {

        ToStringIgnoreNull ignore = clazz.getAnnotation(ToStringIgnoreNull.class);
        boolean ignoringNullField = (ignore != null);

        theFields = new ArrayList<>(5);
        Field[] fields = clazz.getDeclaredFields();
        AccessibleObject.setAccessible(fields, true);
        for (Field field : fields) {
            if (include(field)) {
                ToStringFieldDetail detail = new ToStringFieldDetail(field);
                if (field.getAnnotation(ToStringCount.class) != null) {
                    detail.setCount();
                }
                if (field.getAnnotation(ToStringTable.class) != null) {
                    detail.setTable(field.getAnnotation(ToStringTable.class).fields());
                }

                ToStringSummary summary = field.getAnnotation(ToStringSummary.class);
                if (summary != null) {
                    detail.setSummaryLength(summary.length());
                }
                ToStringMask mask = field.getAnnotation(ToStringMask.class);
                if (mask != null) {
                    detail.setMask();
                }
                if (!ignoringNullField) {
                    ToStringIgnoreNull ignoreField = field.getAnnotation(ToStringIgnoreNull.class);
                    if (ignoreField != null) {
                        detail.setIgnoreIfNull();
                    }
                } else {
                    // Class level
                    detail.setIgnoreIfNull();
                }
                theFields.add(detail);
            }
        }
    }

    private boolean include(Field field) {
        if (field.getName().indexOf(INNER_CLASS_SEPARATOR_CHAR) != -1) {
            // Reject field from inner class.
            return false;
        }
        if (Modifier.isTransient(field.getModifiers())) {
            // Reject transient fields.
            return false;
        }
        if (Modifier.isStatic(field.getModifiers())) {
            // Reject static fields.
            return false;
        }

        // Reject if told to ignore
        return field.getAnnotation(ToStringIgnore.class) == null;
    }

    List<ToStringFieldDetail> getFieldDetails() {
        return theFields;
    }
}
