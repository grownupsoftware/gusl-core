/* Copyright lottomart */
package gusl.core.errors;

import java.util.List;

public class ErrorsDO {

    private List<ErrorDO> errors;

    public List<ErrorDO> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorDO> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "ErrorsDO{" + "errors=" + errors + '}';
    }

}
