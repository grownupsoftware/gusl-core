package gusl.core.errors;

import java.util.ArrayList;
import java.util.List;

public enum ErrorType {
    NOT_FOUND("NOT_FOUND"),
    VALIDATION("VALIDATION"),
    NO_CHANGE_NEEDED("NO_CHANGE_NEEDED"),
    SERVER("SERVER");

    private final String value;

    ErrorType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static List<String> names() {
        List<String> result = new ArrayList<>();
        for (ErrorType val : values()) {
            result.add(val.name());
        }
        return result;
    }
}
