package gusl.core.errors;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;

@DocClass(description = "A description of the error, at either field or system level")
public class ErrorDTO {

    @DocField(description = "Field name that provoked the error")
    private String field;

    @DocField(description = "Description of the error")
    private String message;

    @DocField(description = "Message key definition")
    private String messageKey;

    @DocField(description = "Parameters of the message")
    private String[] parameters;

    public ErrorDTO() {
    }

    public ErrorDTO(String field, String messageKey) {
        this.field = field;
        this.messageKey = messageKey;
    }

    public ErrorDTO(ErrorDO errorDo) {
        this.field = errorDo.getField();
        this.message = errorDo.getMessage();
        this.messageKey = errorDo.getMessageKey();
        this.parameters = errorDo.getParameters();
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String[] getParameters() {
        return parameters;
    }

    public void setParameters(String[] parameters) {
        this.parameters = parameters;
    }

    @Override
    public String toString() {
       return ToString.toString(this);
    }

}
