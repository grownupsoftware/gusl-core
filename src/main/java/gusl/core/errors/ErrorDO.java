package gusl.core.errors;

import gusl.core.annotations.DocField;

import java.util.Arrays;

import static java.util.Arrays.stream;

public class ErrorDO {

    @DocField(description = "Field name that provoked the error")
    private String field;

    @DocField(description = "Description of the error")
    private String message;

    @DocField(description = "Message key definition")
    private String messageKey;

    @DocField(description = "Parameters of the message")
    private String[] parameters;

    private boolean ignoreLogging;

    public ErrorDO() {
        this(null, null, null,  null);
    }

    public ErrorDO(String message, String messageKey) {
        this(null, message, messageKey, null);
    }


    public ErrorDO(String message, String messageKey, String[] parameters) {
        this(null, message, messageKey, parameters);
    }


    public ErrorDO(String field, String message, String messageKey, String[] parameters) {
        super();
        this.field = field;
        this.message = message;
        this.messageKey = messageKey;
        this.parameters = parameters;
        this.ignoreLogging = false;
    }

    public ErrorDO(boolean ignoreLogging,String field, String message, String messageKey, String[] parameters) {
        super();
        this.field = field;
        this.message = message;
        this.messageKey = messageKey;
        this.parameters = parameters;
        this.ignoreLogging = ignoreLogging;
    }

    public ErrorDO(String message, String messageKey, Object... parameters) {
        this(null, message, messageKey, parameters);
    }

    public ErrorDO(boolean ignoreLogging,String message, String messageKey, Object... parameters) {
        this(ignoreLogging, null, message, messageKey, parameters);
    }

    public ErrorDO(String field, String message, String messageKey, Object... parameters) {
        this(field, message, messageKey,
                stream(parameters).map(String::valueOf).toArray(String[]::new));
    }

    public ErrorDO(boolean ignoreLogging, String field, String message, String messageKey, Object... parameters) {
        this(field, message, messageKey,
                stream(parameters).map(String::valueOf).toArray(String[]::new));
    }


    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String[] getParameters() {
        return parameters;
    }

    public void setParameters(String[] parameters) {
        this.parameters = parameters;
    }

    public boolean isIgnoreLogging() {
        return ignoreLogging;
    }

    public void setIgnoreLogging(boolean ignoreLogging) {
        this.ignoreLogging = ignoreLogging;
    }

    @Override
    public String toString() {
        return "ErrorDO{"
                + "field='" + field + '\''
                + ", message='" + message + '\''
                + ", messageKey='" + messageKey + '\''
                + ", parameters=" + Arrays.toString(parameters)
                + '}';
    }
}
