package gusl.core.errors;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;

import java.util.ArrayList;
import java.util.List;

@DocClass(description = "A Collection of errors")
public class ErrorsDTO {

    @DocField(description = "A Collection of errors")
    private List<ErrorDTO> errors = new ArrayList<>(1);

    public ErrorsDTO() {
    }

    public ErrorsDTO(ErrorDO errorDo) {
        errors.add(new ErrorDTO(errorDo));
    }

    public List<ErrorDTO> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorDTO> errors) {
        this.errors = errors;
    }

    public boolean getError() {
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Errors [errors=");
        builder.append(errors);
        builder.append("]");
        return builder.toString();
    }
}
