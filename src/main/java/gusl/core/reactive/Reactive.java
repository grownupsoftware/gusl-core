package gusl.core.reactive;

import gusl.core.executors.NamedThreadFactory;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Utils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.function.Consumer;

/**
 * @author dhudson
 * @since 24/02/2021
 */
public class Reactive {

    private final GUSLLogger logger = GUSLLogManager.getLogger(Reactive.class);

    private static ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2, new NamedThreadFactory("Reactive"));

    private static final int DEFAULT_SIZE = 4;
    private final int defaultSize;
    private final List<Future<?>> futures;

    // Stop having to check if they are present, set to no ops
    private Consumer<Throwable> errorHandler = dummyErrorHandler();
    private Consumer<Future<?>> cancelHandler = dummyCancelHandler();
    private Runnable timeoutHandler;
    private Consumer<ReactiveResult> successHandler;

    // Used in Wait For
    private CountDownLatch latch = null;
    private ReactiveResult reactiveResult = null;
    private Future<?> runningFuture;

    private long endTime;
    private volatile boolean hasCancelled;

    // Two minutes maybe too long
    private long timeout = TimeUnit.MINUTES.toMillis(2);

    public Reactive(int size) {
        defaultSize = size;
        futures = new ArrayList<>(size);
    }

    public Reactive() {
        this(DEFAULT_SIZE);
    }

    public <T> Reactive addFuture(Future<T> future) {
        if (future != null) {
            futures.add(future);
        }
        return this;
    }

    public Reactive onError(Consumer<Throwable> handler) {
        if (handler != null) {
            errorHandler = handler;
        }
        return this;
    }

    public Reactive onCancel(Consumer<Future<?>> handler) {
        if (handler != null) {
            cancelHandler = handler;
        }
        return this;
    }

    public Reactive onTimeout(Runnable handler) {
        if (handler != null) {
            timeoutHandler = handler;
        }
        return this;
    }

    public void onSuccess(Consumer<ReactiveResult> handler) {
        successHandler = handler;
        endTime = System.currentTimeMillis() + timeout;
        runningFuture = executorService.submit(callable());
    }

    public Reactive timeout(Duration duration) {
        timeout = duration.toMillis();
        return this;
    }

    public Optional<ReactiveResult> waitForOptional() {
        return Optional.ofNullable(waitForResult());
    }

    /**
     * Any errors, and this will be null, otherwise return the result.
     *
     * @return
     */
    public ReactiveResult waitForResult() {
        latch = new CountDownLatch(1);

        // Lets trigger it
        onSuccess(result -> {
        });

        logger.debug("waiting for latch");
        try {
            latch.await();
        } catch (InterruptedException ex) {
            // Ignore
        }

        logger.debug("Returning result");
        return reactiveResult;
    }

    /**
     * Will cancel this process, as well as any futures as well.
     */
    public void cancel() {
        if (runningFuture != null) {
            runningFuture.cancel(true);
            handleCancel(runningFuture);
        }
        futures.forEach(future -> {
            future.cancel(true);
        });
    }

    private Consumer<Throwable> dummyErrorHandler() {
        return results -> {
        };
    }

    private Consumer<Future<?>> dummyCancelHandler() {
        return results -> {
        };
    }

    private void countdownIfRequired() {
        if (latch != null) {
            latch.countDown();
        }
    }

    private void handleCancel(Future<?> future) {
        if (!hasCancelled) {
            try {
                hasCancelled = true;
                cancelHandler.accept(future);
            } catch (Throwable t) {
                logger.warn("Exception caught in cancel consumer", t);
            } finally {
                countdownIfRequired();
            }
        }
    }

    private void handleError(Throwable t) {
        try {
            errorHandler.accept(t);
        } catch (Throwable t1) {
            logger.warn("Exception caught in error consumer", t1);
        } finally {
            countdownIfRequired();
        }
    }

    private void handleTimeout() {
        if (timeoutHandler != null) {
            try {
                timeoutHandler.run();
            } catch (Throwable t) {
                logger.warn("Exception caught in timeout runnable", t);
            } finally {
                countdownIfRequired();
            }
        } else {
            handleError(new TimeoutException());
        }
    }

    private void handleSuccess(ReactiveResult result) {
        try {
            reactiveResult = result;
            successHandler.accept(result);
        } catch (Throwable t) {
            logger.warn("Exception caught in success consumer", t);
        } finally {
            countdownIfRequired();
        }
    }

    private Runnable callable() {
        return () -> {

            boolean allComplete = true;
            for (Future<?> future : futures) {
                if (future.isCancelled()) {
                    handleCancel(future);
                    // No more
                    return;
                }

                if (!future.isDone()) {
                    allComplete = false;
                }
            }

            if (!allComplete) {
                if (endTime <= System.currentTimeMillis()) {
                    handleTimeout();
                    return;
                }

                logger.debug("Scheduling ...  ");
                // Let's wait a bit
                runningFuture = executorService.schedule(callable(), 100, TimeUnit.MILLISECONDS);
                return;
            }

            try {
                ReactiveResult result = new ReactiveResult(futures.size());

                // So everything done
                for (Future<?> future : futures) {
                    result.add(future.get());
                }
                handleSuccess(result);

            } catch (Throwable t) {
                //logger.warn("Exception caught when generating results ", t);
                countdownIfRequired();
                handleError(t.getCause());
            }
        };
    }

    /* -- Static Methods --*/
    public static Reactive from(Future<?> future) {
        return new Reactive().addFuture(future);
    }

    @FunctionalInterface
    public interface CompletableFutureWithExceptions<T> {
        T apply() throws Throwable;
    }

    public static Reactive from(CompletableFutureWithExceptions<Future<?>>... futures) {
        Reactive reactive = new Reactive();
        Utils.safeStream(futures).forEach(future -> {
            try {
                reactive.addFuture(future.apply());
            } catch (Throwable e) {
                // need to fix this
                throw new RuntimeException(e);
            }
        });
        return reactive;
    }

    public static Reactive from(Future<?>... futures) {
        Reactive reactive = new Reactive();
        for (Future<?> future : futures) {
            reactive.addFuture(future);
        }

        return reactive;
    }

    public static void process(Runnable runnable) {
        executorService.submit(runnable);
    }
}
