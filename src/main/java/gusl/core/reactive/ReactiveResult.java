package gusl.core.reactive;

import gusl.core.tostring.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dhudson
 * @since 24/02/2021
 */
public class ReactiveResult implements Serializable {

    private static final long serialVersionUID = 1L;

    private final List<Object> results;

    private int index = 0;

    public ReactiveResult(int size) {
        results = new ArrayList<>(size);
    }

    public int size() {
        return results.size();
    }

    public <T> T getIndexOf(int index) {
        return (T) results.get(index);
    }

    public <T> T getNext() {
        return (T) results.get(index++);
    }

    boolean add(Object o) {
        return results.add(o);
    }

    public void resetIndex() {
        index = 0;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
