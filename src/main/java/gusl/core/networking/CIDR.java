package gusl.core.networking;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringIgnore;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dhudson on 09/09/2019
 */
public class CIDR {

    private final String theCIDR;
    // Names are optional, and can also be used as grouping
    private final String theName;

    @ToStringIgnore
    private InetAddress theInetAddress;
    @ToStringIgnore
    private InetAddress theStartAddress;
    @ToStringIgnore
    private InetAddress theEndAddress;
    @ToStringIgnore
    private final int thePrefixLength;
    @ToStringIgnore
    private BigInteger theStart;
    @ToStringIgnore
    private BigInteger theEnd;


    public CIDR(String cidr) throws UnknownHostException {
        this(cidr, null);
    }

    public CIDR(String cidr, String name) throws UnknownHostException {
        theCIDR = cidr;
        theName = name;

        /* split CIDR to address and prefix part */
        if (theCIDR.contains("/")) {
            int index = this.theCIDR.indexOf("/");
            String addressPart = theCIDR.substring(0, index);
            String networkPart = theCIDR.substring(index + 1);

            theInetAddress = InetAddress.getByName(addressPart);
            thePrefixLength = Integer.parseInt(networkPart);

            calculate();
        } else {
            throw new IllegalArgumentException("not an valid CIDR format!");
        }
    }

    private void calculate() throws UnknownHostException {

        ByteBuffer maskBuffer;
        int targetSize;
        if (theInetAddress.getAddress().length == 4) {
            maskBuffer
                    = ByteBuffer
                    .allocate(4)
                    .putInt(-1);
            targetSize = 4;
        } else {
            maskBuffer = ByteBuffer.allocate(16)
                    .putLong(-1L)
                    .putLong(-1L);
            targetSize = 16;
        }

        BigInteger mask = (new BigInteger(1, maskBuffer.array())).not().shiftRight(thePrefixLength);

        ByteBuffer buffer = ByteBuffer.wrap(theInetAddress.getAddress());
        BigInteger ipVal = new BigInteger(1, buffer.array());

        BigInteger startIp = ipVal.and(mask);
        BigInteger endIp = startIp.add(mask.not());

        byte[] startIpArr = toBytes(startIp.toByteArray(), targetSize);
        byte[] endIpArr = toBytes(endIp.toByteArray(), targetSize);

        theStartAddress = InetAddress.getByAddress(startIpArr);
        theEndAddress = InetAddress.getByAddress(endIpArr);
        theStart = new BigInteger(1, this.theStartAddress.getAddress());
        theEnd = new BigInteger(1, this.theEndAddress.getAddress());
    }

    public String getLastAvailableAddress() {
        String[] octets = theEndAddress.toString().split("\\.");
        StringBuilder builder = new StringBuilder(50);
        builder.append(octets[0].substring(1));
        builder.append('.');
        builder.append(octets[1]);
        builder.append('.');
        builder.append(octets[2]);
        builder.append('.');
        builder.append(Integer.parseInt(octets[3]) - 1);
        return builder.toString();
    }

    private byte[] toBytes(byte[] array, int targetSize) {
        int counter = 0;
        List<Byte> newArr = new ArrayList<>();
        while (counter < targetSize && (array.length - 1 - counter >= 0)) {
            newArr.add(0, array[array.length - 1 - counter]);
            counter++;
        }

        int size = newArr.size();
        for (int i = 0; i < (targetSize - size); i++) {
            newArr.add(0, (byte) 0);
        }

        byte[] ret = new byte[newArr.size()];
        for (int i = 0; i < newArr.size(); i++) {
            ret[i] = newArr.get(i);
        }
        return ret;
    }

    public String getNetworkAddress() {
        return theStartAddress.getHostAddress();
    }

    public String getBroadcastAddress() {
        return theEndAddress.getHostAddress();
    }

    public boolean isInRange(String ipAddress) throws UnknownHostException {
        return isInRange(InetAddress.getByName(ipAddress));
    }

    public boolean isInRange(InetAddress ipAddress) {
        BigInteger target = new BigInteger(1, ipAddress.getAddress());

        int st = theStart.compareTo(target);
        int te = target.compareTo(theEnd);

        return (st == -1 || st == 0) && (te == -1 || te == 0);
    }

    public String getCIDR() {
        return theCIDR;
    }

    public String getName() {
        return theName;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
