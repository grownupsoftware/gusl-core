package gusl.core.networking;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Basic Firewall of whitelist and blacklist.
 *
 * @author dhudson on 09/09/2019
 */
public class Firewall {

    private List<CIDR> theWhiteList;
    private List<CIDR> theBlackList;

    public Firewall() {
        theWhiteList = new ArrayList<>(5);
        theBlackList = new ArrayList<>(5);
    }

    public void addCIDRToWhiteList(CIDR cidr) {
        theWhiteList.add(cidr);
    }

    public void addCIDRToWhiteList(String cidr) throws UnknownHostException {
        theWhiteList.add(new CIDR(cidr));
    }

    public void addCIDRToBlackList(CIDR cidr) {
        theBlackList.add(cidr);
    }

    public void addCIDRToBlackList(String cidr) throws UnknownHostException {
        theBlackList.add(new CIDR(cidr));
    }

    public boolean isAddressInWhiteList(InetAddress address) {
        if (isIn(address, theWhiteList) != null) {
            return true;
        }

        return false;
    }

    public boolean isAddressInBlackList(InetAddress address) {
        if (isIn(address, theBlackList) != null) {
            return true;
        }

        return false;
    }

    public boolean isAddressInWhiteList(String address) throws UnknownHostException {
        return isAddressInWhiteList(InetAddress.getByName(address));
    }

    public boolean isAddressInBlackList(String address) throws UnknownHostException {
        return isAddressInBlackList(InetAddress.getByName(address));
    }

    public CIDR whichCIDRInWhiteList(String address) throws UnknownHostException {
        return isIn(InetAddress.getByName(address), theWhiteList);
    }

    public CIDR whichCIDRInBlackList(String address) throws UnknownHostException {
        return isIn(InetAddress.getByName(address), theBlackList);
    }

    public CIDR whichCIDRInWhiteList(InetAddress address) {
        return isIn(address, theWhiteList);
    }

    public CIDR whichCIDRInBlackList(InetAddress address) {
        return isIn(address, theBlackList);
    }

    private CIDR isIn(InetAddress address, List<CIDR> list) {
        for (CIDR cidr : list) {
            if (cidr.isInRange(address)) {
                return cidr;
            }
        }

        return null;
    }

    public void replaceWhiteList(List<String> whiteList) throws UnknownHostException {
        List<CIDR> newWhiteList = new ArrayList<>();
        for (String cidrString : whiteList) {
            newWhiteList.add(new CIDR(cidrString));
        }

        theWhiteList = newWhiteList;
    }


    public void replaceBlackList(List<String> blackList) throws UnknownHostException {
        List<CIDR> newBlackList = new ArrayList<>();
        for (String cidrString : blackList) {
            newBlackList.add(new CIDR(cidrString));
        }

        theBlackList = newBlackList;
    }

    public List<CIDR> getWhiteList() {
        return theWhiteList;
    }

    public void setWhiteList(List<CIDR> whiteList) {
        theWhiteList = whiteList;
    }

    public List<CIDR> getBlackList() {
        return theBlackList;
    }

    public void setBlackList(List<CIDR> blackList) {
        theBlackList = blackList;
    }
}
