/*
 * Grownup Software Limited.
 */
package gusl.core.logging;

import org.apache.logging.log4j.core.Filter.Result;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.filter.AbstractFilter;

/**
 * Package Name Filter.
 *
 * @author dhudson - Mar 15, 2017 - 3:29:57 PM
 */
public class PackageNameFilter extends AbstractFilter {

    private final String theBasePackageName;

    public PackageNameFilter(String basePackage) {
        theBasePackageName = basePackage;
    }

    @Override
    public Result filter(LogEvent event) {
        if (event.getLoggerName().startsWith(theBasePackageName)) {
            return Result.ACCEPT;
        }
        return Result.DENY;
    }

}
