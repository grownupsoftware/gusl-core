package gusl.core.logging;

import gusl.core.exceptions.GUSLException;
import gusl.core.utils.Platform;
import org.apache.logging.log4j.core.LogEvent;

/**
 * Helper Class for Log Events.
 *
 * @author dhudson
 */
public class LogEventHelper {

    // No, no
    private LogEventHelper() {
    }

    public static String getMessageWithPossibleException(LogEvent logEvent) {
        if (logEvent.getThrown() == null) {
            return logEvent.getMessage().getFormattedMessage();
        }

        StringBuilder builder = new StringBuilder(300);
        builder.append(logEvent.getMessage().getFormattedMessage());

        builder.append(Platform.LINE_SEPARATOR);
        builder.append(GUSLException.getFullExceptionMessage(logEvent.getThrown(), true));

        return builder.toString();
    }
}
