/*
 * Grownup Software Limited.
 */
package gusl.core.logging;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.MessageFactory;
import org.apache.logging.log4j.util.Supplier;

/**
 * Log Wrapper, just incase we want to change logging.
 * <p>
 * Added temp, so that we know it temp logging and be culled if required.
 *
 * @author dhudson - Mar 15, 2017 - 1:50:02 PM
 */
@JsonIgnoreType
public class GUSLLogger {

    private static final Level AUDIT = Level.forName("AUDIT", 10);

    private final Logger theLogger;

    public GUSLLogger(Logger logger) {
        theLogger = logger;
    }

    public void debug(Marker marker, Message msg) {
        theLogger.debug(marker, msg);
    }

    public void debug(Marker marker, Message msg, Throwable thrwbl) {
        theLogger.debug(marker, msg, thrwbl);
    }

    public void debug(Marker marker, Object o) {
        theLogger.debug(marker, o);
    }

    public void debug(Marker marker, Object o, Throwable thrwbl) {
        theLogger.debug(marker, o, thrwbl);
    }

    public void debug(Marker marker, String string) {
        theLogger.debug(marker, string);
    }

    public void debug(Marker marker, String string, Object... os) {
        theLogger.debug(marker, string, os);
    }

    public void debug(Marker marker, String string, Throwable thrwbl) {
        theLogger.debug(marker, string, thrwbl);
    }

    public void debug(Message msg) {
        theLogger.debug(msg);
    }

    public void debug(Message msg, Throwable thrwbl) {
        theLogger.debug(msg, thrwbl);
    }

    public void debug(Object o) {
        theLogger.debug(o);
    }

    public void debug(Object o, Throwable thrwbl) {
        theLogger.debug(o, thrwbl);
    }

    public void debug(String string) {
        theLogger.debug(string);
    }

    public void debug(String string, Object... os) {
        theLogger.debug(string, os);
    }

    public void debug(String string, Throwable thrwbl) {
        theLogger.debug(string, thrwbl);
    }

    public void debug(String message, Supplier<?>... paramSuppliers) {
        theLogger.debug(message, paramSuppliers);
    }

    public void error(Marker marker, Message msg) {
        theLogger.error(marker, msg);
    }

    public void error(Marker marker, Message msg, Throwable thrwbl) {
        theLogger.error(marker, msg, thrwbl);
    }

    public void error(Marker marker, Object o) {
        theLogger.error(marker, o);
    }

    public void error(Marker marker, Object o, Throwable thrwbl) {
        theLogger.error(marker, o, thrwbl);
    }

    public void error(Marker marker, String string) {
        theLogger.error(marker, string);
    }

    public void error(Marker marker, String string, Object... os) {
        theLogger.error(marker, string, os);
    }

    public void error(Marker marker, String string, Throwable thrwbl) {
        theLogger.error(marker, string, thrwbl);
    }

    public void error(Message msg) {
        theLogger.error(msg);
    }

    public void error(Message msg, Throwable thrwbl) {
        theLogger.error(msg, thrwbl);
    }

    public void error(Object o) {
        theLogger.error(o);
    }

    public void error(Object o, Throwable thrwbl) {
        theLogger.error(o, thrwbl);
    }

    public void error(String string) {
        theLogger.error(string);
    }

    public void error(String string, Object... os) {
        theLogger.error(string, os);
    }

    public void error(String string, Throwable thrwbl) {
        theLogger.error(string, thrwbl);
    }

    public void error(String message, Supplier<?>... paramSuppliers) {
        theLogger.error(message, paramSuppliers);
    }

    public void fatal(Marker marker, Message msg) {
        theLogger.fatal(marker, msg);
    }

    public void fatal(Marker marker, Message msg, Throwable thrwbl) {
        theLogger.fatal(marker, msg, thrwbl);
    }

    public void fatal(Marker marker, Object o) {
        theLogger.fatal(marker, o);
    }

    public void fatal(Marker marker, Object o, Throwable thrwbl) {
        theLogger.fatal(marker, o, thrwbl);
    }

    public void fatal(Marker marker, String string) {
        theLogger.fatal(marker, string);
    }

    public void fatal(Marker marker, String string, Object... os) {
        theLogger.fatal(marker, string, os);
    }

    public void fatal(Marker marker, String string, Throwable thrwbl) {
        theLogger.fatal(marker, string, thrwbl);
    }

    public void fatal(Message msg) {
        theLogger.fatal(msg);
    }

    public void fatal(Message msg, Throwable thrwbl) {
        theLogger.fatal(msg, thrwbl);
    }

    public void fatal(Object o) {
        theLogger.fatal(o);
    }

    public void fatal(Object o, Throwable thrwbl) {
        theLogger.fatal(o, thrwbl);
    }

    public void fatal(String string) {
        theLogger.fatal(string);
    }

    public void fatal(String string, Object... os) {
        theLogger.fatal(string, os);
    }

    public void fatal(String string, Throwable thrwbl) {
        theLogger.fatal(string, thrwbl);
    }

    public void fatal(String message, Supplier<?>... paramSuppliers) {
        theLogger.fatal(message, paramSuppliers);
    }

    public Level getLevel() {
        return theLogger.getLevel();
    }

    public MessageFactory getMessageFactory() {
        return theLogger.getMessageFactory();
    }

    public String getName() {
        return theLogger.getName();
    }

    public void info(Marker marker, Message msg) {
        theLogger.info(marker, msg);
    }

    public void info(Marker marker, Message msg, Throwable thrwbl) {
        theLogger.info(marker, msg, thrwbl);
    }

    public void info(Marker marker, Object o) {
        theLogger.info(marker, o);
    }

    public void info(Marker marker, Object o, Throwable thrwbl) {
        theLogger.info(marker, o, thrwbl);
    }

    public void info(Marker marker, String string) {
        theLogger.info(marker, string);
    }

    public void info(Marker marker, String string, Object... os) {
        theLogger.info(marker, string, os);
    }

    public void info(Marker marker, String string, Throwable thrwbl) {
        theLogger.info(marker, string, thrwbl);
    }

    public void info(Message msg) {
        theLogger.info(msg);
    }

    public void info(Message msg, Throwable thrwbl) {
        theLogger.info(msg, thrwbl);
    }

    public void info(Object o) {
        theLogger.info(o);
    }

    public void info(Object o, Throwable thrwbl) {
        theLogger.info(o, thrwbl);
    }

    public void info(String string) {
        theLogger.info(string);
    }

    public void info(String string, Object... os) {
        theLogger.info(string, os);
    }

    public void info(String string, Throwable thrwbl) {
        theLogger.info(string, thrwbl);
    }

    public void info(String message, Supplier<?>... paramSuppliers) {
        theLogger.info(message, paramSuppliers);
    }

    public void temp(Marker marker, Message msg) {
        theLogger.info(marker, msg);
    }

    public void temp(Marker marker, Message msg, Throwable thrwbl) {
        theLogger.info(marker, msg, thrwbl);
    }

    public void temp(Marker marker, Object o) {
        theLogger.info(marker, o);
    }

    public void temp(Marker marker, Object o, Throwable thrwbl) {
        theLogger.info(marker, o, thrwbl);
    }

    public void temp(Marker marker, String string) {
        theLogger.info(marker, string);
    }

    public void temp(Marker marker, String string, Object... os) {
        theLogger.info(marker, string, os);
    }

    public void temp(Marker marker, String string, Throwable thrwbl) {
        theLogger.info(marker, string, thrwbl);
    }

    public void temp(Message msg) {
        theLogger.info(msg);
    }

    public void temp(Message msg, Throwable thrwbl) {
        theLogger.info(msg, thrwbl);
    }

    public void temp(Object o) {
        theLogger.info(o);
    }

    public void temp(Object o, Throwable thrwbl) {
        theLogger.info(o, thrwbl);
    }

    public void temp(String string) {
        theLogger.info(string);
    }

    public void temp(String string, Object... os) {
        theLogger.info(string, os);
    }

    public void temp(String string, Throwable thrwbl) {
        theLogger.info(string, thrwbl);
    }

    public boolean isDebugEnabled() {
        return theLogger.isDebugEnabled();
    }

    public boolean isDebugEnabled(Marker marker) {
        return theLogger.isDebugEnabled(marker);
    }

    public boolean isEnabled(Level level) {
        return theLogger.isEnabled(level);
    }

    public boolean isEnabled(Level level, Marker marker) {
        return theLogger.isEnabled(level, marker);
    }

    public boolean isErrorEnabled() {
        return theLogger.isErrorEnabled();
    }

    public boolean isErrorEnabled(Marker marker) {
        return theLogger.isErrorEnabled(marker);
    }

    public boolean isFatalEnabled() {
        return theLogger.isFatalEnabled();
    }

    public boolean isFatalEnabled(Marker marker) {
        return theLogger.isFatalEnabled(marker);
    }

    public boolean isInfoEnabled() {
        return theLogger.isInfoEnabled();
    }

    public boolean isInfoEnabled(Marker marker) {
        return theLogger.isInfoEnabled(marker);
    }

    public boolean isTraceEnabled() {
        return theLogger.isTraceEnabled();
    }

    public boolean isTraceEnabled(Marker marker) {
        return theLogger.isTraceEnabled(marker);
    }

    public boolean isWarnEnabled() {
        return theLogger.isWarnEnabled();
    }

    public boolean isWarnEnabled(Marker marker) {
        return theLogger.isWarnEnabled(marker);
    }

    public void log(Level level, Marker marker, Message msg) {
        theLogger.log(level, marker, msg);
    }

    public void log(Level level, Marker marker, Message msg, Throwable thrwbl) {
        theLogger.log(level, marker, msg, thrwbl);
    }

    public void log(Level level, Marker marker, Object o) {
        theLogger.log(level, marker, o);
    }

    public void log(Level level, Marker marker, Object o, Throwable thrwbl) {
        theLogger.log(level, marker, o, thrwbl);
    }

    public void log(Level level, Marker marker, String string) {
        theLogger.log(level, marker, string);
    }

    public void log(Level level, Marker marker, String string, Object... os) {
        theLogger.log(level, marker, string, os);
    }

    public void log(Level level, Marker marker, String string, Throwable thrwbl) {
        theLogger.log(level, marker, string, thrwbl);
    }

    public void log(Level level, Message msg) {
        theLogger.log(level, msg);
    }

    public void log(Level level, Message msg, Throwable thrwbl) {
        theLogger.log(level, msg, thrwbl);
    }

    public void log(Level level, Object o) {
        theLogger.log(level, o);
    }

    public void log(Level level, Object o, Throwable thrwbl) {
        theLogger.log(level, o, thrwbl);
    }

    public void log(Level level, String string) {
        theLogger.log(level, string);
    }

    public void log(Level level, String string, Object... os) {
        theLogger.log(level, string, os);
    }

    public void log(Level level, String string, Throwable thrwbl) {
        theLogger.log(level, string, thrwbl);
    }

    public void printf(Level level, Marker marker, String string, Object... os) {
        theLogger.printf(level, marker, string, os);
    }

    public void printf(Level level, String string, Object... os) {
        theLogger.printf(level, string, os);
    }

    public <T extends Throwable> T throwing(Level level, T t) {
        return theLogger.throwing(level, t);
    }

    public <T extends Throwable> T throwing(T t) {
        return theLogger.throwing(t);
    }

    public void trace(Marker marker, Message msg) {
        theLogger.trace(marker, msg);
    }

    public void trace(Marker marker, Message msg, Throwable thrwbl) {
        theLogger.trace(marker, msg, thrwbl);
    }

    public void trace(Marker marker, Object o) {
        theLogger.trace(marker, o);
    }

    public void trace(Marker marker, Object o, Throwable thrwbl) {
        theLogger.trace(marker, o, thrwbl);
    }

    public void trace(Marker marker, String string) {
        theLogger.trace(marker, string);
    }

    public void trace(Marker marker, String string, Object... os) {
        theLogger.trace(marker, string, os);
    }

    public void trace(Marker marker, String string, Throwable thrwbl) {
        theLogger.trace(marker, string, thrwbl);
    }

    public void trace(Message msg) {
        theLogger.trace(msg);
    }

    public void trace(Message msg, Throwable thrwbl) {
        theLogger.trace(msg, thrwbl);
    }

    public void trace(Object o) {
        theLogger.trace(o);
    }

    public void trace(Object o, Throwable thrwbl) {
        theLogger.trace(o, thrwbl);
    }

    public void trace(String string) {
        theLogger.trace(string);
    }

    public void trace(String string, Object... os) {
        theLogger.trace(string, os);
    }

    public void trace(String string, Throwable thrwbl) {
        theLogger.trace(string, thrwbl);
    }

    public void warn(Marker marker, Message msg) {
        theLogger.warn(marker, msg);
    }

    public void warn(Marker marker, Message msg, Throwable thrwbl) {
        theLogger.warn(marker, msg, thrwbl);
    }

    public void warn(Marker marker, Object o) {
        theLogger.warn(marker, o);
    }

    public void warn(Marker marker, Object o, Throwable thrwbl) {
        theLogger.warn(marker, o, thrwbl);
    }

    public void warn(Marker marker, String string) {
        theLogger.warn(marker, string);
    }

    public void warn(Marker marker, String string, Object... os) {
        theLogger.warn(marker, string, os);
    }

    public void warn(Marker marker, String string, Throwable thrwbl) {
        theLogger.warn(marker, string, thrwbl);
    }

    public void warn(Message msg) {
        theLogger.warn(msg);
    }

    public void warn(Message msg, Throwable thrwbl) {
        theLogger.warn(msg, thrwbl);
    }

    public void warn(Object o) {
        theLogger.warn(o);
    }

    public void warn(Object o, Throwable thrwbl) {
        theLogger.warn(o, thrwbl);
    }

    public void warn(String string) {
        theLogger.warn(string);
    }

    public void warn(String string, Object... os) {
        theLogger.warn(string, os);
    }

    public void warn(String string, Throwable thrwbl) {
        theLogger.warn(string, thrwbl);
    }

    public void warn(String message, Supplier<?>... paramSuppliers) {
        theLogger.warn(message, paramSuppliers);
    }

    public void audit(Marker marker, Message msg) {
        theLogger.log(AUDIT, marker, msg);
    }

    public void audit(Marker marker, Message msg, Throwable thrwbl) {
        theLogger.log(AUDIT, marker, msg, thrwbl);
    }

    public void audit(Marker marker, Object o) {
        theLogger.log(AUDIT, marker, o);
    }

    public void audit(Marker marker, Object o, Throwable thrwbl) {
        theLogger.log(AUDIT, marker, o, thrwbl);
    }

    public void audit(Marker marker, String string) {
        theLogger.log(AUDIT, marker, string);
    }

    public void audit(Marker marker, String string, Object... os) {
        theLogger.log(AUDIT, marker, string, os);
    }

    public void audit(Marker marker, String string, Throwable thrwbl) {
        theLogger.log(AUDIT, marker, string, thrwbl);
    }

    public void audit(Message msg) {
        theLogger.log(AUDIT, msg);
    }

    public void audit(Message msg, Throwable thrwbl) {
        theLogger.log(AUDIT, msg, thrwbl);
    }

    public void audit(Object o) {
        theLogger.log(AUDIT, o);
    }

    public void audit(Object o, Throwable thrwbl) {
        theLogger.log(AUDIT, o, thrwbl);
    }

    public void audit(String string) {
        theLogger.log(AUDIT, string);
    }

    public void audit(String string, Object... os) {
        theLogger.log(AUDIT, string, os);
    }

    public void audit(String string, Throwable thrwbl) {
        theLogger.log(AUDIT, string, thrwbl);
    }

    public void audit(String message, Supplier<?>... paramSuppliers) {
        theLogger.log(AUDIT, message, paramSuppliers);
    }

    @Override
    public int hashCode() {
        return theLogger.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return theLogger.equals(obj);
    }

    @Override
    public String toString() {
        return theLogger.toString();
    }

    public Logger getRawLogger() {
        return theLogger;
    }

    /**
     * String a string of a formatted message.
     *
     * @param messagePattern
     * @param argArray
     * @return
     */
    public String logMessage(final String messagePattern, final Object... argArray) {
        return LogMessageFormatter.format(messagePattern, argArray);
    }


}
