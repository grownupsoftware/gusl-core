/*
 * Grownup Software Limited.
 */
package gusl.core.logging;

import gusl.core.utils.StringUtils;
import gusl.core.utils.SystemPropertyUtils;
import lombok.CustomLog;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingFileAppender;
import org.apache.logging.log4j.core.appender.rolling.DefaultRolloverStrategy;
import org.apache.logging.log4j.core.appender.rolling.SizeBasedTriggeringPolicy;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.net.URI;
import java.util.Map;

/**
 * Used to control the logger.
 *
 * @author dhudson - Mar 15, 2017 - 1:50:18 PM
 */
@CustomLog
public class GUSLLogController {

    private static final String DEFAULT_PATTERN = "%d{dd-MMM-yyyy HH:mm:ss.SSS} %level [%t] %c{36} %m%n";

    private static LoggerContext theContext = (LoggerContext) LogManager.getContext(false);
    private static Configuration theConfig = theContext.getConfiguration();

    private GUSLLogController() {
    }

    /**
     * Reload config file.
     *
     * @param file
     */
    public static void reloadConfig(URI file) {
        theContext.setConfigLocation(file);
        theConfig = theContext.getConfiguration();
    }

    public static void setContext(LoggerContext context) {
        theContext = context;
        theConfig = context.getConfiguration();
    }

    /**
     * Let a property from the xml config file.
     *
     * @param key
     * @return
     */
    public static String getLogXmlProperty(String key) {
        return theConfig.getStrSubstitutor().getVariableResolver().lookup(key);
    }

    /**
     * Create a file appender for a given logger.
     *
     * @param logger
     */
    public static void createFileAppenderFor(GUSLLogger logger) {
        String baseName = /* getLogXmlProperty(LOG_BASE_KEY) */ SystemPropertyUtils.getLogBase() + "/" + StringUtils.getExtension(logger.getName());
        PatternLayout patternLayout = PatternLayout.newBuilder().withPattern(DEFAULT_PATTERN).build();
        SizeBasedTriggeringPolicy policy = SizeBasedTriggeringPolicy.createPolicy("25MB");
        DefaultRolloverStrategy strategy = DefaultRolloverStrategy.createStrategy("40", "1", "40", "0", null, false, theConfig);
        RollingFileAppender appender
                = RollingFileAppender.createAppender(baseName + ".log", baseName + "%i.log", "true", StringUtils.getExtension(logger.getName()), "true",
                "", "true", policy, strategy, patternLayout, new PackageNameFilter(logger.getName()), "", "", "", null);
        addAppenderToLog(logger, appender);
    }

    /**
     * Add an appender to a given logger, starting the appender if required.
     *
     * @param logger
     * @param appender
     */
    public static void addAppenderToLog(GUSLLogger logger, Appender appender) {
        LoggerConfig config = theConfig.getLoggerConfig(logger.getName());
        if (!appender.isStarted()) {
            appender.start();
        }

        config.addAppender(appender, Level.ALL, null);
        theContext.updateLoggers();
    }

    public synchronized static void shutdown() {
        theConfig.stop();
        theContext.stop();
    }

    public synchronized static void removeAppender(String name) {
        theConfig.getRootLogger().removeAppender(name);
    }

    public synchronized static boolean hasAppender(String name) {
        Map<String, Appender> appenders = theConfig.getRootLogger().getAppenders();
        if (appenders != null) {
            return appenders.containsKey(name);
        }
        return false;
    }

    public synchronized static void addAppender(Appender appender) {
        try {
            appender.start();
        } catch (Throwable t) {
            logger.warn("Unable to start log appender {} ", appender.getName(), t);
            return;
        }
        theConfig.addAppender(appender);
        theConfig.getRootLogger().addAppender(appender, Level.ALL, null);

        theContext.updateLoggers();

        logger.info("Log Appender {} appended", appender.getName());
    }

    public synchronized static void changeRootLogLevel(Level newLevel) {
        if (theConfig.getRootLogger().getLevel() != newLevel) {
            theConfig.getRootLogger().setLevel(newLevel);
            theContext.updateLoggers();

            logger.info("Log level changed to {} ", newLevel.name());
        }
    }

    public static Level getCurrentLevel() {
        return theConfig.getRootLogger().getLevel();
    }
}
