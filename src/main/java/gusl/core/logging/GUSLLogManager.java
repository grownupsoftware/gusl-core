/*
 * Grownup Software Limited.
 */
package gusl.core.logging;

import org.apache.logging.log4j.LogManager;

import java.util.HashMap;

/**
 * Wrap the logging using a delegate so if we want to swap it out in the future
 * we can.
 *
 * @author dhudson - Mar 15, 2017 - 1:50:37 PM
 */
public class GUSLLogManager {

    public final static GUSLLogger APPLICATION_LOGGER = new GUSLLogger(LogManager.getLogger(GUSLLogger.class.getName()));

    private static final HashMap<String, GUSLLogger> theLoggers = new HashMap<>();

    /**
     * Returns a logger which may be newly created, or returned from the
     * repository.
     *
     * @param clazz name to create the logger
     * @return a logger
     */
    public static GUSLLogger getLogger(Class<?> clazz) {
        return getLogger(clazz.getName());
    }

    /**
     * Returns a logger which may be newly created, or returned from the
     * repository.
     *
     * @param name
     * @return
     */
    public static GUSLLogger getLogger(String name) {
        GUSLLogger logger = theLoggers.get(name);

        if (logger == null) {
            logger = new GUSLLogger(LogManager.getLogger(name));
            theLoggers.put(name, logger);
        }

        return logger;
    }

    /**
     * Return the loggers.
     *
     * @return the loggers
     */
    public static HashMap<String, GUSLLogger> getLoggers() {
        return theLoggers;
    }

}
