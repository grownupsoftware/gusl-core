package gusl.core.lambda;

import java.io.Serializable;

/**
 *
 * @author dhudson
 */
public class MutableBoolean implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean theValue;

    public MutableBoolean() {
    }

    public MutableBoolean(boolean value) {
        theValue = value;
    }

    public boolean get() {
        return theValue;
    }

    public void set(boolean value) {
        theValue = value;
    }

    public boolean getAndToggle() {
        try {
            return theValue;
        } finally {
            theValue = !theValue;
        }
    }

    public boolean toggleAndGet() {
        theValue = !theValue;
        return theValue;
    }

    @Override
    public String toString() {
        return Boolean.toString(theValue);
    }
}
