package gusl.core.lambda;


/**
 *
 * @author dhudson
 */
public abstract class AbstractMutableNumber extends Number  {

    private static final long serialVersionUID = 1L;


    public AbstractMutableNumber() {
    }

    public abstract void increment();
    
    public abstract void decrement();
    
    public abstract void reset();
 
}
