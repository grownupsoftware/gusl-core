package gusl.core.lambda;

/**
 *
 * @author dhudson
 */
public class MutableLong extends AbstractMutableNumber {

    private static final long serialVersionUID = 1L;

    private long theValue;

    public MutableLong() {
    }

    public MutableLong(long value) {
        theValue = value;
    }

    public long get() {
        return theValue;
    }

    public void set(long value) {
        theValue = value;
    }

    public void add(long value) {
        theValue += value;
    }

    public void minus(long value) {
        theValue -= value;
    }

    @Override
    public void increment() {
        theValue++;
    }

    public long incrementAndGet() {
        return ++theValue;
    }

    public long getAndIncrement() {
        return theValue++;
    }

    @Override
    public void decrement() {
        theValue--;
    }

    public long decrementAndGet() {
        return --theValue;
    }

    public long getAndDecrement() {
        return theValue--;
    }

    @Override
    public void reset() {
        theValue = 0;
    }

    @Override
    public int intValue() {
        return (int) theValue;
    }

    @Override
    public long longValue() {
        return theValue;
    }

    @Override
    public float floatValue() {
        return (float) theValue;
    }

    @Override
    public double doubleValue() {
        return (double) theValue;
    }

    public boolean isDivisibleBy(int value) {
        return (theValue % value == 0);
    }

    public boolean incrementAndIsDivisibleBy(int value) {
        theValue++;
        return isDivisibleBy(value);
    }

    @Override
    public String toString() {
        return Long.toString(theValue);
    }

}
