package gusl.core.lambda;

import java.io.Serializable;

/**
 * @author dhudson
 * @since 12/07/2022
 */
public class MutableString implements Serializable {

    private static final long serialVersionUID = 1L;

    private String theValue;

    public MutableString() {
    }

    public MutableString(String initialValue) {
        theValue = initialValue;
    }

    public void set(String value) {
        theValue = value;
    }

    public String get() {
        return theValue;
    }

    public void setIfNotSet(String value) {
        if (theValue == null) {
            theValue = value;
        }
    }

    public boolean isDifferent(String value) {
        if (theValue == null && value == null) {
            return false;
        } else if (theValue == null || value == null) {
            return true;
        }
        return !theValue.equals(value);
    }

    @Override
    public String toString() {
        return theValue;
    }
}
