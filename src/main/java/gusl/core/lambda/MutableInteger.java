package gusl.core.lambda;

/**
 *
 * @author dhudson
 */
public class MutableInteger extends AbstractMutableNumber {

    private static final long serialVersionUID = 1L;

    private int theValue;

    public MutableInteger() {
    }

    public MutableInteger(int value) {
        theValue = value;
    }

    public int get() {
        return theValue;
    }

    public void set(int value) {
        theValue = value;
    }

    public void add(int value) {
        theValue += value;
    }

    public void minus(int value) {
        theValue -= value;
    }

    @Override
    public void increment() {
        theValue++;
    }

    public int incrementAndGet() {
        return ++theValue;
    }

    public int getAndIncrement() {
        return theValue++;
    }

    @Override
    public void decrement() {
        theValue--;
    }

    public int decrementAndGet() {
        return --theValue;
    }

    public int getAndDecrement() {
        return theValue--;
    }

    @Override
    public void reset() {
        theValue = 0;
    }

    @Override
    public int intValue() {
        return theValue;
    }

    @Override
    public long longValue() {
        return (long) theValue;
    }

    @Override
    public float floatValue() {
        return (float) theValue;
    }

    @Override
    public double doubleValue() {
        return (double) theValue;
    }

    public boolean isDivisibleBy(int value) {
        return (theValue % value == 0);
    }

    public boolean incrementAndIsDivisibleBy(int value) {
        theValue++;
        return isDivisibleBy(value);
    }

    @Override
    public String toString() {
        return Integer.toString(theValue);
    }
}
