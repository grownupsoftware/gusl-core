package gusl.core.lambda;

import java.io.Serializable;

/**
 * Once the switch has been toggled, it can't be changed.
 *
 * @author dhudson
 */
public class MutableSwitch implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean theOriginalState;
    private boolean theValue;

    public MutableSwitch() {
        this(false);
    }

    public MutableSwitch(boolean value) {
        theOriginalState = value;
        theValue = value;
    }

    public boolean get() {
        return theValue;
    }

    public boolean getAndToggle() {
        try {
            return theValue;
        } finally {
            if (theValue == theOriginalState) {
                theValue = !theValue;
            }
        }
    }

    public boolean toggleAndget() {
        if (theValue == theOriginalState) {
            theValue = !theValue;
        }

        return theValue;
    }

    @Override
    public String toString() {
        return Boolean.toString(theValue);
    }
}
