package gusl.core.lambda;

/**
 *
 * @author dhudson
 */
public class MutableDouble extends AbstractMutableNumber {

    private static final long serialVersionUID = 1L;

    private double theValue;

    public MutableDouble() {
    }

    public MutableDouble(double value) {
        theValue = value;
    }

    public double get() {
        return theValue;
    }

    public void set(double value) {
        theValue = value;
    }

    public void add(double value) {
        theValue += value;
    }

    public void minus(double value) {
        theValue -= value;
    }

    @Override
    public void increment() {
        theValue++;
    }

    public double incrementAndGet() {
        return ++theValue;
    }

    public double getAndIncrement() {
        return theValue++;
    }

    @Override
    public void decrement() {
        theValue--;
    }

    public double decrementAndGet() {
        return --theValue;
    }

    public double getAndDecrement() {
        return theValue--;
    }

    @Override
    public void reset() {
        theValue = 0;
    }

    @Override
    public int intValue() {
        return (int) theValue;
    }

    @Override
    public long longValue() {
        return (long) theValue;
    }

    @Override
    public float floatValue() {
        return (float) theValue;
    }

    @Override
    public double doubleValue() {
        return theValue;
    }

    @Override
    public String toString() {
        return Double.toString(theValue);
    }

}
