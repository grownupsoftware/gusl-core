package gusl.core.lambda;

import java.util.Date;

public class MutableDate {

    private static final long serialVersionUID = 1L;
    private Date theValue;

    public MutableDate() {
    }

    public MutableDate(Date value) {
        theValue = value;
    }

    public Date get() {
        return theValue;
    }

    public void set(Date value) {
        theValue = value;
    }

    public boolean isDifferent(Date value) {
        if (theValue == null && value == null) {
            return false;
        } else if (theValue == null || value == null) {
            return true;
        }
        return !theValue.equals(value);
    }

}
