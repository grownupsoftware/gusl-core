package gusl.core.lambda;

/**
 *
 * @author dhudson
 */
public class MutableFloat extends AbstractMutableNumber {

    private static final long serialVersionUID = 1L;

    private float theValue;

    public MutableFloat() {
    }

    public MutableFloat(float value) {
        theValue = value;
    }

    public float get() {
        return theValue;
    }

    public void set(float value) {
        theValue = value;
    }

    public void add(float value) {
        theValue += value;
    }

    public void minus(float value) {
        theValue -= value;
    }

    @Override
    public void increment() {
        theValue++;
    }

    public double incrementAndGet() {
        return ++theValue;
    }

    public double getAndIncrement() {
        return theValue++;
    }

    @Override
    public void decrement() {
        theValue--;
    }

    public float decrementAndGet() {
        return --theValue;
    }

    public float getAndDecrement() {
        return theValue--;
    }

    @Override
    public void reset() {
        theValue = 0;
    }

    @Override
    public int intValue() {
        return (int) theValue;
    }

    @Override
    public long longValue() {
        return (long) theValue;
    }

    @Override
    public float floatValue() {
        return theValue;
    }

    @Override
    public double doubleValue() {
        return (double) theValue;
    }

    @Override
    public String toString() {
        return Float.toString(theValue);
    }

}
