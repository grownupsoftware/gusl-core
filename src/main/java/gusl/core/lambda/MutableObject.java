package gusl.core.lambda;

import static java.util.Objects.nonNull;

public class MutableObject<T> {

    private static final long serialVersionUID = 1L;
    private T theValue;

    public MutableObject() {
    }

    public MutableObject(T value) {
        theValue = value;
    }

    public T get() {
        return theValue;
    }

    public void set(T value) {
        theValue = value;
    }

    public boolean isDifferent(T value) {
        if (theValue == null && value == null) {
            return false;
        } else if (theValue == null || value == null) {
            return true;
        }
        return !theValue.equals(value);
    }

    public boolean isPresent() {
        return nonNull(theValue);
    }

}
