package gusl.core.executors;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;

/**
 * Even when you have a thread factory that is suppose to report uncaught
 * exceptions, ScheduledThreadPoolExecutors don't. This has something to do with
 * the fact that if there is an exception the task stops.
 *
 * This means that the background task silently finishes with no trace
 * information, and that is poo. This class tries to fix that.
 *
 * @author dhudson
 */
public class ReportingThreadPoolExecutor extends ScheduledThreadPoolExecutor {

    private final GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    public ReportingThreadPoolExecutor(int corePoolSize) {
        super(corePoolSize);
    }

    public ReportingThreadPoolExecutor(int corePoolSize, ThreadFactory threadFactory) {
        super(corePoolSize, threadFactory);
    }

    @Override
    protected void beforeExecute(Thread thread, Runnable runnable) {
        super.beforeExecute(thread, runnable);
    }

    @Override
    protected void afterExecute(Runnable runnable, Throwable throwable) {
        super.afterExecute(runnable, throwable);

        if (runnable instanceof FutureTask<?>) {
            FutureTask<?> task = (FutureTask<?>) runnable;
            if (task.isDone()) {

                try {
                    ((FutureTask<?>) runnable).get();
                } // expose hidden exception, if any
                catch (ExecutionException xE) {
                    throwable = xE.getCause();
                } // cannot expose the wrapped runnable though, no such method
                catch (Exception ignore) {
                    // Interrupted or cancelled
                }

            }

            if (throwable != null) {
                logger.warn("Background thread pool caught exception ", throwable);
            }
        }
    }
}
