/*
 * Grownup Software Limited.
 */
package gusl.core.executors;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Simple Thread Naming Factory.
 *
 * @author dhudson - Mar 17, 2017 - 10:47:05 AM
 */
public class NamedThreadFactory implements ThreadFactory {

    private final ThreadFactory delegate = Executors.defaultThreadFactory();
    private final AtomicLong count = new AtomicLong(0);
    private final String name;
    private final boolean daemon;
    private final int priority;

    /**
     * @param name
     */
    public NamedThreadFactory(String name) {
        this(name, false, Thread.NORM_PRIORITY);
    }

    /**
     * @param name
     * @param daemon
     * @param priority
     */
    public NamedThreadFactory(String name, boolean daemon, int priority) {
        this.name = name;
        this.daemon = daemon;
        this.priority = priority;
    }

    /* (non-Javadoc)
     * @see java.util.concurrent.ThreadFactory#newThread(java.lang.Runnable)
     */
    @Override
    public Thread newThread(Runnable r) {
        Thread t = delegate.newThread(r);
        t.setName(name + "-" + count.incrementAndGet());
        t.setDaemon(daemon);
        t.setPriority(priority);
        return t;
    }

}
