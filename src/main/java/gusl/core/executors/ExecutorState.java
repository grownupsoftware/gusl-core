package gusl.core.executors;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * State model for the Executor.
 *
 * Probably a bit much to have in a separate class, but makes for easy reading.
 *
 * The runState provides the main lifecycle control.
 *
 * Taking on values:
 *
 * RUNNING: Accept new tasks and process queued tasks SHUTDOWN: Don't accept new
 * tasks, but process queued tasks STOP: Don't accept new tasks, don't process
 * queued tasks, and interrupt in-progress tasks TIDYING: All tasks have
 * terminated, workerCount is zero, the thread transitioning to state TIDYING
 * will run the terminated() hook method TERMINATED: terminated() has completed
 *
 * The numerical order among these values matters, to allow ordered comparisons.
 * The runState monotonically increases over time, but need not hit each state.
 * The transitions are:
 *
 * RUNNING -> SHUTDOWN On invocation of shutdown(), perhaps implicitly in
 * finalize() (RUNNING or SHUTDOWN) -> STOP On invocation of shutdownNow()
 * SHUTDOWN -> TIDYING When both queue and pool are empty STOP -> TIDYING When
 * pool is empty TIDYING -> TERMINATED When the terminated() hook method has
 * completed
 *
 * Threads waiting in awaitTermination() will return when the state reaches
 * TERMINATED.
 *
 * Detecting the transition from SHUTDOWN to TIDYING is less straightforward
 * than you'd like because the queue may become empty after non-empty and vice
 * versa during SHUTDOWN state.
 *
 * @author dhudson
 */
public class ExecutorState {

    private final AtomicInteger theState;

    public static final int RUNNING = -1;
    public static final int SHUTDOWN = 0;
    public static final int STOP = 1;
    public static final int TIDYING = 2;
    public static final int TERMINATED = 3;

    /**
     * Start state of running.
     */
    public ExecutorState() {
        theState = new AtomicInteger(RUNNING);
    }

    /**
     * Used for capturing state in at that instant.
     *
     * @param state
     */
    public ExecutorState(int state) {
        theState = new AtomicInteger(state);
    }

    public int getState() {
        return theState.get();
    }

    boolean runStateLessThan(int state) {
        return theState.get() < state;
    }

    public boolean runStateAtLeast(int state) {
        return theState.get() >= state;
    }

    public boolean isRunning() {
        return theState.get() < SHUTDOWN;
    }

    public void set(int state) {
        theState.set(state);
    }

    public boolean compareAndSet(int expected, int newState) {
        return theState.compareAndSet(expected, newState);
    }

    public boolean isShutdown() {
        return !isRunning();
    }

    /**
     * Returns true if this executor is in the process of terminating after
     * {@link #shutdown} or {@link #shutdownNow} but has not completely
     * terminated. This method may be useful for debugging. A return of
     * {@code true} reported a sufficient period after shutdown may indicate
     * that submitted tasks have ignored or suppressed interruption, causing
     * this executor not to properly terminate.
     *
     * @return {@code true} if terminating but not yet terminated
     */
    public boolean isTerminating() {
        return !isRunning() && runStateLessThan(TERMINATED);
    }

    public boolean isTerminated() {
        return (theState.get() == TERMINATED);
    }

}
