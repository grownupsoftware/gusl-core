package gusl.core.executors;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * General purpose low priority thread pool.
 *
 * @author dhudson
 */
public class BackgroundThreadPoolExecutor {

    private static final ScheduledExecutorService theService = new ReportingThreadPoolExecutor(2, new NamedThreadFactory("BackgroundThreadPool worker", false, Thread.MIN_PRIORITY + 1));

    public static void shutdown() {
        theService.shutdown();
    }

    public static boolean isShutdown() {
        return theService.isShutdown();
    }

    public static <T> Future<T> submit(Callable<T> task) {
        return theService.submit(task);
    }

    public static <T> Future<T> submit(Runnable task, T result) {
        return theService.submit(task, result);
    }

    public static Future<?> submit(Runnable task) {
        return theService.submit(task);
    }

    public static <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        return theService.invokeAll(tasks);
    }

    public static void execute(Runnable command) {
        theService.execute(command);
    }

    public static ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
        return theService.schedule(command, delay, unit);
    }

    public static <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
        return theService.schedule(callable, delay, unit);
    }

    public static ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
        return theService.scheduleAtFixedRate(command, initialDelay, period, unit);
    }

    public static ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
        return theService.scheduleWithFixedDelay(command, initialDelay, delay, unit);
    }

}
