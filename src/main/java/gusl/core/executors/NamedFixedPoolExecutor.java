/*
 * Grownup Software Limited.
 */
package gusl.core.executors;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Fixed Pool Executor, with thread names, and warm threads.
 *
 * @author dhudson - Mar 17, 2017 - 10:22:55 AM
 */
public class NamedFixedPoolExecutor extends ThreadPoolExecutor {

    public NamedFixedPoolExecutor(int size, String name) {
        super(size, size, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(),
                new NamedThreadFactory(name));
        // Lets warm them up please
        prestartAllCoreThreads();
    }

}
