package gusl.core.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Format to just two decimal places, as Java script will barf if too big.
 *
 * @author dhudson
 */
public class BigDecimalSerializer extends JsonSerializer<BigDecimal> {

    @Override
    public void serialize(BigDecimal t, JsonGenerator jg, SerializerProvider sp) throws IOException, JsonProcessingException {
        if (t == null) {
            jg.writeNull();
        } else {
            jg.writeString(t.setScale(2, RoundingMode.HALF_UP).toString());
        }
    }

}
