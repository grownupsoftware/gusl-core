package gusl.core.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.DecimalFormat;

/**
 * Stop JS NaN all of the time.
 *
 * @author dhudson
 * @since 16/03/2022
 */
public class JSDoubleSerializer extends JsonSerializer<Double> {

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat(".######");

    @Override
    public void serialize(Double t, JsonGenerator jg, SerializerProvider sp) throws IOException, JsonProcessingException {
        if (t == null) {
            jg.writeNull();
        } else {
            jg.writeString(DECIMAL_FORMAT.format(t));
        }
    }

}
