package gusl.core.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import gusl.core.security.PasswordStorage;
import lombok.SneakyThrows;

import java.io.IOException;

/**
 * @author dhudson
 * @since 08/10/2021
 */
public class SaltSerializer extends JsonSerializer<String> {

    @SneakyThrows
    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (value == null) {
            gen.writeNull();
        } else {
            if (PasswordStorage.isHashed(value)) {
                gen.writeString(value);
            } else {
                gen.writeString(PasswordStorage.createHash(value));
            }
        }
    }
}
