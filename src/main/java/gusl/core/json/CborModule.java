package gusl.core.json;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class CborModule extends SimpleModule {

    private static final long serialVersionUID = 7082718156977091983L;

    /**
     * Create a new CborModule.
     */
    public CborModule() {
        addSerializer(BigDecimal.class, new BigDecimalSerializer());
        addSerializer(BigInteger.class, new BigIntegerSerializer());
    }

    private static class BigDecimalSerializer extends JsonSerializer<BigDecimal> {

        /* (non-Javadoc)
         * @see com.fasterxml.jackson.databind.JsonSerializer#serialize(java.lang.Object, com.fasterxml.jackson.core.JsonGenerator, com.fasterxml.jackson.databind.SerializerProvider)
         */
        @Override
        public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
            gen.writeString(value.toString());
        }
    }

    private static class BigIntegerSerializer extends JsonSerializer<BigInteger> {

        /* (non-Javadoc)
         * @see com.fasterxml.jackson.databind.JsonSerializer#serialize(java.lang.Object, com.fasterxml.jackson.core.JsonGenerator, 
         * com.fasterxml.jackson.databind.SerializerProvider)
         */
        @Override
        public void serialize(BigInteger value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
            gen.writeString(value.toString());
        }
    }
}
