package gusl.core.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.introspect.AnnotationIntrospectorPair;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.TimeZone;


/**
 * Factory Class to create object mapper.
 * <p>
 * Object Mappers are thread safe
 *
 * @author dhudson
 */
public class ObjectMapperFactory {

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final TimeZone UTC_TZ = TimeZone.getTimeZone("UTC");

    public static final String SNAKE_CASE = "SNAKE_CASE";
    public static final String UPPER_CAMEL_CASE = "UPPER_CAMEL_CASE";
    public static final String LOWER_CAMEL_CASE = "LOWER_CAMEL_CASE";
    public static final String LOWER_CASE = "LOWER_CASE";
    public static final String KEBAB_CASE = "KEBAB_CASE";

    public static final String APPLICATION_DEFAULT = LOWER_CAMEL_CASE;

    public static final HashMap<String, ObjectMapper> OBJECT_MAPPERS = new HashMap<>();

    static {
        OBJECT_MAPPERS.put(SNAKE_CASE, createObjectMapper(PropertyNamingStrategy.SNAKE_CASE));
        OBJECT_MAPPERS.put(UPPER_CAMEL_CASE, createObjectMapper(PropertyNamingStrategy.UPPER_CAMEL_CASE));
        OBJECT_MAPPERS.put(LOWER_CAMEL_CASE, createObjectMapper(PropertyNamingStrategy.LOWER_CAMEL_CASE));
        OBJECT_MAPPERS.put(LOWER_CASE, createObjectMapper(PropertyNamingStrategy.LOWER_CASE));
        OBJECT_MAPPERS.put(KEBAB_CASE, createObjectMapper(PropertyNamingStrategy.KEBAB_CASE));
    }

    private ObjectMapperFactory() {
    }

    /**
     * Return the standard configured object mapper.
     * <p>
     * Object Mappers are thread safe, so there on no need to create more than
     * one unless you would like different features.
     *
     * @return the common object mapper.
     */
    public static ObjectMapper getDefaultObjectMapper() {
        return getObjectMapperWithNaming(APPLICATION_DEFAULT);
    }

    /**
     * Return an Shared Object Mapper, with of the naming strategy.
     *
     * @param namingStrategy
     * @return
     */
    public static ObjectMapper getObjectMapperWithNaming(String namingStrategy) {
        return OBJECT_MAPPERS.get(namingStrategy);
    }

    /**
     * Create a Object Mapper with lots of options set.
     * <p>
     * If you modify any properties, you should always create a new one.
     *
     * @param namingStrategy
     * @return a mapper with the default options set.
     */
    public static ObjectMapper createObjectMapper(PropertyNamingStrategy namingStrategy) {
        ObjectMapper mapper = new ObjectMapper();
        setOptions(mapper, namingStrategy);
        return mapper;
    }

    /**
     * Return an Object mapper with the default naming strategy set.
     *
     * @return
     */
    public static ObjectMapper createDefaultObjectMapper() {
        return createObjectMapper(PropertyNamingStrategy.KEBAB_CASE);
    }

    /**
     * Create a mapper with a given factory.
     *
     * @param factory
     * @param namingStrategy
     * @return
     */
    public static ObjectMapper createObjectMapper(JsonFactory factory, PropertyNamingStrategy namingStrategy) {
        ObjectMapper mapper = new ObjectMapper(factory);
        setOptions(mapper, namingStrategy);
        return mapper;
    }

    private static void setOptions(ObjectMapper mapper, PropertyNamingStrategy namingStrategy) {
        mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, true);

        // expect empty lists
        //mapper.disable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS);
        // can be replaced by this ...
        //mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        // Configure deserializer
        mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        mapper.configure(JsonParser.Feature.ALLOW_NUMERIC_LEADING_ZEROS, true);
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY,
                DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL);
        mapper.enable(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME);
        mapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE,
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper = mapper.setAnnotationIntrospector(new AnnotationIntrospectorPair(
                new JacksonAnnotationIntrospector(),
                new JaxbAnnotationIntrospector(TypeFactory.defaultInstance())));
        mapper.setDateFormat(new SimpleDateFormat(DATE_FORMAT));
        mapper.setPropertyNamingStrategy(namingStrategy);
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        mapper.setTimeZone(UTC_TZ);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(MapperFeature.IGNORE_DUPLICATE_MODULE_REGISTRATIONS, true);
        //mapper.findAndRegisterModules();
        // Java Time
        mapper.registerModule(new JavaTimeModule());
        mapper.registerModule(new Jdk8Module());

        //mapper.registerModule(new AfterburnerModule());
    }

    public static void installBigDecimalTwoPlacesSerialiser(ObjectMapper mapper) {
        SimpleModule module = new SimpleModule();
        module.addSerializer(BigDecimal.class, new BigDecimalSerializer());
        mapper.registerModule(module);
    }

    public static void installJSDoubleSerialiser(ObjectMapper mapper) {
        SimpleModule module = new SimpleModule();
        module.addSerializer(Double.class, new JSDoubleSerializer());
        module.addSerializer(double.class, new JSDoubleSerializer());
        mapper.registerModule(module);
    }

    public static <T> CharSequence prettyPrint(T value, ObjectMapper mapper) throws JsonProcessingException {
        if (value == null) {
            return "--null--";
        }

        StringBuilder builder = new StringBuilder(100);
        builder.append("[");
        builder.append(value.getClass().getSimpleName());
        builder.append("] ");
        builder.append(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(value));
        return builder.toString();
    }

    public static <T> CharSequence safePrettyPrint(T value) {
        return safePrettyPrint(value, getDefaultObjectMapper());
    }

    public static <T> CharSequence safePrettyPrint(T value, ObjectMapper mapper) {
        try {
            return prettyPrint(value, mapper);
        } catch (JsonProcessingException ex) {
            return new String("Unable to process " + value.getClass().getName() + ", reason" + ex.getMessage());
        }
    }

    // Use the default Kebab Case mapper
    public static <T> CharSequence prettyPrint(T value) throws JsonProcessingException {
        return prettyPrint(value, getDefaultObjectMapper());
    }
}
