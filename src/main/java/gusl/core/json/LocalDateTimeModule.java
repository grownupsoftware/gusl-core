package gusl.core.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * Experimental Module, I would advise not using.
 *
 * @author dhudson
 * @since 21/09/2021
 */
public class LocalDateTimeModule extends SimpleModule {

    public static final String DEFAULT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    private final ZoneOffset theZoneOffset;
    private final DateTimeFormatter theFormatter;

    public LocalDateTimeModule(ZoneOffset offset, String format) {
        theZoneOffset = offset;
        theFormatter = DateTimeFormatter.ofPattern(format);
        addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
    }

    public LocalDateTimeModule() {
        this(ZoneOffset.UTC, DEFAULT_FORMAT);
    }

    private class LocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {
        @Override
        public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(theFormatter.format(value.atZone(theZoneOffset)));
        }
    }
}
