package gusl.core.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import gusl.core.utils.IOUtils;
import gusl.core.utils.StringUtils;

/**
 *
 * @author dhudson
 */
public class JsonUtils {

    public static final int ELASTIC_MAX_SIZE = 32000;

    private JsonUtils() {
    }

    /**
     * Don't bust the logs when doing this.
     *
     * @param <T>
     * @param value
     * @param mapper
     * @return
     */
    public static <T> CharSequence limitedPrettyPrint(T value, ObjectMapper mapper) {
        return StringUtils.elipseString(ELASTIC_MAX_SIZE, String.valueOf(prettyPrint(value, mapper)));
    }

    public static <T> CharSequence prettyPrint(T value, ObjectMapper mapper) {
        if (value == null) {
            return "--null--";
        }

        StringBuilder builder = new StringBuilder(100);

        builder.append("[");
        builder.append(value.getClass().getSimpleName());
        builder.append("] ");
        try {
            builder.append(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(value));
        } catch (JsonProcessingException ex) {
            builder.append("--Error--");
        }
        return builder.toString();
    }

    // Use the default Kebab Case mapper
    public static <T> CharSequence prettyPrint(T value) {
        return prettyPrint(value, ObjectMapperFactory.getDefaultObjectMapper());
    }

    /**
     * Don't bust the logs when doing this.
     *
     * @param <T>
     * @param value
     * @return
     */
    public static <T> CharSequence limitedPrettyPrint(T value) {
        return StringUtils.elipseString(ELASTIC_MAX_SIZE, String.valueOf(prettyPrint(value)));
    }

    public static <T> T loadResourceObject(String path, Class<T> type) throws IOException {
        BufferedReader reader = IOUtils.createResourceReader(path, JsonUtils.class.getClassLoader());
        ObjectMapper mapper = ObjectMapperFactory.getDefaultObjectMapper();
        return mapper.readValue(reader, type);
    }

    public static <T> String storeObject(T object) throws IOException {
        File file = File.createTempFile("Json", ".json");
        ObjectMapperFactory.createDefaultObjectMapper().writeValue(file, object);
        return file.getAbsolutePath();
    }

    public static <T> void storeObject(T object, String path) throws IOException {
        File file = new File(path);
        internalStore(file, object, ObjectMapperFactory.getDefaultObjectMapper());
    }

    public static <T> void storeObject(T object, String path, ObjectMapper mapper) throws IOException {
        File file = new File(path);
        internalStore(file, object, mapper);
    }

    private static <T> void internalStore(File file, T object, ObjectMapper mapper) throws IOException {
        mapper.writeValue(file, object);
    }
}
