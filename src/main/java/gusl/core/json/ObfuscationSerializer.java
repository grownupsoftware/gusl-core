package gusl.core.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import gusl.core.security.ObfuscatedStorage;

import java.io.IOException;

/**
 * @author dhudson
 * @since 08/10/2021
 */
public class ObfuscationSerializer extends JsonSerializer<String> {

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (value == null) {
            gen.writeNull();
        } else {
            if (ObfuscatedStorage.isObfuscated(value)) {
                // Already encoded
                gen.writeString(value);
            } else {
                gen.writeString(ObfuscatedStorage.encrypt(value));
            }
        }
    }

}
