package gusl.core.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import gusl.core.utils.StringUtils;

import java.io.IOException;

/**
 * @author dhudson
 * @since 10/10/2022
 */
public class Base64Serializer extends JsonSerializer<String> {

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (value == null) {
            gen.writeNull();
        } else {
            gen.writeString(StringUtils.base64Encode(value.getBytes()));
        }
    }
}
