package gusl.core.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import gusl.core.utils.StringUtils;

import java.io.IOException;

/**
 * @author dhudson
 * @since 10/10/2022
 */
public class Base64Deserializer extends StdDeserializer<String> {

    public Base64Deserializer() {
        super(String.class);
    }

    @Override
    public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = p.getCodec().readTree(p);
        return new String(StringUtils.base64Decode(node.textValue()));
    }
}
