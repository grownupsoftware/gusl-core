/* Copyright Grownup  Software Limited */
package gusl.core.utils;

import lombok.CustomLog;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * @author grant
 */
@CustomLog
public class RandomObjectFiller {

    private static final Random random = new Random();

    public static <T> T createAndFill(Class<T> clazz) throws Exception {
        T instance = ClassUtils.getNewInstance(clazz);
        for (Field field : ClassUtils.getFieldsFor(clazz,true)) {
            Object value = getRandomValueForField(field);
            field.set(instance, value);
        }
        return instance;
    }

    private static Object getRandomValueForField(Field field) throws Exception {
        Class<?> type = field.getType();

        //logger.info("type-> {}",type.getCanonicalName());
        // Note that we must handle the different types here! This is just an 
        // example, so this list is not complete! Adapt this to your needs!
        if (type.isEnum()) {
            Object[] enumValues = type.getEnumConstants();
            return enumValues[random.nextInt(enumValues.length)];
        } else if (type.equals(Boolean.TYPE) || type.equals(Boolean.class)) {
            return random.nextBoolean();
        } else if (type.equals(Integer.TYPE) || type.equals(Integer.class)) {
            return random.nextInt();
        } else if (type.equals(Long.TYPE) || type.equals(Long.class)) {
            return random.nextLong();
        } else if (type.equals(Double.TYPE) || type.equals(Double.class)) {
            return random.nextDouble();
        } else if (type.equals(Float.TYPE) || type.equals(Float.class)) {
            return random.nextFloat();
        } else if (type.equals(String.class)) {
            return UUID.randomUUID().toString().substring(0, 9);
        } else if (type.equals(BigInteger.class)) {
            return BigInteger.valueOf(random.nextInt());
        } else if (type.equals(Date.class)) {
            return new Date();
        } else if (type.equals(BigDecimal.class)) {
            return BigDecimal.valueOf(random.nextInt());
        }
        return createAndFill(type);
    }
}
