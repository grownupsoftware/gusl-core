package gusl.core.utils;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Provides Lambda functions to re-throw any internal exceptions that can then
 * be caught outside the lambda statement
 *
 * @author grant
 */
public class LambdaExceptionHelper {

    @FunctionalInterface
    public interface ConsumerWithExceptions<T, E extends Exception> {

        void accept(T t) throws E;
    }

    @FunctionalInterface
    public interface BiConsumerWithExceptions<T, U, E extends Exception> {

        void accept(T t, U u) throws E;
    }

    @FunctionalInterface
    public interface FunctionWithExceptions<T, R, E extends Exception> {

        R apply(T t) throws E;
    }

    @FunctionalInterface
    public interface SupplierWithExceptions<T, E extends Exception> {

        T get() throws E;
    }

    @FunctionalInterface
    public interface RunnableWithExceptions<E extends Exception> {

        void run() throws E;
    }

    @FunctionalInterface
    public interface MapWithExceptions<T, K, R, E extends Exception> {

        R apply(T t) throws E;
    }

    public static <T> boolean rethrowOptionalPresent(CompletableFuture<Optional<T>> future) {
        try {
            return future.get().isPresent();
        } catch (Exception e) {
            throwAsUnchecked(e);
            return false;
        }
    }

    public static <T> boolean rethrowOptionalListPresent(CompletableFuture<Optional<List<T>>> future) {
        try {
            return future.get().isPresent();
        } catch (Exception e) {
            throwAsUnchecked(e);
            return false;
        }
    }

    public static <T> boolean rethrowListPresent(CompletableFuture<List<T>> future) {
        try {
            return future.get() != null;
        } catch (Exception e) {
            throwAsUnchecked(e);
            return false;
        }
    }

    /**
     * Example Usage: .forEach(rethrowConsumer(name ->
     * System.out.println(Class.forName(name)))); or
     * .forEach(rethrowConsumer(ClassNameUtil::println));
     *
     * @param <T> the type
     * @param <E> the exception
     * @param consumer the functional interface consumer
     * @return instance of type
     */
    public static <T, E extends Exception> Consumer<T> rethrowConsumer(ConsumerWithExceptions<T, E> consumer) {
        return t -> {
            try {
                consumer.accept(t);
            } catch (Exception exception) {
                throwAsUnchecked(exception);
            }
        };
    }

    /**
     *
     * @param <T> the type of the first parameter
     * @param <U> the type of the second parameter
     * @param <E> the exception
     * @param biConsumer the functional interface consumer
     * @return
     */
    public static <T, U, E extends Exception> BiConsumer<T, U> rethrowBiConsumer(BiConsumerWithExceptions<T, U, E> biConsumer) {
        return (t, u) -> {
            try {
                biConsumer.accept(t, u);
            } catch (Exception exception) {
                throwAsUnchecked(exception);
            }
        };
    }

//    /**
//     * .map(rethrowFunction(name -> Class.forName(name))) or
//     * .map(rethrowFunction(Class::forName))
//     */
    /**
     *
     * @param <T> the type
     * @param <R> the return type
     * @param <E> the exception
     * @param function the functional interface function
     * @return
     */
    public static <T, R, E extends Exception> Function<T, R> rethrowFunction(FunctionWithExceptions<T, R, E> function) {
        return t -> {
            try {
                return function.apply(t);
            } catch (Exception exception) {
                throwAsUnchecked(exception);
                return null;
            }
        };
    }

    /**
     *
     * @param <T> the type
     * @param <K> the key value
     * @param <R> the return type
     * @param <E> the exception
     * @param function the functional interface function
     * @return
     */
    public static <T, K, R, E extends Exception> Function<T, R> rethrowCollector(MapWithExceptions<T, K, R, E> function) {
        return t -> {
            try {
                return function.apply(t);
            } catch (Exception exception) {
                throwAsUnchecked(exception);
                return null;
            }
        };
    }

//    /**
//     * rethrowSupplier(() -> new StringJoiner(new String(new byte[]{77, 97, 114,
//     * 107}, "UTF-8"))),
//     */
    /**
     *
     * @param <T> the type
     * @param <E> the exception
     * @param function the functional interface function
     * @return
     */
    public static <T, E extends Exception> Supplier<T> rethrowSupplier(SupplierWithExceptions<T, E> function) {
        return () -> {
            try {
                return function.get();
            } catch (Exception exception) {
                throwAsUnchecked(exception);
                return null;
            }
        };
    }

    @SuppressWarnings("unchecked")
    private static <E extends Throwable> void throwAsUnchecked(Exception exception) throws E {
        throw (E) exception;
    }

}
