/* Copyright Grownup  Software Limited */
package gusl.core.utils;

import lombok.CustomLog;

import java.time.Clock;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Generate ID's unique across nodes.
 * <p>
 * There is a chance of number over-run, but for this to happen, you would have
 * to generate more IDs than there are milliseconds during the running on of the
 * VM.
 *
 * @author dhudson
 */
@CustomLog
public class IdGenerator {

    // The day Casanova was born.                
    private static final long BORN_EPOCH = 1496962800000L; // 2017-06-09

    private static final long MAX_JAVASCRIPT_ID = 999_999_999_999_999l;

    // Node Ids will come from the Router, but allow for a testing one.
    private static final int TESTING_NODE_ID = 99;
    private static final int MAX_NODE_ID = 88;

    private static final Clock CLOCK = Clock.systemUTC();

    private static final AtomicLong theCorrelationId = new AtomicLong(CLOCK.millis());

    private static int theNodeId;

    private static AtomicLong theUniqueId;

    /**
     * Used in testing.
     */
    public static void setForTesting() {
        theNodeId = TESTING_NODE_ID;
        seed(TESTING_NODE_ID);
    }

    /**
     * This should be the node ID from registration with the router.
     * <p>
     * Nodes need to be less than 88 as 10 is added to the node ID.
     *
     * @param uniqueId
     */
    public static void setNodeUniqueId(int uniqueId) {
        theNodeId = uniqueId;
        seed(uniqueId);
    }

    public static void setIfNotSet() {
        if (theNodeId == 0) {
            setForTesting();
        }
    }

    /**
     * Return a unique ID for this VM.
     *
     * @return unique ID for this VM
     */
    public static long generateCorrelationId() {
        return theCorrelationId.getAndIncrement();
    }

    public static String generateUniqueNodeIdAsString() {
        return Long.toString(generateUniqueNodeId(), 32);
    }

    public static long generateUniqueNodeId() {
        return generateId();
    }

    private static void seed(int nodeId) {
        int newNode = nodeId;
        if (nodeId != TESTING_NODE_ID) {
            if (nodeId > MAX_NODE_ID || nodeId < 1) {
                throw new RuntimeException("Invalid node ID 1 - 99 valid");
            }
            // This solves the sub 10 node IDs
            newNode = nodeId + 10;
        }

        StringBuilder builder = new StringBuilder(20);
        builder.append(newNode);
        builder.append(Clock.systemUTC().millis() - BORN_EPOCH);

        theUniqueId = new AtomicLong(Long.parseLong(builder.toString()));
    }

    /**
     * Generate a numeric id of the form
     *
     * @return
     */
    private static long generateId() {
        return theUniqueId.incrementAndGet();
    }

    /**
     * Return random UUID.
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString();
    }

    public static int getNodeId() {
        return theNodeId;
    }
}
