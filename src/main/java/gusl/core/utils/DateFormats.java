package gusl.core.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A Collection of Thread Local Date Formats.
 *
 * @author dhudson
 */
public class DateFormats {

    private final static Map<String, ThreadLocal<SimpleDateFormat>> theDateFormats = new ConcurrentHashMap<>();
    private final static Map<String, ThreadLocal<SimpleDateFormat>> theLenientFormats = new ConcurrentHashMap<>();
    private final static Map<String, ThreadLocal<SimpleDateFormat>> theUTCFormats = new ConcurrentHashMap<>();

    public static final String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    public static final String NULL_DATE = "~~null~~";

    private DateFormats() {
    }

    /**
     * Return a simple date format for format yyyy-MM-dd'T'HH:mm:ss.SSS'Z' and
     * the time zone set to UTC.
     *
     * @return
     */
    public static SimpleDateFormat getISODateFormat() {
        return getUTCFormatFor(ISO_DATE_FORMAT);
    }

    /**
     * Return a thread safe simple date format for the given format, with
     * lenient set to true.
     *
     * @param format
     * @return
     */
    public static SimpleDateFormat getLenientFormatFor(String format) {
        return getDateFormat(format, theLenientFormats, true, false);
    }

    /**
     * Return a thread safe simple date format, with lenient set to false.
     *
     * @param format
     * @return
     */
    public static SimpleDateFormat getFormatFor(String format) {
        return getDateFormat(format, theDateFormats, false, false);
    }

    public static SimpleDateFormat getUTCFormatFor(String format) {
        return getDateFormat(format, theUTCFormats, false, true);
    }

    /**
     * Format the date with the given date format, performing a null check along
     * the way.
     *
     * @param format
     * @param date
     * @return the text ~~null~~ or the formated date.
     */
    public static String formatDate(String format, Date date) {
        if (date == null) {
            return NULL_DATE;
        }

        return getFormatFor(format).format(date);
    }

    /**
     * Format the date with the given date format, performing a null check along
     * the way, using the UTC timezone and not the system timezone
     *
     * @param format
     * @param date
     * @return the text ~~null~~ or the formated date.
     */
    public static String formatUTCDate(String format, Date date) {
        if (date == null) {
            return NULL_DATE;
        }

        return getUTCFormatFor(format).format(date);
    }

    private static SimpleDateFormat getDateFormat(String format, Map<String, ThreadLocal<SimpleDateFormat>> map, boolean lenient, boolean utc) {
        ThreadLocal<SimpleDateFormat> dateLocal = map.get(format);
        if (dateLocal == null) {
            dateLocal = new ThreadLocal<SimpleDateFormat>() {
                @Override
                protected SimpleDateFormat initialValue() {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
                    simpleDateFormat.setLenient(lenient);
                    if (utc) {
                        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    }
                    return simpleDateFormat;
                }
            };
            map.put(format, dateLocal);
        }

        return dateLocal.get();
    }

}
