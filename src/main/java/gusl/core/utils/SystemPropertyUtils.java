package gusl.core.utils;

/**
 * Try and keep all System and Env properties here.
 *
 * @author dhudson
 */
public class SystemPropertyUtils {

    /**
     * Launcher will set this so that the application node knows which port its
     * running on.
     */
    public static final String PORT_KEY = "ApplicationPort";

    /**
     * Set this for extra Debug Information
     */
    public static final String APPLICATION_DEBUG_KEY = "ApplicationDebug";

    public static final String OPERATIONAL_MODE_KEY = "ApplicationMode";

    public static final String CONFIG_LOCATION_KEY = "ApplicationConfig";

    public static final String LOGS_LOCATION_KEY = "ApplicationLogs";

    public static final String REPO_LOCATION_KEY = "ApplicationRepository";

    public static final String CONSTRUCT_KEY = "ApplicationConstruct";

    public static final String APPLICATION_REGISTRA_KEY = "ApplicationRegistra";

    public static final String INSTANCE_KEY = "ApplicationInstance";

    public static final String RESTRICT_KEY = "ApplicationRestrict";

    public static final String VAULT_URL = "ApplicationVaultUrl";

    public static final String VAULT_TOKEN = "ApplicationVaultToken";
    /**
     * Used for logging
     */
    private static boolean isDebugging;

    private static final String theOperationalMode;

    private static final String theConstructName;

    /**
     * COM, IE, or UK
     */
    private static final String theInstanceType;

    private static final boolean isRestricted;

    static {
        isDebugging = getBooleanProperty(APPLICATION_DEBUG_KEY);
        theOperationalMode = getProperty(OPERATIONAL_MODE_KEY);
        theConstructName = getProperty(CONSTRUCT_KEY);
        theInstanceType = getProperty(INSTANCE_KEY);
        isRestricted = getBooleanProperty(RESTRICT_KEY);
    }

    private SystemPropertyUtils() {
    }

    public static String getProperty(String key) {
        String property = System.getenv(key);

        if (!StringUtils.isBlank(property)) {
            // System has preference
            return property;
        }

        return System.getProperty(key);
    }

    public static boolean getBooleanProperty(String key) {
        String value = getProperty(key);
        if (StringUtils.isBlank(value)) {
            return false;
        }

        return Boolean.parseBoolean(value);
    }

    public static String getRepositoryLocation() {
        return getProperty(REPO_LOCATION_KEY);
    }

    public static final String getOperationalMode() {
        return theOperationalMode;
    }

    public static final String getConstructName() {
        return theConstructName;
    }

    public static void setAsyncLogging() {
        // Set up the disruptor logging
        System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
    }

    public static int getApplicationPort() {
        return Integer.parseInt(getProperty(PORT_KEY));
    }

    public static void setDebugging(boolean value) {
        isDebugging = value;
    }

    /**
     * Should always ask this method because debugging can be changed at
     * runtime.
     *
     * @return true if debugging
     */
    public static boolean isDebugging() {
        return isDebugging;
    }

    public static String getLogBase() {
        return getProperty(LOGS_LOCATION_KEY);
    }

    public static boolean hasConfigLocation() {
        return !StringUtils.isBlank(getGivenConfig());
    }

    public static String getGivenConfig() {
        return getProperty(CONFIG_LOCATION_KEY);
    }

    public static String getInstanceType() {
        return theInstanceType;
    }

    public static boolean isRestricted() {
        return isRestricted;
    }

    public static String getVaultUrl() {
        return getProperty(VAULT_URL);
    }

    public static String getVaultToken() {
        return getProperty(VAULT_TOKEN);
    }

    public static void setVaultParams(String url, String token) {
        System.setProperty(VAULT_URL, url);
        System.setProperty(VAULT_TOKEN, token);
    }

    public static boolean isProduction() {
        return "PROD".equals(getOperationalMode());
    }
}
