package gusl.core.utils;

import lombok.CustomLog;
import lombok.NonNull;

import java.lang.reflect.Field;
import java.util.Comparator;

/**
 * @author dhudson
 * @since 22/08/2022
 */
@CustomLog
public class FieldComparator<T> implements Comparator<T> {

    private final Field theField;

    public FieldComparator(@NonNull Field field) {
        theField = field;
    }

    @Override
    public int compare(T o1, T o2) {
        if (o1 == null || o2 == null) {
            return 0;
        }

        try {
            Object oneValue = theField.get(o1);
            Object otherValue = theField.get(o2);

            if (oneValue == null || otherValue == null) {
                return 0;
            }

            if (oneValue instanceof Comparable) {
                return ((Comparable<Object>) oneValue).compareTo(otherValue);
            }
        } catch (Throwable t) {
            logger.warn("FieldComparator error for {} ... ", theField.getName(), t);
        }
        return 0;
    }
}
