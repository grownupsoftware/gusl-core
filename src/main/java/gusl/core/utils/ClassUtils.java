package gusl.core.utils;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import org.reflections.Reflections;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

/**
 * @author dhudson on 20/05/2020
 */
public class ClassUtils {
    protected final static GUSLLogger logger = GUSLLogManager.getLogger(ClassUtils.class);

    private ClassUtils() {
    }

    public static List<String> getFieldNamesFor(Class<?> clazz) {
        Set<Field> fields = getFieldsFor(clazz, false);
        List<String> fieldNames = new ArrayList<>(fields.size());
        fields.forEach(field -> fieldNames.add(field.getName()));
        return fieldNames;
    }

    public static Map<String, Field> getFieldForAsMap(Class<?> clazz, boolean accessRequired) {
        Map<String, Field> fieldMap = new HashMap<>();
        getFieldsFor(clazz, accessRequired).forEach(field -> fieldMap.put(field.getName(), field));
        return fieldMap;
    }

    /**
     * Get fields for a class, including all sub classes.
     *
     * @param clazz
     * @return
     */
    public static Set<Field> getFieldsFor(Class<?> clazz, boolean accessRequired) {

        Set<Field> fields = new LinkedHashSet<>();
        walkFields(clazz, fields, accessRequired);

        Class<?> parentClass = clazz.getSuperclass();

        while (parentClass != null) {
            walkFields(parentClass, fields, accessRequired);
            parentClass = parentClass.getSuperclass();
        }

        return fields;
    }

    private static void walkFields(Class<?> clazz, Set<Field> fields, boolean accessRequired) {
        Field[] javaFields = clazz.getDeclaredFields();

        for (int i = 0; i < javaFields.length; i++) {
            Field field = javaFields[i];
            if (field.getName().startsWith("$") || field.getName().equals("serialVersionUID")) {
                continue;
            }
            if (field.isSynthetic() || Modifier.isStatic(field.getModifiers())) {
                continue;
            }
            if (accessRequired) {
                field.setAccessible(true);
            }
            fields.add(field);
        }
    }

    /**
     * Try and return the empty constructor instance of the given class.
     *
     * @param clazz
     * @param <T>
     * @return
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     */
    public static <T> T getNewInstance(Class<T> clazz) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor[] ctrs = clazz.getDeclaredConstructors();
        for (int i = 0; i < ctrs.length; i++) {
            if (ctrs[i].getGenericParameterTypes().length == 0) {
                return (T) ctrs[i].newInstance();
            }
        }

        return null;
    }

    private static Reflections getReflections() {
        ConfigurationBuilder builder = new ConfigurationBuilder();
        // Need to add new projects here ...
        for (String prefix : new String[]{"gusl", "supernova", "com.smarttech"}) {
            builder.addUrls(ClasspathHelper.forPackage(prefix));
        }

//        builder.addUrls(ClasspathHelper.forPackage(""));
        builder.addScanners(new TypeAnnotationsScanner());
        try {
            return new Reflections(builder);
        } catch (IllegalStateException ex) {
            // Try again ...
            return new Reflections(builder);
        }
    }

    public static Set<Class<?>> getAnnotatedClasses(Class<? extends Annotation> annotatedClass) {
        return getReflections().getTypesAnnotatedWith(annotatedClass);
    }

    public static <T> Map<String, Method> getMapOfSetterMethodByFieldName(Class<T> dataClass) {
        final List<String> fieldNames = safeStream(ClassUtils.getFieldsFor(dataClass, true))
                .map(Field::getName).collect(toList());
        final Map<String, Field> fieldForAsMap = getFieldForAsMap(dataClass, true);
        Map<String, Method> mapMethodByFieldName = new HashMap<>();

        safeStream(fieldNames).forEach(fieldName -> mapMethodByFieldName.put(fieldName, findSetterMethod(fieldName, dataClass, fieldForAsMap)));

        return mapMethodByFieldName;
    }

    public static <T> Method findSetterMethod(String fieldName, Class<T> dataClass) {
        final Map<String, Field> fieldForAsMap = getFieldForAsMap(dataClass, true);
        return findSetterMethod(fieldName, dataClass, fieldForAsMap);
    }

    public static <T> Method findSetterMethod(String fieldName, Class<T> dataClass, final Map<String, Field> fieldForAsMap) {

        final Field field = fieldForAsMap.get(fieldName);
        if (isNull(field)) {
            logger.warn("Failed to find field [{}] in {}", fieldName, dataClass.getCanonicalName());
            return null;
        }
        final Class<?> fieldClazz = field.getType();

        try {
            Method setterMethod = null;
            try {
                setterMethod = dataClass.getMethod(getMethodName("set", fieldName), fieldClazz);

            } catch (NoSuchMethodException ex) {
                // try auto boxing
                if ("boolean".equals(fieldClazz.getCanonicalName())) {
                    try {
                        setterMethod = dataClass.getMethod(getMethodName("set", fieldName), Boolean.class);
                    } catch (NoSuchMethodException e) {
                        // ignore
                    }
                }
            }

            if (nonNull(setterMethod)) {
                return setterMethod;
            }
            logger.warn("Failed to find setter " + getMethodName("set", fieldName) + "(" + fieldClazz.getCanonicalName() + ") in Class: " + dataClass.getCanonicalName()
                    + " ignoring and carrying on");
        } catch (NullPointerException ex) {
            logger.warn("Error ...   {} : {} : ", fieldName, dataClass);
        }
        return null;
    }

    protected static String getMethodName(String prefix, String name) {
        return prefix + name.substring(0, 1).toUpperCase() + name.substring(1);
    }
}
