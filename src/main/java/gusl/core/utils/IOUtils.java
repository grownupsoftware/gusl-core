/*
 * Grownup Software Limited.
 */
package gusl.core.utils;

import gusl.core.logging.GUSLLogger;
import lombok.NonNull;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.*;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.concurrent.TimeoutException;

/**
 * Input/Output utilities.
 *
 * @author dhudson - Mar 15, 2017 - 1:35:16 PM
 */
public class IOUtils {

    public final static String JAR_MANIFEST_LOCATION = "META-INF/MANIFEST.MF";

    public final static String WAR_MANIFEST_LOCATION = "/" + JAR_MANIFEST_LOCATION;

    /**
     * Constant {@value}
     */
    public final static String PARENT_PATH = "..";

    /**
     * Constant {@value}
     */
    public final static String SELF_PATH = ".";

    /**
     * Constant {@value}
     */
    private static final String DOT_SLASH = "." + File.separator;

    /**
     * Constructor.
     */
    private IOUtils() {
    }

    /**
     * Open for reading, a resource of the specified name from the search path
     * used to load classes.
     *
     * @param resourceName for example a file name.
     * @param classLoader
     * @return an input stream for the resource.
     * @throws IOException if the resource could not be found.
     * @since 1.0
     */
    public static InputStream getResourceAsStream(String resourceName, ClassLoader classLoader)
            throws IOException {

        InputStream stream = classLoader.getResourceAsStream(resourceName);
        if (stream == null) {
            stream = classLoader.getResourceAsStream("/" + resourceName);
        }

        if (stream == null) {
            throw new IOException("Resource [" + resourceName + "] not found");
        }
        return stream;
    }

    /**
     * Find a resource from the class loader.
     *
     * @param resourceName to find
     * @param classLoader
     * @return the located URL
     * @throws IOException if unable to find the resource.
     */
    public static URL getResourceAsURL(String resourceName, ClassLoader classLoader) throws IOException {

        URL resource = classLoader.getResource(resourceName);
        if (resource == null) {
            resource = classLoader.getResource("/" + resourceName);
        }

        if (resource == null) {
            throw new IOException("Resource [" + resourceName + "] not found");
        }

        return resource;
    }

    /**
     * Locate and return a resource as a file.
     *
     * @param resource    to locate
     * @param classLoader
     * @return a {@code File} of the resource
     * @throws IOException if unable to locate resource
     * @since 1.0
     */
    public static File getResourceAsFile(String resource, ClassLoader classLoader)
            throws IOException {

        final URL url = classLoader.getResource(resource);
        if (url == null) {
            throw new IOException("Resource [" + resource + "] not found");
        }

        try {
            return new File(url.toURI());
        } catch (final URISyntaxException ex) {
            throw new IOException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Create a file given the specified name and write the contents
     *
     * @param fileName to create
     * @param contents to write
     * @throws IOException if an error occurs
     * @since 1.0
     */
    public static void writeFile(String fileName, CharSequence contents) throws IOException {
        final File outputFile = new File(fileName);

        writeFile(outputFile, contents);
    }

    /**
     * Create a file given the specified file and write the contents
     *
     * @param file     to create
     * @param contents to write
     * @throws IOException if a write error occurs
     * @since 1.0
     */
    public static void writeFile(File file, CharSequence contents) throws IOException {
        final BufferedWriter writer
                = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                file),
                "UTF-8"));

        writer.append(contents);
        closeQuietly(writer);
    }

    /**
     * Close a closeable resource, catching and discarding any IOExceptions that
     * may arise.
     *
     * @param resource to close
     * @since 1.0
     */
    public final static void closeQuietly(Closeable resource) {
        if (resource == null) {
            return;
        }
        try {
            resource.close();
        } catch (final IOException ignore) {
        }
    }

    /**
     * Close all closeables, catching and discarding any IOExceptions that may
     * arise.
     *
     * @param closeables to close
     */
    public final static void closeAllQuietly(List<? extends Closeable> closeables) {
        if (closeables == null) {
            return;
        }

        for (Closeable closeable : closeables) {
            closeQuietly(closeable);
        }
    }

    public final static void closeAllQuietly(Closeable... closeables) {
        if (closeables == null) {
            return;
        }

        for (Closeable closeable : closeables) {
            closeQuietly(closeable);
        }
    }

    /**
     * Returns the file name of the given file without path or extension
     *
     * @param file to process
     * @return The filename without path or extension
     * @since 1.0
     */
    public static String getFileBaseName(File file) {
        final String fullPath = file.getName();

        final int dot = fullPath.lastIndexOf('.');
        final int sep = fullPath.lastIndexOf(File.pathSeparatorChar);

        return fullPath.substring(sep + 1, dot);
    }

    /**
     * Utility method to return the a file extension
     *
     * @param file to process
     * @return the file extension
     * @since 1.0
     */
    public static String getExtension(File file) {
        final int dot = file.getName().lastIndexOf('.');
        return file.getName().substring(dot + 1);
    }

    /**
     * Return the relative path from file 1 to file 2
     *
     * @param file1 first file
     * @param file2 second file
     * @return the relative path between two files
     * @since 1.0
     */
    public static String getRelativePathForFiles(String file1, String file2) {
        File f1 = new File(file1);
        String parentDir = f1.getParentFile().getAbsolutePath();

        // File f2 = new File(file2);
        // String parent2 = f2.getParentFile().getAbsolutePath();
        String relPath = IOUtils.getRelativePath(parentDir, file2);

        if (relPath.startsWith(DOT_SLASH)) {
            // Remove the ./
            relPath = relPath.substring(2);
        }

        if (relPath.endsWith(File.separator)) {
            relPath = relPath.substring(0, relPath.length() - 1);
        }

        return relPath;
    }

    /**
     * Return the relative position between two directories
     *
     * @param absolutePath1 path 1
     * @param absolutePath2 path 2
     * @return the relative path between path1 and path2
     * @since 1.0
     */
    public static String getRelativePath(String absolutePath1, String absolutePath2) {
        final StringBuffer relativePath = new StringBuffer();
        final StringTokenizer tokenizer1
                = new StringTokenizer(absolutePath1, File.separator);
        final StringTokenizer tokenizer2
                = new StringTokenizer(absolutePath2, File.separator);
        // are there tokens?
        if (tokenizer1.hasMoreTokens() && tokenizer2.hasMoreTokens()) {
            // are the first tokens (drive letters) equal?
            String token1 = tokenizer1.nextToken();
            String token2 = tokenizer2.nextToken();
            if (token1.equals(token2)) {
                int parentCount = 0;
                while (tokenizer1.hasMoreTokens() && tokenizer2.hasMoreTokens()) {
                    token1 = tokenizer1.nextToken();
                    token2 = tokenizer2.nextToken();
                    if (!token1.equals(token2)) {
                        relativePath.append(File.separator);
                        relativePath.append(token2);
                        parentCount++;
                    }
                }
                // one or both are now out of tokens
                if (tokenizer1.hasMoreTokens()) {
                    parentCount += tokenizer1.countTokens();
                } else if (tokenizer2.hasMoreTokens()) {
                    while (tokenizer2.hasMoreTokens()) {
                        relativePath.append(File.separator);
                        relativePath.append(tokenizer2.nextToken());
                    }
                }
                // now append parent paths or self path
                if (parentCount > 0) {
                    for (int index = 0; index < parentCount - 1; index++) {
                        relativePath.insert(0, PARENT_PATH);
                        relativePath.insert(0, File.separator);
                    }
                    relativePath.insert(0, PARENT_PATH);
                } else {
                    relativePath.insert(0, SELF_PATH);
                }
                // add a path separator to the end of this
                relativePath.append(File.separator);
            } else {
                return (absolutePath2);
            }
        }
        return (relativePath.toString());
    }

    /**
     * Creates a reader for a named resource.
     *
     * @param resourceName to create a reader for
     * @param classLoader
     * @return a buffered reader for the named resource
     * @throws IOException if the resource could not be found
     * @since 1.0
     */
    public static BufferedReader createResourceReader(String resourceName, ClassLoader classLoader) throws IOException {
        final InputStream inputStream = getResourceAsStream(resourceName, classLoader);
        final BufferedReader reader = null;
        try {

            final InputStreamReader fr = new InputStreamReader(inputStream);
            final BufferedReader br = new BufferedReader(fr);

            return br;
        } catch (final Exception ex) {
            IOUtils.closeQuietly(reader);
            throw new IOException("Unable to create reader for resource "
                    + resourceName, ex);
        }
    }

    /**
     * Creates a reader for a given file input stream. A character set of
     * <code>UTF-8</code> will be used
     *
     * @param fileInputStream the file input stream to read.
     * @return a Line Number Reader
     * @throws IOException if unable to create a reader for the given file input
     *                     stream.
     * @since 1.0
     */
    public static LineNumberReader createFileReader(InputStream fileInputStream) throws IOException {
        try {
            final LineNumberReader reader
                    = new LineNumberReader(new InputStreamReader(
                    fileInputStream,
                    "UTF-8"));
            return reader;
        } catch (final Exception ex) {
            throw new IOException(
                    "Unable to create reader for file input stream "
                            + fileInputStream,
                    ex);
        }
    }

    /**
     * Creates a reader for a given file path. A character set of {@code UTF-8}
     * will be used.
     *
     * @param filePath to read
     * @return a LineNumberReader
     * @throws IOException if the path is invalid, or unable to read the file
     * @since 1.0
     */
    public static LineNumberReader createFileReader(String filePath) throws IOException {
        try {
            return createFileReader(new FileInputStream(new File(filePath)));
        } catch (FileNotFoundException ex) {
            throw new IOException("File not found " + filePath, ex);
        }
    }

    /**
     * Count the number of lines in a file.
     *
     * @param path of the file
     * @return number of lines in a file
     * @throws IOException if unable to read the file
     * @since 1.0
     */
    public static int getLineCount(String path) throws IOException {
        LineNumberReader reader = null;
        try {
            reader = createFileReader(new FileInputStream(new File(path)));
            int lines = 0;
            while ((reader.readLine()) != null) {
                lines++;
            }
            return lines;
        } finally {
            closeQuietly(reader);
        }
    }

    /**
     * Create a LineNumberReader for a given String
     *
     * @param content to read
     * @return a LineNumberReader
     * @since 1.0
     */
    public static LineNumberReader createStringReader(String content) {
        return new LineNumberReader(new StringReader(content));
    }

    /**
     * Reads the entire contents of a file into a String.
     *
     * @param file the file to read.
     * @return a string containing the entire file content.
     * @throws IOException if unable to read the file.
     * @since 1.0
     */
    public static String readFileAsString(File file)
            throws IOException {

        if (file == null) {
            throw new IOException("Null file parameter");
        }

        try {
            return new String(Files.readAllBytes(file.toPath()));
        } catch (IOException ex) {
            throw new IOException("Unable to read file " + file, ex);
        }
    }

    /**
     * Copy a source file {@code fromFile} to a destination file {@code toFile}
     *
     * @param fromFile source file
     * @param toFile   destination file
     * @throws IOException if unable to copy the file
     * @since 1.0
     */
    public static void copyFile(File fromFile, File toFile) throws IOException {

        // open the input file
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fromFile);
        } catch (final FileNotFoundException ex) {
            closeQuietly(fis);
            throw new IOException("Unable to open file "
                    + fromFile.getAbsolutePath());
        } catch (final SecurityException ex) {
            closeQuietly(fis);
            throw new IOException("Unable to access file "
                    + fromFile.getAbsolutePath()
                    + " - "
                    + ex.getMessage());
        }

        // open the output file (creating if necessary)
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(toFile);
        } catch (final Exception ex) {
            closeQuietly(fis);
            closeQuietly(fos);
            throw new IOException("Unable to open file "
                    + toFile.getAbsolutePath()
                    + " for output");
        }

        try {
            copyStream(fis, fos);
        } catch (final IOException ex) {
            throw new IOException("Failure copying "
                    + fromFile
                    + " to "
                    + toFile
                    + " - "
                    + ex.getMessage());
        } finally {
            closeQuietly(fis);
            closeQuietly(fos);
        }
    }

    /**
     * Copy the input stream to the output stream
     *
     * @param inputStream  inbound stream
     * @param outputStream outbound stream
     * @throws IOException if unable to copy streams
     * @since 1.0
     */
    public static void copyStream(InputStream inputStream, OutputStream outputStream) throws IOException {

        final BufferedInputStream bis = new BufferedInputStream(inputStream);
        final BufferedOutputStream bos = new BufferedOutputStream(outputStream);

        try {
            int fileByte = 0;
            final byte[] byteArray = new byte[2048];

            while (fileByte != -1) {
                fileByte = bis.read(byteArray);
                if (fileByte != -1) {
                    bos.write(byteArray, 0, fileByte);
                }
            }
            bos.flush();
        } catch (final IOException ex) {
            throw new IOException("Failure copying streams "
                    + ex.getMessage(), ex);
        } finally {
            closeQuietly(bos);
            closeQuietly(bis);
        }
    }

    /**
     * Reads the input stream to a String
     *
     * @param inputStream to process
     * @return A {@code String} representation of the input stream
     * @throws IOException if there is an error reading the input
     * @since 1.0
     */
    public static String inputStreamAsString(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        copyStream(inputStream, baos);
        return new String(baos.toByteArray());
    }

    /**
     * Copy a source folder contents to a destination folder contents.
     * <p>
     * If the destination folder does not exist, it will be created.
     *
     * @param sourceFolder   folder containing the source
     * @param destFolder     folder to copy to
     * @param includeSubDirs copy any sub dirs as well
     * @throws IOException if unable to copy files
     * @since 1.0
     */
    public static void copyFolder(File sourceFolder, File destFolder, boolean includeSubDirs) throws IOException {
        if (!destFolder.exists()) {
            destFolder.mkdir();
        }

        File[] files = sourceFolder.listFiles();

        for (File file : files) {
            File destFile = new File(destFolder, file.getName());
            if (file.isFile()) {
                copyFile(file, destFile);
            } else if (file.isDirectory() && includeSubDirs) {
                copyFolder(file, destFile, includeSubDirs);
            }
        }
    }

    /**
     * Load the contents of the URL
     *
     * @param url to process
     * @return the contents of the URL
     * @throws IOException if an {@code IOException} occurs
     * @since 1.0
     */
    public static String loadURL(URL url) throws IOException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            final StringBuffer buffer = new StringBuffer(1500);
            String line = reader.readLine();
            while (line != null) {
                buffer.append(line);
                buffer.append(Platform.LINE_SEPARATOR);
                line = reader.readLine();
            }
            return buffer.toString();
        } catch (final IOException ex) {
            throw new IOException("Unload to load URL [" + url + "] ");
        } finally {
            closeQuietly(reader);
        }
    }

    /**
     * Read a stream and remove all line breaks.
     *
     * @param is input stream
     * @return string result
     * @throws IOException if unable to read the stream
     */
    public static String convertAndRemoveBreaklinesFromStream(InputStream is) throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    if (n == '\n' || n == '\r') {
                        continue;
                    }
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString(); //.replaceAll("\n", "").replaceAll("\r", "");
        }
        return "";
    }

    public static Properties loadJarManifest(ClassLoader classLoader) throws IOException {
        Properties manifest = new Properties();
        InputStream is = getResourceAsStream(JAR_MANIFEST_LOCATION, classLoader);
        if (is == null) {
            throw new IOException("Can't load manifest");
        }
        manifest.load(is);
        return manifest;
    }

    public static void removeAllDirectories(Path rootPath) throws IOException {
        Files.walk(rootPath, FileVisitOption.FOLLOW_LINKS)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }

    public static String md5(File file) throws IOException {
        byte[] b = Files.readAllBytes(file.toPath());
        try {
            byte[] hash = MessageDigest.getInstance("MD5").digest(b);
            return DatatypeConverter.printHexBinary(hash);
        } catch (NoSuchAlgorithmException ignore) {
            // Will not happen
            return null;
        }
    }

    public static void clearTerminal() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void waitForService(String host, int port, @NonNull Duration maxTime, GUSLLogger logger) throws TimeoutException {
        long endTime = System.currentTimeMillis() + maxTime.toMillis();
        while (true) {
            try {
                Socket socket = new Socket();
                SocketAddress address = new InetSocketAddress(host, port);
                socket.connect(address, 3000);
                // If we connect then all OK
                return;
            } catch (IOException ex) {
                if (System.currentTimeMillis() >= endTime) {
                    throw new TimeoutException();
                }
                logger.info("Awaiting for port {} to accept connections", port);
                Utils.sleep(1000);
            }
        }
    }
}
