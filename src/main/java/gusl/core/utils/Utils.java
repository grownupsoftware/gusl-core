/*
 * @author dhudson -
 * Created 22 May 2012 : 06:11:28
 */
package gusl.core.utils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Class of static methods that are general utils
 *
 * @author dhudson - created 25 Jun 2012
 * @since 1.0
 */
public final class Utils {

    public static final String DEFAULT_TIME_MS_FOMAT = "HH:mm:ss:SS";

    /**
     * Constant for timestamp {@value}
     */
    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * Constant for date format {@value}
     */
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    private static String[] theSortedLocales;

    public static final long MILLIS_IN_A_DAY = (1000 * 60 * 60 * 24);

    private Utils() {
    }

    /**
     * Return a sorted list of locales supported by Java
     *
     * @return a String array of sorted locales
     * @since 1.0
     */
    public static String[] getLocales() {
        if (theSortedLocales == null) {
            Locale locales[] = Locale.getAvailableLocales();

            theSortedLocales = new String[locales.length];

            int i = 0;
            for (Locale locale : locales) {
                theSortedLocales[i++] = locale.toString();
            }

            Arrays.sort(theSortedLocales);
        }

        return theSortedLocales;
    }

    /**
     * Return the current / default locale
     *
     * @return String representation of the current locale
     * @since 1.0
     */
    public static String getDefaultLocale() {
        return Locale.getDefault().toString();
    }

    /**
     * Return a date formated in yyyy-MM-dd format.
     *
     * @param date to format
     * @return the formatted date.
     */
    public static String formatDate(Date date) {
        if (date == null) {
            return "";
        }
        return DateFormats.getFormatFor(DEFAULT_DATE_FORMAT).format(date);
    }

    /**
     * Parse the supplied date, supplied as yyyy-MM-dd
     *
     * @param date to parse
     * @return a date if able to parse the string.
     * @throws ParseException if unable to parse the date.
     */
    public static Date parseDate(String date) throws ParseException {
        return DateFormats.getFormatFor(DEFAULT_DATE_FORMAT).parse(date);
    }

    /**
     * The supplied date is formatted using the yyyy-MM-dd HH:mm:ss formatting
     * pattern
     *
     * @param date the date to format
     * @return a formatted date and time string.
     * @since 1.0
     */
    public static String formatDateTime(Date date) {
        if (date == null) {
            return "";
        }
        return DateFormats.getFormatFor(DEFAULT_DATE_TIME_FORMAT).format(date);
    }

    /**
     * The supplied date is formatted using the yyyy-MM-dd HH:mm:ss formatting
     * pattern
     *
     * @param millis a time represented as the number of milliseconds, between a
     *               point in time and midnight, January 1, 1970 UTC.
     * @return a formatted date and time string.
     * @since 1.0
     */
    public static String formatDateTime(long millis) {
        return formatDateTime(new Date(millis));
    }

    /**
     * Return the millis into a String format of hh:mm:ss:SS
     *
     * @param time to use
     * @return formatted time
     */
    public static String getTimestamp(long time) {
        return DateFormats.getFormatFor(DEFAULT_TIME_MS_FOMAT).format(time);
    }

    /**
     * Dynamically extend the System Class Loader with the given path or jar
     * file
     *
     * @param path to extend the class path by
     * @throws IOException if invalid path
     * @since 1.0
     */
    public static void extendClassPath(String path) throws IOException {
        final File filePath = new File(path);

        final ClassLoader cl = ClassLoader.getSystemClassLoader();

        if (cl instanceof URLClassLoader) {
            final URLClassLoader ul = (URLClassLoader) cl;

            final Class<?>[] paraTypes = new Class[1];
            paraTypes[0] = URL.class;

            try {
                final Method method = URLClassLoader.class.getDeclaredMethod("addURL", paraTypes);

                method.setAccessible(true);
                final Object[] args = new Object[1];

                args[0] = filePath.toURI().toURL();
                method.invoke(ul, args);
            } catch (final IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException | MalformedURLException ex) {
                throw new IOException("Unable to extend class path ", ex);
            }
        } else {
            throw new IOException("System Class loader is not a URL class loader");
        }
    }

    /**
     * Create a unique Token.
     *
     * @return a unique UUID string
     */
    public static String createUniqueToken() {
        return UUID.randomUUID().toString();
    }

    /**
     * Sleep for a given period.
     *
     * @param millis to sleep for
     */
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ignore) {
        }
    }

    /**
     * Helper method to return the value of the BigDecimal.
     * <p>
     * 0 is returned if the value null.
     *
     * @param value
     * @return 0 or value as double
     */
    public static double getBigDecimalDouble(BigDecimal value) {
        if (value == null) {
            return 0;
        }

        return value.doubleValue();
    }

    /**
     * Converts a list of objects into a map of objects keyed by some function
     * of the object.
     *
     * @param <K>
     * @param <V>
     * @param list
     * @param keyMapper
     * @return
     */
    public static <K, V> Map<K, V> toMap(List<V> list, Function<V, K> keyMapper) {
        return toMap(list, null, keyMapper);
    }

    /**
     * Converts a list of objects into a map of objects keyed by some function
     * of the object.
     *
     * @param <K>
     * @param <V>
     * @param list
     * @param map
     * @param keyMapper
     * @return
     */
    public static <K, V> Map<K, V> toMap(List<V> list, Map<K, V> map, Function<V, K> keyMapper) {
        if (list != null) {
            if (map == null) {
                map = new HashMap<>();
            }
            for (V value : list) {
                K key = keyMapper.apply(value);
                map.put(key, value);
            }
        }
        return map;
    }

    public static <K, V> Map<K, List<V>> toMapOfLists(List<V> list, Function<V, K> keyMapper) {
        return toMapOfLists(list, null, keyMapper);
    }

    public static <K, V> Map<K, List<V>> toMapOfLists(List<V> list, Map<K, List<V>> map, Function<V, K> keyMapper) {
        if (list != null) {
            if (map == null) {
                map = new HashMap<>();
            }
            for (V value : list) {
                K key = keyMapper.apply(value);
                List<V> mappedList = map.get(key);
                if (mappedList == null) {
                    mappedList = new ArrayList<>();
                    map.put(key, mappedList);
                }
                mappedList.add(value);
            }
        }

        return map;
    }

    @SafeVarargs
    public static final <T> List<T> listOf(T... ts) {
        ArrayList<T> list = new ArrayList<>();
        list.addAll(Arrays.asList(ts));
        return list;
    }

    @SafeVarargs
    public static final <T> Set<T> setOf(T... ts) {
        HashSet<T> set = new HashSet<>();
        set.addAll(Arrays.asList(ts));
        return set;
    }

    /**
     * Returns time difference between two dates in the time unit specified as a
     * parameter
     *
     * @param initialTime
     * @param finalTime
     * @param unit        Time unit of the difference value
     * @return
     */
    public static long getTimeDifference(Date initialTime, Date finalTime, TimeUnit unit) {
        long diff = finalTime.getTime() - initialTime.getTime();
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     * Convenience method for getting a null safe stream from a collection
     *
     * @param <T>
     * @param collection
     * @return a stream of the collection if not null, otherwise an empty
     * stream
     */
    public static <T> Stream<T> safeStream(Collection<T> collection) {
        return collection == null ? Stream.empty() : collection.stream();
    }

    public static <T> Stream<T> safeStream(T[] array) {
        return array == null ? Stream.empty() : Arrays.stream(array);
    }

    public static <T> Collection<T> safeCollection(Collection<T> collection) {
        return collection == null ? Collections.emptyList() : collection;
    }

    public static boolean hasElements(Collection collection) {
        if (collection == null || collection.isEmpty()) {
            return false;
        }

        return true;
    }

    public static <T> void safeConsumer(Collection<T> collection, Consumer<T> consumer) {
        if (hasElements(collection)) {
            consumer.accept((T) collection);
        }
    }

    /**
     * Convenience method to simplify writing object comparators that contain
     * multiple distinct comparisons.
     *
     * @param <T>
     * @param o
     * @param levels
     * @return
     */
    @SafeVarargs
    public static <T> int cascadingCompare(T o, Function<T, Integer>... levels) {
        int compare = 0;
        for (int i = 0; compare == 0 && i < levels.length; i++) {
            Function<T, Integer> fn = levels[i];
            compare = fn.apply(o);
        }
        return compare;
    }

    /**
     * Return the Date as a LocalDate, using the default time zone
     *
     * @param date
     * @return
     */
    public static LocalDate asLocalDate(Date date) {
        return asLocalDate(date, ZoneId.systemDefault());
    }

    /**
     * Return the date as a LocalDate with the given time zone
     *
     * @param date
     * @param zone
     * @return
     */
    public static LocalDate asLocalDate(Date date, ZoneId zone) {
        if (date == null) {
            return null;
        }

        return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalDate();
    }

    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime asLocalDateTime(Date date) {
        return asLocalDateTime(date, ZoneId.systemDefault());
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime asLocalDateTime(Date date, ZoneId zone) {
        if (date == null) {
            return null;
        }

        return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalDateTime();
    }

    /**
     * Check to see if the address is a local or loopback address.
     *
     * @param address
     * @return true if any local, loopback or site local
     */
    public static boolean isLocalAddress(InetAddress address) {
        return (address.isLoopbackAddress()) || (address.isAnyLocalAddress()) || (address.isSiteLocalAddress());
    }

    /**
     * Return a random number inclusive of the range.
     *
     * @param minRange
     * @param maxRange
     * @return
     */
    public static int getRandomValue(int minRange, int maxRange) {
        return ThreadLocalRandom.current().nextInt(minRange, maxRange + 1);
    }

    /**
     * Wait for all of the futures to complete.
     * <p>
     * This includes if they complete exceptionally.
     *
     * @param futures
     * @param timeout in ms
     * @throws TimeoutException
     */
    public static void waitForFuturesToComplete(List<? extends Future> futures, long timeout) throws TimeoutException {
        long endTime = System.currentTimeMillis() + timeout;
        boolean allComplete = false;
        // Lets wait for the futures to finish
        while (!allComplete) {
            allComplete = true;
            for (Future<?> future : futures) {

                if (!future.isDone()) {
                    allComplete = false;
                }
            }

            if (!allComplete) {
                if (endTime <= System.currentTimeMillis()) {
                    throw new TimeoutException();
                }

                // Lets wait a bit
                sleep(100);
            }
        }
    }

    public static Date dateOneYearAgo(Date fromDate) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(fromDate);
        cal.add(Calendar.YEAR, -1);
        return cal.getTime();
    }

    public static Date subtractDays(Date date, int days) {
        return new Date(date.getTime() - (days * MILLIS_IN_A_DAY));
    }

    public static Date subtractDaysFromNow(int days) {
        return subtractDays(new Date(), days);
    }

    public static boolean isTrue(Boolean value) {
        if (value == null) {
            return false;
        }
        return value;
    }
}
