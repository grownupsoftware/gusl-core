/*
 * Grownup Software Limited.
 */
package gusl.core.utils;

import java.awt.GraphicsEnvironment;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Utilities for the Java Platform
 *
 * @author dhudson - Mar 15, 2017 - 1:27:49 PM
 */
public class Platform {

    private static final String PLATFORM = System.getProperty("os.name");
    private static final String TMP_DIRECTORY = System.getProperty("java.io.tmpdir");

    private static final String JAVA_VERSION = System.getProperties().getProperty("java.version");
    private static final String JAVA_VENDOR = System.getProperties().getProperty("java.vendor");

    private static final OperatingSystemMXBean theOSBean = ManagementFactory.getOperatingSystemMXBean();
    private static final RuntimeMXBean theRuntimeBean = ManagementFactory.getRuntimeMXBean();
    private static final MemoryMXBean theMemoryBean = ManagementFactory.getMemoryMXBean();

    private static String theIPAddress;
    private static String theHostName;

    public static final String UNKNOWN = "unknown";

    /**
     * Constant OS dependent line separator
     */
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private Platform() {
        // Static methods only
    }

    public static String getTmpDirectory() {
        return TMP_DIRECTORY;
    }

    /**
     * Check to see if there is a console
     *
     * @return true if there is no console
     */
    public static boolean isHeadless() {
        return GraphicsEnvironment.isHeadless();
    }

    /**
     * Check to see if running on a Mac
     *
     * @return true if running on a Mac
     */
    public static boolean isMac() {
        return (PLATFORM.startsWith("Mac"));
    }

    /**
     * Check to see if running on Windows
     *
     * @return true if the platform is windows
     * @since 2.4
     */
    public static boolean isWindows() {
        return (PLATFORM.startsWith("Windows"));
    }

    /**
     * Get platform string
     *
     * @return the platform string
     */
    public static String getPlatform() {
        return PLATFORM;
    }

    /**
     * Return the Java Version
     *
     * @return java version
     */
    public static String getJavaVersion() {
        return JAVA_VERSION;
    }

    /**
     * Return the Java Vendor
     *
     * @return java vendor
     */
    public static String getJavaVendor() {
        return JAVA_VENDOR;
    }

    public static String getJavaName() {
        return theRuntimeBean.getVmName();
    }

    /**
     * Return the mac address of the first network card.
     *
     * @return mac address or null, if unable to obtain the mac address
     */
    public static String getFirstMacAddress() {
        try {
            NetworkInterface network = NetworkInterface.getByInetAddress(InetAddress.getLocalHost());
            return new String(network.getHardwareAddress());
        } catch (UnknownHostException | SocketException ignore) {
            return null;
        }
    }

    public static String getArchitecture() {
        return theOSBean.getArch();
    }

    public static String getOSName() {
        return theOSBean.getName();
    }

    public static String getOSVersion() {
        return theOSBean.getVersion();
    }

    /**
     * Return the number of cores for the machine.
     *
     * Will always be 1 or greater.
     *
     * @return number of cores for the machine
     */
    public static int getNumberOfCores() {
        return Runtime.getRuntime().availableProcessors();
    }

    /**
     * Return an integer system property if present, or the default value.
     *
     * @param systemProperty
     * @param defaultValue
     * @return system property or default value
     */
    public static int getIntSystemProperty(String systemProperty, int defaultValue) {
        try {
            return Integer.parseInt(System.getProperty(systemProperty));
        } catch (NumberFormatException ex) {
            return defaultValue;
        }
    }

    /**
     * Check to see if a system property is present, and if it is not empty.
     *
     * @param systemProperty to check for
     * @return true if present and not empty
     */
    public static boolean hasSystemProperty(String systemProperty) {
        return !StringUtils.nullOrEmpty(System.getProperty(systemProperty));
    }

    /**
     * Return the IP Address if possible, otherwise return {@value #UNKNOWN}.
     * <p>
     * Please beware that a host may have many NICs and this might not return
     * the correct one.</p>
     *
     * @return host name or {@value #UNKNOWN}
     */
    public static String getIPAddress() {
        if (theIPAddress == null) {
            try {
                InetAddress localHost = InetAddress.getLocalHost();

                theIPAddress = localHost.getHostAddress();
            } catch (UnknownHostException ex) {
                theIPAddress = UNKNOWN;
            }
        }

        return theIPAddress;
    }

    /**
     * Return the host name if possible, otherwise return {@value #UNKNOWN}
     *
     * @return host name or {@value #UNKNOWN}
     */
    public static String getHostName() {
        if (theHostName == null) {
            try {
                InetAddress localHost = InetAddress.getLocalHost();
                theHostName = localHost.getHostName();
            } catch (UnknownHostException ex) {
                theHostName = UNKNOWN;
            }
        }
        return theHostName;
    }

    public static long getMaxMemory() {
        return theMemoryBean.getHeapMemoryUsage().getMax() + theMemoryBean.getNonHeapMemoryUsage().getMax();
    }

    public static long getUsedMemory() {
        return theMemoryBean.getHeapMemoryUsage().getUsed() + theMemoryBean.getNonHeapMemoryUsage().getUsed();
    }

    public static long getUpTime() {
        return theRuntimeBean.getUptime();
    }

    public static double getSystemLoad() {
        return theOSBean.getSystemLoadAverage();
    }
}
