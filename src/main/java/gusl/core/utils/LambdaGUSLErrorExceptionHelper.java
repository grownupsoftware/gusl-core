package gusl.core.utils;

import gusl.core.exceptions.GUSLException;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author grantwallace
 */
public class LambdaGUSLErrorExceptionHelper {

    @FunctionalInterface
    public interface ConsumerWithExceptions<T, E extends GUSLException> {

        void accept(T t) throws E;
    }

    @FunctionalInterface
    public interface BiConsumerWithExceptions<T, U, E extends GUSLException> {

        void accept(T t, U u) throws E;
    }

    @FunctionalInterface
    public interface FunctionWithExceptions<T, R> {

        R apply(T t) throws GUSLException;
    }

    @FunctionalInterface
    public interface BiFunctionWithExceptions<T, R, H> {

        R apply(T t, H h) throws GUSLException;
    }

    @FunctionalInterface
    public interface SupplierWithExceptions<T, E extends GUSLException> {

        T get() throws E;
    }

    @FunctionalInterface
    public interface RunnableWithExceptions<E extends GUSLException> {

        void run() throws E;
    }

    @FunctionalInterface
    public interface MapWithExceptions<T, K, R, E extends GUSLException> {

        R apply(T t) throws E;
    }

    /**
     * Example Usage: .forEach(rethrowConsumer(name ->
     * System.out.println(Class.forName(name)))); or
     * .forEach(rethrowConsumer(ClassNameUtil::println));
     *
     * @param <T>      the type
     * @param <E>      the exception
     * @param consumer the functional interface consumer
     * @return instance of type
     */
    public static <T, E extends GUSLException> Consumer<T> rethrowGUSLErrorConsumer(ConsumerWithExceptions<T, E> consumer) {
        return t -> {
            try {
                consumer.accept(t);
            } catch (GUSLException exception) {
                throwAsUnchecked(exception);
            }
        };
    }

    /**
     * @param <T>        the type of the first parameter
     * @param <U>        the type of the second parameter
     * @param <E>        the exception
     * @param biConsumer the functional interface consumer
     * @return
     */
    public static <T, U, E extends GUSLException> BiConsumer<T, U> rethrowGUSLErrorBiConsumer(BiConsumerWithExceptions<T, U, E> biConsumer) {
        return (t, u) -> {
            try {
                biConsumer.accept(t, u);
            } catch (GUSLException exception) {
                throwAsUnchecked(exception);
            }
        };
    }

//    /**
//     * .map(rethrowFunction(name -> Class.forName(name))) or
//     * .map(rethrowFunction(Class::forName))
//     */

    /**
     * @param <T>      the type
     * @param <R>      the return type
     * @param <E>      the exception
     * @param function the functional interface function
     * @return
     */
    public static <T, R, E extends GUSLException> Function<T, R> rethrowGUSLErrorFunction(FunctionWithExceptions<T, R> function) {
        return t -> {
            try {
                return function.apply(t);
            } catch (GUSLException exception) {
                throwAsUnchecked(exception);
                return null;
            }
        };
    }

    /**
     * @param <T>      the type
     * @param <K>      the key value
     * @param <R>      the return type
     * @param <E>      the exception
     * @param function the functional interface function
     * @return
     */
    public static <T, K, R, E extends GUSLException> Function<T, R> rethrowGUSLErrorCollector(MapWithExceptions<T, K, R, E> function) {
        return t -> {
            try {
                return function.apply(t);
            } catch (GUSLException exception) {
                throwAsUnchecked(exception);
                return null;
            }
        };
    }

//    /**
//     * rethrowSupplier(() -> new StringJoiner(new String(new byte[]{77, 97, 114,
//     * 107}, "UTF-8"))),
//     */

    /**
     * @param <T>      the type
     * @param <E>      the exception
     * @param function the functional interface function
     * @return
     */
    public static <T, E extends GUSLException> Supplier<T> rethrowGUSLErrorSupplier(SupplierWithExceptions<T, E> function) {
        return () -> {
            try {
                return function.get();
            } catch (GUSLException exception) {
                throwAsUnchecked(exception);
                return null;
            }
        };
    }

    @SuppressWarnings("unchecked")
    private static <E extends Throwable> void throwAsUnchecked(Exception exception) throws E {
        throw (E) exception;
    }

}
