package gusl.core.utils;

import lombok.CustomLog;

import java.time.Clock;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Generate ID's unique across nodes.
 * <p>
 * Old Version, pre 1/11/2018
 *
 * @author dhudson
 */
@CustomLog
public class IdGeneratorV1 {

    // The day it was born.                
    private static final long BORN_EPOCH = 1496962800000L; // 2017-06-09

    private static final long MAX_JAVASCRIPT_ID = 999_999_999_999_999l;
    private static final int ONE_HUNDRED = 100;

    // Node Ids will come from the Router, but allow for a testing one.
    private static final int TESTING_NODE_ID = 99;
    private static final Clock CLOCK = Clock.systemUTC();

    private static final AtomicLong theCorrelationId = new AtomicLong(CLOCK.millis());
    /*
     * Shared resources across the class.
     * Its worth noting here that the shared resources are only used for one system at a time, as an engine should not be creating edge Ids.
     */
    private static final ReentrantLock theLock = new ReentrantLock(true);

    private static long theSequence;
    private static long theLastMillis;
    private static int theNodeId;

    private static AtomicLong theUniqueId;

    /**
     * Used in testing.
     */
    public static void setForTesting() {
        theNodeId = TESTING_NODE_ID;
    }

    /**
     * This should be the node ID from registration with the router.
     *
     * @param uniqueId
     */
    public static void setNodeUniqueId(int uniqueId) {
        theNodeId = uniqueId;
    }

    /**
     * Return a unique ID for this VM.
     *
     * @return unique ID for this VM
     */
    public static long generateCorrelationId() {
        return theCorrelationId.getAndIncrement();
    }

    public static long generateUniqueNodeId() {
        return generateId(theNodeId);
    }

    public static long generateTestingId() {
        return generateId(TESTING_NODE_ID);
    }

    /**
     * Generate a numeric id of the form
     * &lt;timestamp&gt;&lt;sequence&gt;&lt;node&gt;. timestamp is the number of
     * milliseconds since the Eddie epoch. sequence is a two digit number
     * between 00 and 99. This allows up to 100 ids per millisecond or 100,000
     * per second. node is a two digit node id. This allows ids to be generated
     * on distributed components without clashing.
     *
     * @param nodeId
     * @return
     */
    private static long generateId(int nodeId) {

        if ((nodeId < 0) || (nodeId > 99)) {
            throw new IllegalArgumentException(nodeId + " is not valid. Node id must be between 0 and 99.");
        }
        if (nodeId == 0) {
            throw new IllegalStateException("Not registered with the Fed.");
        }

        long timestamp = CLOCK.millis();
        long sequence = 0;

        try {
            theLock.lock();

            if (timestamp < theLastMillis) {
                timestamp = theLastMillis;
            }

            if (theLastMillis == timestamp) {
                theSequence++;
                if (theSequence == 99) {
                    // Guard against 100 calls to generate ID in the same millis.
                    // Testing kind of proved that we never get here, but just incase we decide to run on a cray
                    logger.warn("Used 100 sequence numbers for id in a single millisecond. Blocking until next millisecond.");
                    timestamp = waitNextMillis(theLastMillis);
                    theLastMillis = timestamp;
                    theSequence = 0;
                }
            } else {
                theSequence = 0;
                theLastMillis = timestamp;
            }

            sequence = theSequence;
        } finally {
            theLock.unlock();
        }

        // timestamp
        long id = timestamp - BORN_EPOCH;
        // add two digit sequence
        id *= ONE_HUNDRED;
        id += sequence;
        // add two digit node id
        id *= ONE_HUNDRED;
        id += nodeId;

        if (id > MAX_JAVASCRIPT_ID) {
            logger.warn("Id is larger than maximum supported Java Script number. This will cause rounding issues on Java Script clients.");
        }

        return id;
    }

    /**
     * Wait until the next timestamp tick
     *
     * @param lastTimestamp
     * @return
     */
    private static long waitNextMillis(long lastTimestamp) {
        long timestamp = CLOCK.millis();
        while (timestamp <= lastTimestamp) {
            timestamp = CLOCK.millis();
        }
        return timestamp;
    }

    /**
     * Return random UUID.
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString();
    }
}
