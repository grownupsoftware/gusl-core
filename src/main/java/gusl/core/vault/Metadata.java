package gusl.core.vault;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author dhudson
 * @since 10/03/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class Metadata {
    private LocalDateTime createdTime;
    private Boolean destroyed;
    private Integer version;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
