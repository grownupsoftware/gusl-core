package gusl.core.vault;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 10/03/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class VaultResponse {

    private String requestId;
    private String leaseId;
    private Boolean renewable;
    private Long leaseDuration;
    private Data data;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
