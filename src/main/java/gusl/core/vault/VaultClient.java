package gusl.core.vault;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.exceptions.GUSLException;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.StringUtils;
import gusl.core.utils.SystemPropertyUtils;
import lombok.CustomLog;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

/**
 * @author dhudson
 * @since 10/03/2022
 */
@CustomLog
public class VaultClient {

    private final ObjectMapper theMapper = ObjectMapperFactory.getObjectMapperWithNaming(ObjectMapperFactory.SNAKE_CASE);
    private HttpClient theClient;

    public VaultClient() {
        theClient = HttpClient.newBuilder()
                .connectTimeout(Duration.ofSeconds(10))
                .build();
    }

    public String getSecret(String secretKey) throws GUSLException {
        checkProperty(SystemPropertyUtils.VAULT_URL);
        checkProperty(SystemPropertyUtils.VAULT_TOKEN);
        String[] tokens = secretKey.split(":");
        if (tokens.length != 2) {
            throw new GUSLException("Missing key element [:]" + secretKey);
        }
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(SystemPropertyUtils.getVaultUrl() + tokens[0]))
                    .header("Authorization", "Bearer " + SystemPropertyUtils.getVaultToken())
                    .build();
            HttpResponse<String> response = theClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == 403) {
                throw new GUSLException("Token Expired");
            }
            VaultResponse vaultResponse = theMapper.readValue(response.body(), VaultResponse.class);
            return vaultResponse.getData().getData().get(tokens[1]);
        } catch (IOException | InterruptedException ex) {
            logger.warn("Unable to fetch key {}", secretKey, ex);
            throw new GUSLException(ex.getLocalizedMessage());
        }
    }

    private void checkProperty(String propertyKey) throws GUSLException {
        if (StringUtils.isBlank(SystemPropertyUtils.getProperty(propertyKey))) {
            throw new GUSLException("Missing " + propertyKey + " property");
        }
    }
}
