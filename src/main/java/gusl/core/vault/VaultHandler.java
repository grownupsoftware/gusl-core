package gusl.core.vault;

import gusl.core.exceptions.GUSLException;

/**
 * @author dhudson
 * @since 11/03/2022
 */
public class VaultHandler {

    public static final VaultClient theClient = new VaultClient();

    private VaultHandler() {
    }

    public static String getSecret(String secretKey) throws GUSLException {
        return theClient.getSecret(secretKey);
    }
}
