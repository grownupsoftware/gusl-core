package gusl.core.vault;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

/**
 * @author dhudson
 * @since 10/03/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class Data {
    private Map<String, String> data;
    private Metadata metadata;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
