package gusl.core.dates;

import gusl.core.exceptions.GUSLException;

/**
 *
 * @author dhudson
 */
public class GUSLDateException extends GUSLException {

    private static final long serialVersionUID = 1L;

    public GUSLDateException(String message, Throwable ex) {
        super(message, ex);
    }

    public GUSLDateException(String message) {
        super(message);
    }
}
