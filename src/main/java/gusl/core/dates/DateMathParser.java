package gusl.core.dates;

import java.time.*;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

/**
 * A parser for date/time formatted text with optional date math.
 * <p>
 * The format of the datetime is configurable, and unix timestamps can also be
 * used. Datemath is appended to a datetime with the following syntax:
 * <code>[+-/](\d+)?[yMwdhHms]</code>. Supported rounding units are y year M
 * month w week (beginning on a monday) d day h/H hour m minute s second
 * Examples :-
 * <p>
 * <ol>
 *
 * <li>now+1h</li>
 * <li>now-1h/d</li>
 * <li>+1M/d</li>
 *
 *
 * </ol></p>
 *
 * @author dhudson
 */
public class DateMathParser {

    public static final String START_OF_LAST_MONTH = "now-1M/M";
    public static final String START_OF_THIS_MONTH = "now/M";
    public static final String HOUR_AGO = "now-1h";
    public static final String START_OF_TODAY = "now/d";
    public static final String START_OF_YESTERDAY = "now-1d/d";

    /**
     * Parse the expression, if now not present, it will be assumed.
     *
     * @param dateMath
     * @param rounding if true, 1 Millisecond will be removed from a rounding
     *                 result to make it inclusive
     * @return an Instant
     * @throws GUSLDateException
     */
    public static Instant parse(String dateMath, boolean rounding) throws GUSLDateException {
        String mathString;
        Instant time = Instant.now();
        // This should start with now ..
        if (dateMath.startsWith("now")) {
            mathString = dateMath.substring("now".length());
        } else {
            mathString = dateMath;
        }

        return parseMath(mathString, time, rounding, null);
    }

    public static Instant parse(String dateMath) throws GUSLDateException {
        return parse(dateMath, false);
    }

    public static Instant parse(String dateMath, Date date) throws GUSLDateException {
        return parseMath(dateMath, date.toInstant(), false, null);
    }

    public static Instant parse(String dateMath, Date date, boolean rounding) throws GUSLDateException {
        return parseMath(dateMath, date.toInstant(), rounding, null);
    }

    public static Instant parse(String dateMath, Instant instant, boolean rounding) throws GUSLDateException {
        return parseMath(dateMath, instant, rounding, null);
    }

    public static Instant parse(String dateMath, Instant instant) throws GUSLDateException {
        return parseMath(dateMath, instant, false, null);
    }

    private static Instant parseMath(final String mathString, final Instant time, final boolean roundUpProperty,
                                     ZoneId timeZone) throws GUSLDateException {
        if (timeZone == null) {
            timeZone = ZoneOffset.UTC;
        }
        ZonedDateTime dateTime = ZonedDateTime.ofInstant(time, timeZone);
        for (int i = 0; i < mathString.length(); ) {
            char c = mathString.charAt(i++);
            final boolean round;
            final int sign;
            if (c == '/') {
                round = true;
                sign = 1;
            } else {
                round = false;
                switch (c) {
                    case '+':
                        sign = 1;
                        break;
                    case '-':
                        sign = -1;
                        break;
                    default:
                        throw new GUSLDateException("operator not supported for date math " + mathString);
                }
            }

            if (i >= mathString.length()) {
                throw new GUSLDateException("truncated date math " + mathString);
            }

            final int num;
            if (!Character.isDigit(mathString.charAt(i))) {
                num = 1;
            } else {
                int numFrom = i;
                while (i < mathString.length() && Character.isDigit(mathString.charAt(i))) {
                    i++;
                }
                if (i >= mathString.length()) {
                    throw new GUSLDateException("truncated date math " + mathString);
                }
                num = Integer.parseInt(mathString.substring(numFrom, i));
            }
            if (round) {
                if (num != 1) {
                    throw new GUSLDateException("rounding `/` can only be used on single unit types " + mathString);
                }
            }
            char unit = mathString.charAt(i++);
            switch (unit) {
                case 'y':
                    if (round) {
                        dateTime = dateTime.withDayOfYear(1).with(LocalTime.MIN);
                        if (roundUpProperty) {
                            dateTime = dateTime.plusYears(1);
                        }
                    } else {
                        dateTime = dateTime.plusYears(sign * num);
                    }
                    break;
                case 'M':
                    if (round) {
                        dateTime = dateTime.withDayOfMonth(1).with(LocalTime.MIN);
                        if (roundUpProperty) {
                            dateTime = dateTime.plusMonths(1);
                        }
                    } else {
                        dateTime = dateTime.plusMonths(sign * num);
                    }
                    break;
                case 'w':
                    if (round) {
                        dateTime = dateTime.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).with(LocalTime.MIN);
                        if (roundUpProperty) {
                            dateTime = dateTime.plusWeeks(1);
                        }
                    } else {
                        dateTime = dateTime.plusWeeks(sign * num);
                    }
                    break;
                case 'd':
                    if (round) {
                        dateTime = dateTime.with(LocalTime.MIN);
                        if (roundUpProperty) {
                            dateTime = dateTime.plusDays(1);
                        }
                    } else {
                        dateTime = dateTime.plusDays(sign * num);
                    }
                    break;
                case 'h':
                case 'H':
                    if (round) {
                        dateTime = dateTime.withMinute(0).withSecond(0).withNano(0);
                        if (roundUpProperty) {
                            dateTime = dateTime.plusHours(1);
                        }
                    } else {
                        dateTime = dateTime.plusHours(sign * num);
                    }
                    break;
                case 'm':
                    if (round) {
                        dateTime = dateTime.withSecond(0).withNano(0);
                        if (roundUpProperty) {
                            dateTime = dateTime.plusMinutes(1);
                        }
                    } else {
                        dateTime = dateTime.plusMinutes(sign * num);
                    }
                    break;
                case 's':
                    if (round) {
                        dateTime = dateTime.withNano(0);
                        if (roundUpProperty) {
                            dateTime = dateTime.plusSeconds(1);
                        }
                    } else {
                        dateTime = dateTime.plusSeconds(sign * num);
                    }
                    break;
                default:
                    throw new GUSLDateException("unit " + unit + " not supported for date math " + mathString);
            }
            if (round && roundUpProperty) {
                // subtract 1 millisecond to get the largest inclusive value
                dateTime = dateTime.minus(1, ChronoField.MILLI_OF_SECOND.getBaseUnit());
            }
        }
        return dateTime.toInstant();
    }

}
