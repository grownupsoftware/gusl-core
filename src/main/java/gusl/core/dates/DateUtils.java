package gusl.core.dates;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @author dhudson
 * @since 25/03/2022
 */
public class DateUtils {
    private DateUtils() {
    }

    public static long zonedDateTimeDifference(ZonedDateTime d1, ZonedDateTime d2, ChronoUnit unit) {
        return unit.between(d1, d2);
    }

    public static long zonedDateTimeDifference(ZonedDateTime date, ChronoUnit unit) {
        return zonedDateTimeDifference(date, ZonedDateTime.now(), unit);
    }

    public static long localDateTimeDifference(LocalDateTime d1, LocalDateTime d2, ChronoUnit unit) {
        return unit.between(d1, d2);
    }

    public static long localDateTimeDifference(LocalDateTime date, ChronoUnit unit) {
        return localDateTimeDifference(date, LocalDateTime.now(), unit);
    }

    /**
     * Return the Date as a LocalDate, using the default time zone
     *
     * @param date
     * @return
     */
    public static LocalDate asLocalDate(Date date) {
        return asLocalDate(date, ZoneId.systemDefault());
    }

    /**
     * Return the date as a LocalDate with the given time zone
     *
     * @param date
     * @param zone
     * @return
     */
    public static LocalDate asLocalDate(Date date, ZoneId zone) {
        if (date == null) {
            return null;
        }

        return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalDate();
    }

    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime asLocalDateTime(Date date) {
        return asLocalDateTime(date, ZoneId.systemDefault());
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime asLocalDateTime(Date date, ZoneId zone) {
        if (date == null) {
            return null;
        }

        return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalDateTime();

    }

    public static Date asDate(ZonedDateTime zonedDateTime) {
        return Date.from(zonedDateTime.toInstant());
    }

}
