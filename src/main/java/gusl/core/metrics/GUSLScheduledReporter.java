package gusl.core.metrics;

import com.codahale.metrics.MetricFilter;
import gusl.core.executors.NamedThreadFactory;

import java.io.Closeable;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author grant
 */
public abstract class GUSLScheduledReporter implements Closeable {

    private final GUSLMetricRegistry registry;
    private final ScheduledExecutorService executor;
    private final MetricFilter filter;
    private final double durationFactor;
    private final String durationUnit;
    private final double rateFactor;
    private final String rateUnit;

    protected GUSLScheduledReporter(GUSLMetricRegistry registry,
                                    String name,
                                    MetricFilter filter,
                                    TimeUnit rateUnit,
                                    TimeUnit durationUnit) {
        this.registry = registry;
        this.filter = filter;
        this.executor = Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory(name));
        this.rateFactor = rateUnit.toSeconds(1);
        this.rateUnit = calculateRateUnit(rateUnit);
        this.durationFactor = 1.0 / durationUnit.toNanos(1);
        this.durationUnit = durationUnit.toString().toLowerCase(Locale.US);
    }

    public void start(long period, TimeUnit unit) {
        executor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                report();
            }
        }, period, period, unit);
    }

    public void stop() {
        executor.shutdown();
        try {
            executor.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException ignored) {
            // do nothing
        }
    }

    /**
     * Stops the reporter and shuts down its thread of execution.
     */
    @Override
    public void close() {
        stop();
    }

    public void report() {
        report(GUSLMetrics.builder()
                .gauges(registry.getGauges(filter))
                .counters(registry.getCounters(filter))
                .histograms(registry.getHistograms(filter))
                .meters(registry.getMeters(filter))
                .timers(registry.getTimers(filter))
                .latencies(registry.getLatencies(filter))
                .queues(registry.getQueues(filter)).build());
    }

    public abstract void report(GUSLMetrics metrics);

    protected String getRateUnit() {
        return rateUnit;
    }

    protected String getDurationUnit() {
        return durationUnit;
    }

    protected double convertDuration(double duration) {
        return duration * durationFactor;
    }

    protected double convertRate(double rate) {
        return rate * rateFactor;
    }

    private String calculateRateUnit(TimeUnit unit) {
        final String s = unit.toString().toLowerCase(Locale.US);
        return s.substring(0, s.length() - 1);
    }
}
