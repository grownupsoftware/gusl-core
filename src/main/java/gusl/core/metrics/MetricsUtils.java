package gusl.core.metrics;

import com.codahale.metrics.Meter;
import com.codahale.metrics.Snapshot;

import java.text.DecimalFormat;

/**
 * Static method helper class to deal with metrics.
 *
 * @author dhudson
 */
public class MetricsUtils {

    private static final ThreadLocal<DecimalFormat> DECIMAL_FORMAT
            = new ThreadLocal<DecimalFormat>() {
                @Override
                protected DecimalFormat initialValue() {
                    return new DecimalFormat("###,###,###.##");
                }
            };

    /**
     * Simple toString for snapshot, as it doesn't have one.
     *
     * <p>
     * min [xx] max [xx] mean [xx]</p>
     *
     * @param snapshot to process
     * @return min max and mean
     */
    public static CharSequence snapshotToString(Snapshot snapshot) {
        StringBuilder builder = new StringBuilder(300);
        builder.append(" min [");
        builder.append(snapshot.getMin());
        builder.append("] max [");
        builder.append(snapshot.getMax());
        builder.append("] mean [");
        builder.append(snapshot.getMean());
        builder.append("]");

        return builder;
    }

    /**
     * Return the mean rate from the meter formatted.
     *
     * @param meter to process
     * @return formatted mean rate
     */
    public static CharSequence getFormattedMeanRate(Meter meter) {
        return DECIMAL_FORMAT.get().format(meter.getMeanRate());
    }
}
