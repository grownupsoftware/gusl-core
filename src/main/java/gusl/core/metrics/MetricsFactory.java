package gusl.core.metrics;

import com.codahale.metrics.*;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.JmxAttributeGauge;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;
import gusl.core.utils.ReflectionUtil;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;

/**
 * Helper Factory class for metrics.
 *
 * @author dhudson
 */
public class MetricsFactory {

    private static final String PROP_METRIC_REG_JVM_MEMORY = "jvm.memory";
    private static final String PROP_METRIC_REG_JVM_GARBAGE = "jvm.garbage";
    private static final String PROP_METRIC_REG_JVM_THREADS = "jvm.threads";
    private static final String PROP_METRIC_REG_JVM_PROCESS_CPU = "jvm.cpu.process";
    private static final String PROP_METRIC_REG_JVM_SYSTEM_CPU = "jvm.cpu.system";
    private static final String JMX_OPERATING_SYSTEM_OBJECT_NAME = "java.lang:type=OperatingSystem";
    private static final String JMX_PROCESS_CPU_ATTRIBUTE_NAME = "ProcessCpuLoad";
    private static final String JMX_SYSTEM_CPU_ATTRIBUTE_NAME = "SystemCpuLoad";

    private static final GUSLMetricRegistry theRegistry = new GUSLMetricRegistry();

    private static GUSLConsoleReporter theReporter = null;
    private static GUSLJmxReporter theJmxReporter = null;

    private static boolean jmxRegistered = false;

    public static Gauge getGauge(String id) {
        return theRegistry.getGauges().get(id);
    }

    public static Counter getCounter(String id) {
        return getCounter(getCallingClass(), id);
    }

    public static Map<String, Counter> getCounters() {
        return theRegistry.getCounters();
    }

    public static Meter getMeter(String id) {
        return getMeter(getCallingClass(), id);
    }

    public static Meter getMeter(Class<?> clazz, String id) {
        return theRegistry.meter(getName(clazz, id));
    }

    public static Meter getMeter(String meterName, String id) {
        return theRegistry.meter(MetricRegistry.name(meterName, id));
    }

    public static Timer getTimer(Class<?> clazz, String id) {
        return theRegistry.timer(getName(clazz, id));
    }

    public static Timer getTimer(String name) {
        return theRegistry.timer(name);
    }

    public static Latency getLatency(String name) {
        return theRegistry.latency(name);
    }

    public static Latency getLatency(Class<?> clazz, String id) {
        return theRegistry.latency(getName(clazz, id));
    }

    public static SortedMap<String, Latency> getLatencies() {
        return theRegistry.getLatencies();
    }

    public static QueueMetric getQueueMetric(String name) {
        return theRegistry.queueMetric(name);
    }

    public static Counter getCounter(Class<?> clazz, String id) {
        return theRegistry.counter(getName(clazz, id));
    }

    public static Histogram histogram(Class<?> clazz, String id) {
        return theRegistry.histogram(getName(clazz, id));
    }

    private static String getName(Class<?> clazz, String id) {
        return MetricRegistry.name(clazz.getName(), id);
    }

    public static void runConsoleReporter() {

        if (theReporter == null) {
            theReporter = GUSLConsoleReporter.forRegistry(theRegistry)
                    .convertRatesTo(TimeUnit.SECONDS)
                    .convertDurationsTo(TimeUnit.SECONDS)
                    .build();

            theReporter.start(5, TimeUnit.SECONDS);
        }
    }

    public static void report() {
        if (theReporter != null) {
            theReporter.report();
        }
    }

    public static void runJmxReporter(String jmxDomainName) {
        if (theJmxReporter == null) {
            theJmxReporter = GUSLJmxReporter.forRegistry(theRegistry)
                    .convertRatesTo(TimeUnit.SECONDS)
                    .convertDurationsTo(TimeUnit.MILLISECONDS)
                    .inDomain(jmxDomainName)
                    .registerWith(ManagementFactory.getPlatformMBeanServer())
                    .build();

            theJmxReporter.start();
        }
    }

    public static void registerJvmMetrics() {
        if (jmxRegistered) {
            return;
        }
        jmxRegistered = true;
        theRegistry.register(PROP_METRIC_REG_JVM_MEMORY, new MemoryUsageGaugeSet());
        theRegistry.register(PROP_METRIC_REG_JVM_GARBAGE, new GarbageCollectorMetricSet());
        theRegistry.register(PROP_METRIC_REG_JVM_THREADS, new ThreadStatesGaugeSet());

        try {
            MBeanServer mbeanServer = ManagementFactory.getPlatformMBeanServer();
            ObjectName jmxOsObjectName = new ObjectName(JMX_OPERATING_SYSTEM_OBJECT_NAME);
            theRegistry.register(PROP_METRIC_REG_JVM_PROCESS_CPU, new JmxAttributeGauge(mbeanServer, jmxOsObjectName,
                    JMX_PROCESS_CPU_ATTRIBUTE_NAME));
            theRegistry.register(PROP_METRIC_REG_JVM_SYSTEM_CPU, new JmxAttributeGauge(mbeanServer, jmxOsObjectName,
                    JMX_SYSTEM_CPU_ATTRIBUTE_NAME));
        } catch (MalformedObjectNameException e) {
        }
    }

    public static Histogram getHistogram(Class<?> clazz, String id) {
        return theRegistry.histogram(getName(clazz, id));
    }

    public static SortedMap<String, Meter> getMeters() {
        return theRegistry.getMeters();
    }

    public static GUSLMetricRegistry getRegistry() {
        return theRegistry;
    }

    public static <T extends Metric> T register(String name, T metric) {
        return getRegistry().register(getName(getCallingClass(), name), metric);
    }

    public static <T extends Metric> T registerWithoutClass(String name, T metric) {
        getRegistry().remove(name);
        return getRegistry().register(name, metric);
    }

    private static Class<?> getCallingClass() {
        return ReflectionUtil.getCallerClass(3);
    }

}
