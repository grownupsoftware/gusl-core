package gusl.core.metrics;

import com.codahale.metrics.*;
import lombok.*;

import java.util.Map;

/**
 * @author dhudson
 * @since 17/03/2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GUSLMetrics {
    private Map<String, Gauge> gauges;
    private Map<String, Counter> counters;
    private Map<String, Histogram> histograms;
    private Map<String, Meter> meters;
    private Map<String, Timer> timers;
    private Map<String, Latency> latencies;
    private Map<String, QueueMetric> queues;
}
