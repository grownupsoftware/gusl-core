package gusl.core.metrics;

import com.codahale.metrics.Clock;
import com.codahale.metrics.Metric;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author gbw
 */
public class ThroughputMetric implements Metric {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(ThroughputMetric.class);

    private volatile AtomicIntegerArray rollingSeconds = new AtomicIntegerArray(60);
    private volatile AtomicIntegerArray minutes = new AtomicIntegerArray(60);
    private volatile AtomicLong total = new AtomicLong(0);

    private volatile AtomicInteger lastSecond = new AtomicInteger(0);
    private volatile AtomicLong lastMinute = new AtomicLong(0);
    private volatile AtomicLong lastHour = new AtomicLong(0);

    private volatile AtomicInteger secondIndex = new AtomicInteger(0);

    private final Clock clock;

    private final long startTime;

    public ThroughputMetric() {
        clock = Clock.defaultClock();
        startTime = clock.getTime();
    }

    public void inc() {
        total.getAndIncrement();
        long time = clock.getTime() - startTime;
        int second = (int) (TimeUnit.MILLISECONDS.toSeconds(time) % 60);
        long minute = TimeUnit.MILLISECONDS.toMinutes(time) % 60;
        long hour = TimeUnit.MILLISECONDS.toHours(time);

        int sIdx = secondIndex.get();
        if (second > sIdx) {
            // nothing recv between now (second) and last request (sIdx)
            // e.g. last=3, now =10 - need to clear 4-10
            for (int x = sIdx + 1; x <= second; x++) {
                rollingSeconds.getAndSet(x, 0);
            }
            secondIndex.set(second);
        } else if (second > sIdx) {
            // e.g. last=55, now=5 - need to clear 56-59 and 1-5
            for (int x = sIdx + 1; x <= 60; x++) {
                rollingSeconds.getAndSet(x, 0);
            }
            for (int x = 0; x < sIdx; x++) {
                rollingSeconds.getAndSet(x, 0);
            }

        }

        rollingSeconds.getAndIncrement(second);

        logger.info("h: {} m: {} s: {}", hour, minute, second);
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("seconds: ").append(secondThroughput()).append("\n");
        //builder.append("minutes: ").append(minutes.toString()).append("\n");
        return builder.toString();
    }

    public String secondThroughput() {
        StringBuilder builder = new StringBuilder();
        int sIdx = secondIndex.get();
        for (int x = sIdx; x < 60; x++) {
            builder.append(rollingSeconds.get(x)).append(",");
        }
        for (int x = 0; x < sIdx; x++) {
            builder.append(rollingSeconds.get(x)).append(",");
        }
        return builder.toString();

    }

    public static void main(String[] args) throws InterruptedException {
        ThroughputMetric metric = new ThroughputMetric();
        for (int x = 0; x < 70; x++) {
            Thread.sleep(500);
            metric.inc();
            logger.info(metric.toString());
        }
//        logger.info(metric.toString());
    }

}
