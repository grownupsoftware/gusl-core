package gusl.core.metrics;

import com.codahale.metrics.Metric;

import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * @author gbw
 */
public class QueueMetric implements Metric {

    private int RANGE[] = new int[]{0, 10, 20, 50, 100, 200, 500, 1000, 2000, 3000, 4000, 5000, 10000, 20000, 50000};
    private volatile AtomicIntegerArray counters = new AtomicIntegerArray(RANGE.length);

    public void update(int size) {
        for (int x = 0; x < RANGE.length; x++) {
            if (x == 0) {
                if (size <= RANGE[x]) {
                    counters.getAndIncrement(0);
                    break;
                }
            } else if (x == RANGE.length - 1) {
                if (size >= RANGE[x]) {
                    counters.getAndIncrement(x);
                }

            } else if (size >= RANGE[x - 1] && size < RANGE[x]) {
                counters.getAndIncrement(x);
                break;
            }
        }
    }

    public int[] getRange() {
        return RANGE;
    }

    public void setRange(int[] range) {
        RANGE = range;
    }

    public int getValue(int index) {
        return counters.get(index);
    }

    public void reset() {
        for (int x = 0; x < counters.length(); x++) {
            counters.set(x, 0);
        }
    }

}
