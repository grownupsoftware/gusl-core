package gusl.core.metrics;

import com.codahale.metrics.Clock;
import com.codahale.metrics.Metric;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicLongArray;

/**
 * @author grant
 */
public class Latency implements Metric {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(Latency.class);

    // RANGE = bucket size in nanos - used as default where colection size is 1
    private long RANGE[] = new long[]{100000l, 200000l, 500000l, 1000000l, 2000000l, 3000000l, 4000000l, 5000000l, 10000000l, 15000000l, 20000000l, 50000000l, 100000000l, 250000000l, 500000000l, 1000000000l, 2000000000l, 3000000000l, 4000000000l, 5000000000l, 10000000000l};
//    private volatile AtomicLongArray counters = new AtomicLongArray(RANGE.length);
//    private volatile AtomicLong totalElapsed = new AtomicLong(0);

    private final int COLLECTION_BUCKETS[] = new int[]{1, 5, 10, 20, 50, 100, 500, 1000, 2000, 5000, 10000};

    private final BucketCounter bucketCounters[] = new BucketCounter[COLLECTION_BUCKETS.length];
    private final Clock clock;

    public Latency() {

        clock = Clock.defaultClock();
        for (int x = 0; x < bucketCounters.length; x++) {
            bucketCounters[x] = new BucketCounter(x);
        }
    }

    public long[] getRange() {
        return RANGE;
    }

    public void setRange(long[] range) {
        RANGE = range;
    }

    public BucketCounter[] getBucketCounters() {
        return bucketCounters;
    }

    public class BucketCounter {

        private final int bucketIndex;
        private volatile AtomicLongArray bucketCounters = new AtomicLongArray(RANGE.length);
        private volatile AtomicLong bucketTotalElapsed = new AtomicLong(0);

        public BucketCounter(int bucketIndex) {
            this.bucketIndex = bucketIndex;
        }

        public int getBucketSize() {
            return COLLECTION_BUCKETS[bucketIndex];
        }

        public void update(long duration) {
            for (int x = 0; x < RANGE.length; x++) {
                if (x == 0) {
                    if (duration < RANGE[x]) {
                        bucketCounters.getAndIncrement(0);
                        break;
                    }
                } else if (x == RANGE.length - 1) {
                    if (duration >= RANGE[x]) {
                        bucketCounters.getAndIncrement(x);
                    }

                } else {
                    if (duration >= RANGE[x - 1] && duration < RANGE[x]) {
                        bucketCounters.getAndIncrement(x);
                        break;
                    }
                }
            }
            bucketTotalElapsed.getAndAdd(duration);
        }

        private void reset() {
            bucketTotalElapsed.set(0);
            for (int x = 0; x < bucketCounters.length(); x++) {
                bucketCounters.set(x, 0);
            }
        }

        public long getValue(int index) {
            return bucketCounters.get(index);
        }

        public long getTotalElapsed() {
            return bucketTotalElapsed.get();
        }
    }

    public static class Context implements AutoCloseable {

        private final Latency timer;
        private final Clock clock;
        private final long startTime;
        private int number;

        private Context(Latency timer, Clock clock, int number) {
            this.timer = timer;
            this.clock = clock;
            this.startTime = clock.getTick();
            this.number = number;
        }

        /**
         * Updates the timer with the difference between current and start time.
         * Call to this method will not reset the start time. Multiple calls
         * result in multiple updates.
         *
         * @return the elapsed time in nanoseconds
         */
        public long stop() {
            final long stop = clock.getTick();
            final long elapsed = stop - startTime;
            timer.update(number, elapsed, TimeUnit.NANOSECONDS);
            return elapsed;
        }

        /**
         * Equivalent to calling {@link #stop()}.
         */
        @Override
        public void close() {
            stop();
        }

        public void updateCollectionSize(int number) {
            this.number = number;
        }
    }

    public Context time() {
        return new Context(this, clock, 1);
    }

    public Context time(int number) {
        return new Context(this, clock, number);
    }

    public void update(int number, long duration, TimeUnit unit) {
        update(number, unit.toNanos(duration));
    }

    public void update(long duration, TimeUnit unit) {
        update(1, unit.toNanos(duration));
    }

    private void update(int number, long duration) {
        for (int x = 0; x < COLLECTION_BUCKETS.length; x++) {
            if (x == 0) {
                if (number <= COLLECTION_BUCKETS[x]) {
                    bucketCounters[0].update(duration);
                    break;
                }
            } else if (x == COLLECTION_BUCKETS.length - 1) {
                if (number >= COLLECTION_BUCKETS[x]) {
                    bucketCounters[x].update(duration);
                }

            } else {
                if (number >= COLLECTION_BUCKETS[x - 1] && number < COLLECTION_BUCKETS[x]) {
                    bucketCounters[x].update(duration);
                    break;
                }
            }
        }
    }

    public void reset() {
        for (BucketCounter bucketCounter : bucketCounters) {
            bucketCounter.reset();
        }
    }

    public int[] getBucketSizes() {
        return COLLECTION_BUCKETS;
    }

    public String getTag(long value) {
        if (value < 1000000) {
            return String.format("%4.2f ms", (double) value / 1000000.0d);
        } else if (value < 1000000000) {
            return String.format("%4.0f ms", (double) value / 1000000.0d);
        } else {
            return String.format("%4.0f s", (double) value / 1000000000.0d);
        }
    }

    public long getTotalElapsed() {
        return bucketCounters[0].bucketTotalElapsed.get();
    }
}
