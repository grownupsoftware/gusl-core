package gusl.core.security;

/**
 * @author dhudson
 * @since 17/05/2022
 */
public enum ObfuscateMethod {
    // Clear Text
    NONE,
    // Salted Write, read will be salted value
    SALTED_WRITE,
    // Obfuscated write, read will be obfuscated
    OBFUSCATED_WRITE,
    // Obfuscated write, read will be decrypted
    OBFUSCATED_WRITE_READ
}
