package gusl.core.security;

import gusl.core.exceptions.GUSLException;

/**
 *
 * @author dhudson
 */
@SuppressWarnings("serial")
public class GUSLSecurityException extends GUSLException {

    public GUSLSecurityException(String message, Throwable ex) {
        super(message, ex);
    }

    public GUSLSecurityException(String message) {
        super(message);
    }
}
