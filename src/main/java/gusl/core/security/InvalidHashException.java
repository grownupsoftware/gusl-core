package gusl.core.security;

/**
 *
 * @author dhudson
 */
@SuppressWarnings("serial")
public class InvalidHashException extends GUSLSecurityException {
    
    public InvalidHashException(String message, Throwable ex) {
        super(message, ex);
    }
    
    public InvalidHashException(String message) {
        super(message);
    }
}
