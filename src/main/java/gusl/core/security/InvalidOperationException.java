package gusl.core.security;

/**
 *
 * @author dhudson
 */
@SuppressWarnings("serial")
public class InvalidOperationException extends GUSLSecurityException {
    
    public InvalidOperationException(String message, Throwable ex) {
        super(message, ex);
    }
    
    public InvalidOperationException(String message) {
        super(message);
    }
}
