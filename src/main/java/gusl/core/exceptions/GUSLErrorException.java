package gusl.core.exceptions;

import gusl.core.errors.ErrorDO;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Note: This is also a catch call for any other exception through by inter-node
 * communication.
 * <p>
 * NB: All Sub Classes of this, must have an empty constructor.
 *
 * @author grant
 */
@SuppressWarnings("serial")
public class GUSLErrorException extends ResourceException {

    private List<ErrorDO> errors = new ArrayList<>(1);
    private boolean suppressStack;

    public static GUSLErrorException of(String message) {
        ErrorDO error = new ErrorDO();
        error.setMessage(message);
        return new GUSLErrorException(error);
    }

    public GUSLErrorException() {
        super("GUSLErrorException - use getErrors()");
    }

    public GUSLErrorException(ErrorDO error) {
        super(error.toString(), Locale.getDefault());
        this.errors.add(error);
    }

    public GUSLErrorException(ErrorDO error, boolean suppressStack) {
        super(error.toString(), Locale.getDefault());
        this.errors.add(error);
        this.suppressStack = suppressStack;
    }

    public GUSLErrorException(ErrorDO error, Throwable cause) {
        super(error.toString(), Locale.getDefault(), cause);
        this.errors.add(error);
    }

    public GUSLErrorException(Locale locale, ErrorDO error) {
        super(error.toString(), locale);
        this.errors.add(error);
    }

    public GUSLErrorException(List<ErrorDO> errors) {
        super(errors.get(0).toString(), Locale.getDefault());
        this.errors.addAll(errors);
    }

    public GUSLErrorException(Locale locale, List<ErrorDO> errors) {
        super(errors.get(0).toString(), locale);
        this.errors.addAll(errors);
    }

    public ErrorDO getError() {
        if (!errors.isEmpty()) {
            return errors.get(0);
        } else {
            return new ErrorDO(null, getMessage(), "internal.error");
        }
    }

    public void setErrors(List<ErrorDO> errors) {
        this.errors = errors;
    }

    public List<ErrorDO> getErrors() {
        return errors;
    }

    public boolean hasErrorKey(String key) {
        for (ErrorDO error : errors) {
            if (key.equals(error.getMessageKey())) {
                return true;
            }
        }
        return false;
    }

    public boolean isSuppressStack() {
        return suppressStack;
    }

    public GUSLErrorException setSuppressStack(boolean suppressStack) {
        this.suppressStack = suppressStack;
        return this;
    }

    public GUSLErrorException addError(ErrorDO err) {
        this.errors.add(err);
        return this;
    }

    @Override
    public String toString() {
        return "GUSLErrorException{" + "errors=" + errors + '}';
    }

}
