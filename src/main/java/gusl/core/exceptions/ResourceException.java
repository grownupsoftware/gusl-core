package gusl.core.exceptions;

import java.util.Locale;

/**
 *
 * @author grant
 */
public class ResourceException extends GUSLException {

    private static final long serialVersionUID = -8178841584372733192L;

    private Locale locale;

    /**
     * @param message
     */
    public ResourceException(String message) {
        this(message, Locale.getDefault());
    }

    /**
     * @param message
     * @param locale
     */
    public ResourceException(String message, Locale locale) {
        super(message);
        this.locale = locale;
    }

    /**
     * @param cause
     */
    public ResourceException(Throwable cause) {
        this(cause.getMessage(), cause);
    }

    /**
     * @param message
     * @param cause
     */
    public ResourceException(String message, Throwable cause) {
        this(message, Locale.getDefault(), cause);
    }

    /**
     * @param message
     * @param locale
     * @param cause
     */
    public ResourceException(String message, Locale locale, Throwable cause) {
        super(message, cause);
        this.locale = locale;
    }

    /**
     * Returns locale.
     *
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Sets locale.
     *
     * @param locale the locale value
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}
