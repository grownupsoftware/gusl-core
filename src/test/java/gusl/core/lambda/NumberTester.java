package gusl.core.lambda;

import lombok.CustomLog;
import org.junit.Test;

/**
 * @author dhudson
 */
@CustomLog
public class NumberTester {

    @Test
    public void numberTest() {
        MutableInteger number = new MutableInteger();

        logger.info("{}", number.getAndIncrement());
        logger.info("{}", number);

        number.reset();

        logger.info("{}", number.incrementAndGet());
        logger.info("{}", number);

        MutableBoolean bool = new MutableBoolean();

        logger.info("{}", bool.getAndToggle());
        logger.info("{}", bool);

        MutableSwitch mSwitch = new MutableSwitch(true);
        logger.info("{}", mSwitch.getAndToggle());
        logger.info("{}", mSwitch.getAndToggle());
        logger.info("{}", mSwitch.getAndToggle());
    }
}
