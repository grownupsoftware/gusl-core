package gusl.core.networking;

import lombok.CustomLog;
import org.junit.Test;

import java.net.UnknownHostException;
import java.util.Collections;

/**
 * https://ipinfo.io/AS714
 *
 * @author dhudson on 09/09/2019
 */
@CustomLog
public class FirewallTester {

    @Test
    public void testAppleCIDRs() throws UnknownHostException {

        System.out.println("Hello World ...");

        String appleIP = "17.222.112.77";

        Firewall firewall = new Firewall();
        firewall.addCIDRToWhiteList("144.178.0.0/19");
        firewall.addCIDRToWhiteList("144.178.2.0/24");
        firewall.addCIDRToWhiteList("144.178.48.0/21");
        firewall.addCIDRToWhiteList("144.178.55.0/24");
        firewall.addCIDRToWhiteList("144.178.56.0/21");
        firewall.addCIDRToWhiteList("17.0.0.0/16");
        firewall.addCIDRToWhiteList("17.0.0.0/21");
        firewall.addCIDRToWhiteList("17.0.0.0/8");
        firewall.addCIDRToWhiteList("17.0.0.0/9");
        firewall.addCIDRToWhiteList("17.0.109.0/24");

        System.out.println("Firewall says ..  " + firewall.isAddressInWhiteList(appleIP));

    }

    @Test
    public void replaceWhiteList() throws UnknownHostException {

        System.out.println("Hello World ...");

        Firewall firewall = new Firewall();
        assert (firewall.getWhiteList().size() == 0);

        firewall.replaceWhiteList(Collections.singletonList("144.178.0.0/19"));
        assert (firewall.getWhiteList().size() == 1 && firewall.getWhiteList().get(0).getCIDR().equals("144.178.0.0/19"));
    }


    @Test
    public void replaceBlackList() throws UnknownHostException {

        System.out.println("Hello World ...");

        Firewall firewall = new Firewall();
        assert (firewall.getBlackList().size() == 0);

        firewall.replaceBlackList(Collections.singletonList("144.178.0.0/19"));
        assert (firewall.getBlackList().size() == 1 && firewall.getBlackList().get(0).getCIDR().equals("144.178.0.0/19"));
    }
}
