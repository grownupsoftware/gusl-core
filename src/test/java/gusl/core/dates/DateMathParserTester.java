package gusl.core.dates;

import lombok.CustomLog;
import org.junit.Test;

import java.time.Instant;

/**
 * @author dhudson
 */
@CustomLog
public class DateMathParserTester {

    private String START_OF_LAST_MONTH = "now-1M/M";
    private String START_OF_THIS_MONTH = "now/M";
    private String HOUR_AGO = "now-1h";
    private String START_OF_TODAY = "now/d";
    private String START_OF_YESTERDAY = "now-1d/d";

    @Test
    public void testParser() {

        try {
            Instant instant = DateMathParser.parse(START_OF_YESTERDAY, true);
            logger.info("{}", instant);
            //logger.info("{}", formatter.format(instant));

        } catch (GUSLDateException ex) {
            logger.warn("Oh no ...  ", ex);
        }
    }
}
