/*
 * Grownup  Software Limited.
 */
package gusl.core.config;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Dummy Config class.
 *
 * @author dhudson - Jul 26, 2017 - 11:58:45 AM
 */
@Getter
@Setter
@NoArgsConstructor
public class Foo {

    @JsonDeserialize(using = ByteNumberDeserializer.class)
    private int inputBufferSize;

    @JsonDeserialize(using = CoreCountDeserializer.class)
    private int coreSize;

    @JsonDeserialize(using = ObfuscationDeserializer.class)
    private String password;

    @JsonDeserialize(using = MillisNumberDeserializer.class)
    private long time;

    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String systemEnv;

    @JsonDeserialize(using = SecretDeserializer.class)
    private String vaultPassword;

    @JsonDeserialize(using = VaultDeserializer.class)
    private String override;

    @JsonDeserialize(using = SecretDeserializer.class)
    private String secretPassword;

    @JsonDeserialize(using = SecretDeserializer.class)
    private String clearPassword;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
