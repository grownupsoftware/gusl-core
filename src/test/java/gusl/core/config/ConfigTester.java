package gusl.core.config;

import gusl.core.utils.Platform;
import lombok.CustomLog;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * @author dhudson - Jul 26, 2017 - 11:56:57 AM
 */
@CustomLog
public class ConfigTester {

    @Test
    public void loadTester() throws IOException {

        Foo foo = ConfigUtils.loadConfig(Foo.class, "Foo.json");

        int cores = Platform.getNumberOfCores();

        logger.info("{}", foo.getSystemEnv());

        assert (foo.getCoreSize() == cores + 1);
        assert (foo.getInputBufferSize() == ConfigUtils.KILOBYTE * 8);
        assert (foo.getPassword().equals("password"));

        assert (foo.getTime() == (6 * 1000));
    }

    private void printSystem() {
        logger.info("System ...");
        Map<String, String> system = System.getenv();
        for (String key : system.keySet()) {
            logger.info("{}:{}", key, system.get(key));
        }
    }

    private void printProperties() {
        Properties props = System.getProperties();
        props.list(System.out);
    }


    @Test
    public void systemPropsTest() {
        System.setProperty("BAR","BAR");
        System.setProperty("FOO","FOO");
        String template = "startsWith..${FOO:withDefault}/with more:data${BAR}";
        logger.info("{}", SystemProperyDeserializer.substitute(template));
        logger.info("{}",SystemProperyDeserializer.substitute("HelloWorld"));
        logger.info("{}",SystemProperyDeserializer.substitute("${ApplicationRouter:http://localhost:9000}"));
    }
}
