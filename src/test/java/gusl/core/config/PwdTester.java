/*
 * Grownup  Software Limited.
 */
package gusl.core.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import gusl.core.security.ObfuscatedStorage;
import org.junit.Test;

import java.io.IOException;

/**
 * @author dhudson - Jul 26, 2017 - 12:50:11 PM
 */
//@Ignore
public class PwdTester {

    private String scramble = "dc357948-fb1b-4fd4-8aff-0bf8b8aa040b";

    @Test
    public void generateOBTest() {
        System.out.println("" + ObfuscatedStorage.encrypt(scramble));
    }

    @Test
    public void unscrambleTest() throws IOException {
        String key = "OB:xNDDa9tRuUraLG5oDAuz1WoFrzA=";

        System.out.print(deserialize(key));
    }

    public String deserialize(String value) throws IOException, JsonProcessingException {

        if (ObfuscatedStorage.isObfuscated(value)) {
            return ObfuscatedStorage.decryptKey(value);
        }

        return value;
    }


}
