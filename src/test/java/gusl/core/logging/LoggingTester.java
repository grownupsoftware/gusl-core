package gusl.core.logging;

import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.Utils;
import lombok.CustomLog;
import org.junit.Test;

/**
 * @author dhudson on 03/10/2019
 */
@CustomLog
public class LoggingTester {

    @Test
    public void loggerTest() {

        logger.debug("And this is what this looks like now {}", () -> ObjectMapperFactory.safePrettyPrint(logger));

        logger.info("{}", logger.getRawLogger().getClass().getName());

        logger.getRawLogger().info("This is a test {}", () -> "Hello");

        logger.audit("This is an AUDIT message .....");

        //Utils.sleep(3000);
    }
}
