package gusl.core.eventbus;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import gusl.core.logging.GUSLLogger;
import lombok.CustomLog;
import org.junit.Test;

/**
 *
 * @author dhudson
 */
@CustomLog
public class SlowRunnerTester {

    private final CountDownLatch theLatch = new CountDownLatch(1);

    @Test
    public void scheduledTest() throws InterruptedException {
        LmEventBus eventBus = new LmEventBusImpl();
        eventBus.configure(2);

        eventBus.register(this);
        logger.info("Posting 2 second into the future");
        eventBus.post(new FooEvent());

        theLatch.await(3, TimeUnit.SECONDS);
        logger.info("All done");
        eventBus.shutdown();
    }

    @OnEvent(slowLongRunning = true)
    public void handleFoo(FooEvent ev) {
        logger.info("handle FooEvent ... {}", Thread.currentThread().getName());
        theLatch.countDown();
    }
}
