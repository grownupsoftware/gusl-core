/*
 * Grownup Software Limited.
 */
package gusl.core.eventbus;

import gusl.core.utils.Utils;
import org.junit.Test;

/**
 *
 * @author dhudson - Apr 10, 2017 - 3:41:18 PM
 */
public class PostTester {

    @Test
    public void postTest() {

        FooListener listener = new FooListener();
        LmEventBus eventBus = new LmEventBusImpl();
        eventBus.configure(2);

        eventBus.register(listener);

        eventBus.post(new FooEvent());
        
        
        Utils.sleep(2000);
    }
}
