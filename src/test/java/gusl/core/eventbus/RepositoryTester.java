package gusl.core.eventbus;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import gusl.core.json.ObjectMapperFactory;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Platform;
import lombok.CustomLog;
import org.junit.Test;

/**
 *
 * @author dhudson
 */
@CustomLog
public class RepositoryTester {

    @Test
    public void repositoryTester() throws EventBusRepositoryException {

        LmEventBus eventBus = new LmEventBusImpl();
        eventBus.configure(2);

        File location = new File(Platform.getTmpDirectory(), "ebr");
        eventBus.register(this);

        EventBusRepository repository = new JsonFileEventBusRepository(location);
        eventBus.installRepositoryHandler(repository);

        for (int i = 0; i < 10; i++) {
            // Should give us a enough time...
            eventBus.persistAndScheduledPost(new FooEvent(), 3, TimeUnit.DAYS);
            eventBus.persistAndPost(new FooEvent());
        }

        List<StorableQueueEntry> repo = repository.load();
        for (StorableQueueEntry repoEntry : repo) {
            assert (repoEntry.getId() != null);
            assert (repoEntry.getEvent() != null);
        }
    }

    @OnEvent
    public void handleFoo(FooEvent event) {
        logger.info("I have a foo");
    }

    //@Test
    public void deleteMe() throws IOException {
        ObjectMapper mapper = ObjectMapperFactory.getDefaultObjectMapper();

        MyEvent event = new MyEvent();
        event.setEvent(new FooEvent());
        event.setId(UUID.randomUUID().toString());

//        JsonNode node = mapper.readTree(mapper.writeValueAsString(event));
//        JsonNode eventNode = node.get("event");
        logger.info(mapper.writeValueAsString(event));

    }

    private class MyEvent {
//using = EventSer.class

        //@JsonSerialize(using = EventSer.class)
         @JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
        private Object event;
        private String id;

        public MyEvent() {
        }

        public Object getEvent() {
            return event;
        }

        public void setEvent(Object event) {
            this.event = event;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

    }

    private class EventDeser extends JsonDeserializer<Object> {

        @Override
        public Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            return null;
        }
    }

    private class EventSer extends JsonSerializer<Object> {

        public EventSer() {
            
        }
        
        @Override
        public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
            logger.info("I got here .....");
            gen.getCodec();
        }

    }
}
