/*
 * Grownup Software Limited.
 */
package gusl.core.eventbus;

import gusl.core.exceptions.GUSLException;
import lombok.CustomLog;

import java.io.IOException;

/**
 * @author dhudson - Apr 10, 2017 - 11:24:40 AM
 */
@CustomLog
public class FooListener {

    @OnEvent(order = 10)
    public void method1(FooEvent foo) {
        logger.info("Execute method1");
    }

    @OnEvent(order = 30)
    public void method3(FooEvent foo) {
        logger.info("Execute method3");
    }

    @OnEvent
    public void method4(FooEvent foo) {
        logger.info("Execute method4");
    }

    @OnEvent(order = 20)
    public void method2(FooEvent foo) {
        logger.info("Execute method2");
    }

    @OnEvent(order = 99)
    public void method99(FooEvent foo) {
        logger.info("Execute method99");
    }

    @OnEvent(order = 15, failFast = true, expected = IOException.class)
    public void throwIOExceptionExpected(FooEvent foo) throws IOException {
        throw new IOException("Should not see this..");
    }

    @OnEvent(order = 15, failFast = true)
    public void throwIOExceptionNotExpected(FooEvent foo) throws IOException {
        throw new IOException("Should see this..");
    }

    @OnEvent(order = 15, failFast = true, expected = IOException.class)
    public void throwLmExceptionNotExpected(FooEvent foo) throws GUSLException {
        throw new GUSLException("Should see this..");
    }

//    @OnEvent
//    public void invalid1() {
//    }
//
//    @OnEvent
//    public void invalid2(FooEvent foo, ArrayList<String> list) {
//    }
}
