/*
 * Grownup Software Limited.
 */
package gusl.core.eventbus;

/**
 *
 * @author dhudson - Apr 10, 2017 - 11:12:22 AM
 */
public class FooEvent {

    public FooEvent() {
    }

    public String getMessage() {
        return "Hello World";
    }
}
