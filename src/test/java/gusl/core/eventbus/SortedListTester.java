/*
 * Grownup Software Limited.
 */
package gusl.core.eventbus;

import gusl.core.exceptions.GUSLException;
import lombok.CustomLog;
import org.junit.Test;

import java.util.List;

/**
 * @author dhudson - Apr 10, 2017 - 11:10:22 AM
 */
@CustomLog
public class SortedListTester {

    @Test
    public void sortedMethodTest() throws GUSLException {
        ListenerRegistra registra = new ListenerRegistra();
        FooListener fooListener1 = new FooListener();
        registra.register(fooListener1);

        List<ListenerMethod> methods = registra.getListenersFor(FooEvent.class);

        assert (methods.size() == 5);

        for (ListenerMethod method : methods) {
            logger.info(" Method Name {} ", method.getMethod().getName());
        }

        assert (methods.get(0).getMethod().getName().equals("method1"));
        assert (methods.get(1).getMethod().getName().equals("method2"));
        assert (methods.get(2).getMethod().getName().equals("method3"));
        assert (methods.get(3).getMethod().getName().equals("method4"));
        assert (methods.get(4).getMethod().getName().equals("method99"));

        // Register the same thing again
        registra.register(fooListener1);

        methods = registra.getListenersFor(FooEvent.class);
        assert (methods.size() == 5);

        registra.register(new FooListener());
        methods = registra.getListenersFor(FooEvent.class);

        assert (methods.size() == 10);

        registra.unregister(fooListener1);
        methods = registra.getListenersFor(FooEvent.class);

        assert (methods.size() == 5);

        registra.unregister(fooListener1);

        methods = registra.getListenersFor(FooEvent.class);

        assert (methods.isEmpty());
    }
}
