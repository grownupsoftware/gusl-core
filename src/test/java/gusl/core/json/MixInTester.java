package gusl.core.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author dhudson
 */
public class MixInTester {

    @Test
    public void minInTest() {
        try {
            ObjectMapper mapper = ObjectMapperFactory.createDefaultObjectMapper();
            FooDTO foo = new FooDTO();
            System.out.println(mapper.writeValueAsString(foo));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
