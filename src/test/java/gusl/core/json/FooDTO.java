package gusl.core.json;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gusl.core.config.ObfuscationDeserializer;
import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.Setter;

/**
 * @author dhudson
 */
@Getter
@Setter
public class FooDTO {

    @JsonDeserialize(using = ObfuscationDeserializer.class)
    @JsonSerialize(using = ObfuscationSerializer.class)
    private String number;

    @JsonSerialize(using = SaltSerializer.class)
    private String string;

    public FooDTO() {
        number = "key";
        string = "String";
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
