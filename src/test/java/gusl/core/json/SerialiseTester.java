package gusl.core.json;

import lombok.CustomLog;
import org.junit.Test;

import java.io.IOException;

/**
 * @author dhudson
 * @since 08/10/2021
 */
@CustomLog
public class SerialiseTester {

    @Test
    public void serialTest() throws IOException {
        FooDTO foo = new FooDTO();
        String json = ObjectMapperFactory.getDefaultObjectMapper().writeValueAsString(foo);

        logger.info("JSON .. {}", json);

        FooDTO bar = ObjectMapperFactory.createDefaultObjectMapper().readValue(json, FooDTO.class);

        logger.info("Foo .. {}", bar);

        String barString = ObjectMapperFactory.getDefaultObjectMapper().writeValueAsString(bar);

        logger.info("BarString {}", barString);

        logger.info("Equals {}", json.equals(barString));
    }

}
