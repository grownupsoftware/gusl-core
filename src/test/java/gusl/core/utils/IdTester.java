package gusl.core.utils;

import lombok.CustomLog;
import org.junit.Test;

import java.time.Clock;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

/**
 * @author dhudson
 */
@CustomLog
public class IdTester {

    @Test
    public void testIdGen() {
        IdGenerator.setNodeUniqueId(2);
        long id = IdGenerator.generateUniqueNodeId();

        Snowflake snowflake = new Snowflake(2, 5);

        logger.info("Ours {} ... Snowflake {}  ..  New  .. {}", id, snowflake.nextId(), Long.toString(id, 32));

    }

    @Test
    public void shortIdGen() {

        Clock CLOCK = Clock.systemUTC();
        // The day it was born.                
        long BORN_EPOCH = 1496962800000L; // 2017-06-09

        long startTime = CLOCK.millis() - BORN_EPOCH;

        logger.info("startTime = Epoch {} {} / 2 {}", CLOCK.millis(), startTime, startTime / 2);

        Date date = new Date(44182285725L);
        logger.info("Short Date {}", DateFormats.getISODateFormat().format(date));
    }

    @Test
    public void multiThreadIDGen() {
        AtomicLong aLong = new AtomicLong(0);

        IdGenerator.setForTesting();
        ExecutorService executor = Executors.newFixedThreadPool(10);
        Set<String> ids = new HashSet<>();
        Map<String, Long> longs = new ConcurrentHashMap<>();
        IntStream.range(0, 10000).forEach(i -> executor.submit(() -> {
            //ids.add(IdGenerator.generateUniqueNodeIdAsString());
            final long newLong = aLong.incrementAndGet();
            longs.put(IdGenerator.generateUniqueNodeIdAsString(), newLong);
        }));
        //Utils.sleep(6000);
        stop(executor);
        logger.info("Count {}:{}", longs.size(), aLong.get());
        //logger.info("{}", Collections.newSetFromMap(longs));
    }

    public static void stop(ExecutorService executor) {
        try {
            executor.shutdown();
            executor.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            logger.warn("termination interrupted");
        } finally {
            if (!executor.isTerminated()) {
                logger.warn("killing non-finished tasks");
            }
            executor.shutdownNow();
        }
    }
}
