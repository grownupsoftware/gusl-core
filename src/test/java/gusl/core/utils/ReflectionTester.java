package gusl.core.utils;

import gusl.core.annotations.DocClass;
import lombok.CustomLog;
import org.junit.Test;

import java.util.Set;

/**
 * @author dhudson
 * @since 07/09/2021
 */
@CustomLog
public class ReflectionTester {

    @Test
    public void reflectionTest() {
        Set<Class<?>> classes = ClassUtils.getAnnotatedClasses(DocClass.class);

        logger.info("Number of classes {}", classes.size());
    }
}
