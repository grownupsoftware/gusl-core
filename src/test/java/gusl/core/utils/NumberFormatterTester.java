package gusl.core.utils;

import lombok.CustomLog;
import org.junit.Test;

import java.text.DecimalFormat;

/**
 * @author dhudson
 * @since 03/11/2022
 */
@CustomLog
public class NumberFormatterTester {

    @Test
    public void test1() {
        double amount = 1234143144;
        DecimalFormat format = new DecimalFormat("###,###,###.00####");

        logger.info("{}", format.format(amount));

    }
}
