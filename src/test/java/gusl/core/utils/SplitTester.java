package gusl.core.utils;

import lombok.CustomLog;
import org.junit.Test;

/**
 * @author dhudson
 * @since 11/07/2022
 */
@CustomLog
public class SplitTester {

    @Test
    public void splitTest() {
        String[] test1 = {"this is a call", "to=my"};
        String[] test2 = new String[0];
        String[] test3 = {"foo=bar", "luck=optimal"};
        String[] test4 = null;

        addToMap(test1);
        addToMap(test2);
        addToMap(test3);
        addToMap(test4);
    }

    private void addToMap(String[] args) {
        if(args == null || args.length == 0) {
            return;
        }

        for (String env : args) {
            String data[] = env.split("=");
            if(data.length == 2) {
                logger.info("Setting {} : {}",data[0],data[1]);
            }
        }
    }
}
