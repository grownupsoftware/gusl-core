package gusl.core.utils;

import lombok.CustomLog;
import org.junit.Test;

import java.text.SimpleDateFormat;

/**
 * @author dhudson
 */
@CustomLog
public class DateFormatTester {

    @Test
    public void dateFormatTest() {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        logger.info("Lenient ? {}", format.isLenient());
    }
}
