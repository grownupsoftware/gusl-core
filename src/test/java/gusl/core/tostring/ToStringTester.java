package gusl.core.tostring;

import lombok.CustomLog;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author dhudson on 2019-08-16
 */
@CustomLog
public class ToStringTester {

    @Test
    public void toStringTest() {
        Foo foo = new Foo();
        foo.setC("nnnnnnnnnnnnnnlkads;fljdfl;kajdf;ljadsfajdfk;dfj;jf;ldasjf;ldsjfad;lsjf;alsdfj;dsalfjd;lsfja;dlsfjads;lfja;dsfjdas;");
        foo.setD(";adfjdf dasfkjdksfj;adfj adkjf ;adlfkj a;sdlfkjdsa ;faldksfj ;aldkfj;lkfjask;ljsd;l fjas;ldfja;sdlfjs;dlfjk");
        logger.info("{}", foo);
    }

    @Test
    public void toTableTest() {
        Foos foos = new Foos();
        List<Foo> fooList = new ArrayList<>();
        foos.setFoos(fooList);

        fooList.add(Foo.builder().a("This is an A").b("This is a B").c("This is a c").d("This is a d").build());
        fooList.add(Foo.builder().a("Ta").b("b").c("c").d("d").build());
        fooList.add(Foo.builder().a("This is an A").b("This is a B").c("This is a c").d("This is a d").build());
        fooList.add(Foo.builder().a("Ta").b("b").c("c").d("d").build());

        logger.info("{}", foos);
    }

    @Test
    public void tableGenerator() {
        TableGenerator tableGenerator = new TableGenerator();

        List<String> headersList = new ArrayList<>();
        headersList.add("Id");
        headersList.add("F-Name");
        headersList.add("L-Name");
        headersList.add("Email");

        List<List<String>> rowsList = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            List<String> row = new ArrayList<>();
            row.add(UUID.randomUUID().toString().substring(30));
            row.add(UUID.randomUUID().toString().substring(10));
            row.add(UUID.randomUUID().toString().substring(28));
            row.add(UUID.randomUUID().toString());

            rowsList.add(row);
        }

        System.out.println(tableGenerator.generateTable(headersList, rowsList));
    }
}
