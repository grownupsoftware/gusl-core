package gusl.core.tostring;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author dhudson
 * @since 26/09/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class Foos {

    @ToStringTable()
    private List<Foo> foos;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
