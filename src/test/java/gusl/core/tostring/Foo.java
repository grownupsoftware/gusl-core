package gusl.core.tostring;

import lombok.*;

/**
 * @author dhudson on 2019-08-16
 */
@ToStringIgnoreNull
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Foo {

    private String a;
    private String b;
    @ToStringMask
    private String c;
    @ToStringSummary
    private String d;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
