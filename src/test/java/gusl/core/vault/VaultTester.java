package gusl.core.vault;

import gusl.core.config.ConfigUtils;
import gusl.core.config.Foo;
import gusl.core.exceptions.GUSLException;
import gusl.core.utils.SystemPropertyUtils;
import lombok.CustomLog;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

/**
 * @author dhudson
 * @since 10/03/2022
 */
@CustomLog
public class VaultTester {

    @BeforeClass
    public static void setup() {
        SystemPropertyUtils.setVaultParams("https://vault.vision-x.cloud/v1/kv/data/", "s.C1g7tG56pDzUbXrxW7VC3DdX");
    }

    @Test
    public void vaultTest() throws GUSLException {
        VaultClient client = new VaultClient();
        logger.info("Response {}", client.getSecret("visionx/qradar:key"));
    }

    @Test
    public void vaultElasticTest() throws GUSLException {
        VaultClient client = new VaultClient();
        client.getSecret("elastic:ELASTIC_PASSWORD");
    }

    @Test
    public void configTest() throws GUSLException {
        try {
            Foo foo = ConfigUtils.loadConfig(Foo.class, "Foo.json");
            logger.info("foo={}", foo);
        } catch (IOException ex) {
            logger.warn("Nope ..", ex);
        }
    }
}
